<?php
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

header('Expires: Tue, 03 Jul 2001 06:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Acerca de ...',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<link type="text/css" href="sys/acerca/_css/acerca.css" rel="stylesheet"/>'),
                'header' => true,
                'menu' => true,
                'id_menu' => $_SESSION['xIdMenu'],
                'txt_mod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>

  <div class="dvAbout">
	<img src="sys/acerca/_css/CSITI128.png" alt="CITI" /><br />  
    <span>Consultores de Sistemas Integrales</span><br />
    <span>en Tecnologias de la Informacion</span>
    
    <br />
	<p>Sistema Integral de Seguridad P�blica (<strong>SISP</strong>).</p>
	<hr/>

	<div>
		<br/>
		<strong>Producto:</strong> SISP <br/>
		<strong>Version:</strong> 1.5.0 <br/><br/>

		<strong>&copy; CSITI Guerrero 2014</strong><br/>
		<strong>Reservados todos los derechos.</strong><br/><br/><br/>

		<strong>Autorizado a:</strong> Secretaria de Seguridad P�blica y Protecci�n Civil<br/>
		<strong>Licencia No:</strong> SISP0001-30062014-MXGR2700-01 <br/>
	</div>

  </div>

<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>