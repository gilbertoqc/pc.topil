Sistema Integral de Seguridad Pública (SISP)

SISP es una Plataforma Informática diseñada para integrar y coordinar las áreas encargadas de la seguridad pública en los estados y municipios, para la operación del mando único. Y está conformada en tres grandes bloques.


*******************************
*** Modulos que lo Integran ***
*******************************

	- ¡Bloque administrativo!
		Se compone de los módulos que gestionan la información del personal operativo y administrativo de seguridad pública, como son: datos personales, aptitudes, media filiación, adscripción laboral, nivel de estudios y referencias; basado en la cédula única de identificación policial.

		Además, administra los trámites del personal como permisos, vacaciones, estímulos, incidencias y baja de la corporación, llevando un registro de cada trámite, en el archivo digital que viene integrado en el sistema.

		Incluye también el módulo de profesionalización, con la finalidad dar seguimiento al desempeño de cada elemento y de sus evaluaciones de control de confianza.

	- ¡Bloque Operativo!
		En los módulos de este bloque se podrán registrar las incidencias delictivas que se suscitan en una demarcación o sector, con la descripción detallada de los hechos, la ubicación geográfica donde ocurrió, personas implicadas, vehículos, armas y demás factores involucrados.

		Además se incluye una herramienta para la Investigación y Análisis Criminal, en la cual, es posible registrar información precisa de grupos delictivos, conflictos políticos, sociales y religiosos; obteniendo reportes estadísticos que serán de gran ayuda para mejorar la eficiencia operativa de la corporación.

	- ¡Bloque logístico!

		Con el objetivo de aprovechar al máximo los recursos y el estado de fuerza de la corporación, se implementaron los módulos para el control de inventario del armamento, equipo policial y municiones; la administración de los depósitos, control de resguardos; documentación y expedientes requeridos para la alta en la Licencia Oficial Colectiva avalada por la Secretaría de la Defensa Nacional.

		También administra el parque vehicular con que cuenta la corporación, para tener un mejor control sobre la ubicación, resguardo y mantenimiento de las unidades.


*******************************
*** Estructura del proyecto ***
*******************************
Las carpetas estarán estructuradas de la siguiente manera:

	- adm : Bloque administrativo

	- log : Bloque logístico

	- ope : Bloque operativo

	- includes: Todos los archivos generales para el funcionamiento del sistema
		-> class: Todas las clases .php (tablas, configuración, etc.)
		-> css: Todas las hojas de estilo generales
		-> js: Todas las librerías .js (jquery, jquery.ui, validate, etc.)
		-> lib: Todas las librerías adicionales php (fpdf, thumbnails, etc)

	- pry_ : Todas las carpetas que sólo estarán disponibles durante el desarrollo del proyecto,
		 y no se incluirán en la distribución del sistema.


*******************************
*** Tecnologia Implementada ***
*******************************
	- Apache 2.2 	http://httpd.apache.org/download.cgi
	- MySQL 5.x 	http://httpd.apache.org/download.cgi
	- PHP 5.x.x 	http://www.php.net/downloads.php


*******************************
***        Creditos        ****
*******************************
@product:   SISP
@version:   2.0.0
@copyright: citisystems
@author: 	  @Mike, @Quirino, @Markinho, @Wakko, @Rana
@website:   http://www.citisystems.com.mx
@email:     admin@citisystems.com.mx