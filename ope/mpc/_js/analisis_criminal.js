$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
    $("#txtFechaAnalisis").datepicker({
        yearRange: 'c-1:c',
    });

    //$("#txtFechaAnalisis").focus();



/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
 //-- Ajusta el tamaño del contenedor del grid...
    $('#dvGridAnalisisCriminal').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...

    //alert(xDcrypt($('#hdnUrlDatos').val()));
    xGrid = $('#dvGridAnalisisCriminal').xGrid({
        xColSort: 1,
        xHeight:50,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 15,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xTypeDataAjax: 'json',
    });
    

/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/
tinymce.init({
        selector: "#txtAreaAnalisisCriminal",
        language : 'es',
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor"
        ],
        toolbar1: "alignleft aligncenter alignright alignjustify | bold italic underline strikethrough | forecolor | bullist numlist | hr removeformat | inserttime",
        menubar: false,
        toolbar_items_size: 'big',
});



/********************************************************************/
/** Convierte a mayúsculas el contenido de los textbox y textarea. **/
/********************************************************************/
$('#txtFechaAnalisis').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
$('#txtAreaAnalisisCriminal').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });



/********************************************************************/
/********* Asignacion de validacion para el formulario **************/
/********************************************************************/

/***** Aplicación de las reglas de validación de los campos del formulario *****/
  $('#frmAnalisisCriminal').validate({
      rules: {
    /***** INICIO de Reglas de validacion para el formulario de xxxxx *****/
            txtFechaAnalisis:{
                required:true,
            },
            txtAreaAnalisisCriminal:{
                required:true,
            },
    /***** FIN de Reglas de validacion para el formulario de xxxxx *****/
     },
     messages:{
    /***** INICIO de Mensajes de validacion para el formulario de xxxxx *****/
            txtFechaAnalisis:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                digits: '<span class="ValidateError" title="Este campo sólo acepta digitos"></span>',
            },
            txtAreaAnalisisCriminal:{
                required: '<span class="ValidateError" title="Este campo es obligatorio></span>',
            },

    /***** FIN de Mensajes de validacion para el formulario de xxxxx *****/
     },
      errorClass: "help-inline",
      errorElement: "span",
      highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
      },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
      }
  /***** Fin de validacion *****/
  });


/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/


/********************************************************************/
/***** Control del formulario para agregar/modifiar             *****/
/********************************************************************/
 $('#dvFormAnalisisCriminal').dialog({
        show: {
            effect: "fade",duration: 500
        },
        hide: {
            effect: "puff",duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        buttons: {
            'Guardar': function(){
                GurdarAnalisisCriminal();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        },
        open: function(){
            resetFormulario();
            if ($('#txtIdAnalisisCriminal').val() == 0) {
                $(this).dialog('option', 'title', 'SISP :: Agregar Analisis Criminal');
            }else{
                $(this).dialog('option', 'title', 'SISP :: Analisis Crimina');
                //datosReferencia();
            }
        }
    });

    $('#btnAgregar').click(function(){
        //$('#hdnIdRef').val('0');
        //$('#hdnTypeOper').val('1');
        $('#dvFormAnalisisCriminal').dialog('open');
    });



/***** Fin del document.ready *****/
});


/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
// Acción del botón Guardar
function GurdarAnalisisCriminal(){
    var valida = $('#frmAnalisisCriminal').validate().form();
    var frmPreg = $(getHTMLMensaje('¿Está seguro de guardar los cambios realizados a Datos Criminales?', 2));
    var mensaje='';  
    var oper = $("#txtIdAnalisisCriminal").val(); 
        
    if (valida) {
        if( oper == '1' ){
                mensaje= 'Confirme la actualización de los datos.';
            }else{
                mensaje='Confirme la alta en sistema.';
            }
            var frmPreg = $(getHTMLMensaje(mensaje, 2));
                 
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 350,
            buttons:{
                'Aceptar': function(){
                    $.ajax({
                            url: xDcrypt( $('#hdnUrlAdd').val() ),   
                            data: $( "#frmAnalisisCriminal" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,            
                            success: function ( xdata ) {                                   
                                if( xdata.rslt ){  
                                    if( oper == '1' ){                                   
                                        $("#dvFormAnalisisCriminal").dialog('close');                                        
                                        xGrid.refreshGrid();       
                                    }else{                                                            
                                        /*$("#txtInventario").focus();
                                        $("#txtInventario").attr("value","");                                                                                                                
                                        $("#txtSerie").attr("value","");
                                        $("#txtModelo").attr("value","");*/                              
                                        $("#xGridBody").html( xdata.html );                                        
                                        xGrid.refreshGrid();                                                                                
                                    }                                    
                                                                                                      
                                }else{
                                    $(this).dialog('close');
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                        
                                }                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });  
                        $(this).dialog('close'); 
                },
                'Cancelar': function(){
                     $(this).dialog('close');
                }
            }
        });
    }
}




/*Reinicia Formulario de la captura de datos criminales*/
function resetFormulario(){
    $('#txtIdAnalisisCriminal').val(0);
    $('#txtFechaAnalisis').val('');
    //Aqui reset al textarea
    //$('#txtAreaAnalisisCriminal').val('');
    

    //Removemos las clases de error por si las moscas
    $('span').removeClass('ValidateError'); //Con esto elimina los iconos de error en la validacion
    $('td').removeClass('error'); //Elimina la clase para quitar el contorno de color rojo
}

