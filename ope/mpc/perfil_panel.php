<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Perfil Criminal
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mpc_perfiles_criminales.class.php';
$objPC=new OpetblMpcPerfilesCriminales();

//se reciben parametros
$id_perfil = $objSys->decrypt($_GET["id"]);
$objPC->select($id_perfil);
//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Perfil Criminal',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="includes/js/tinymce_4.0.20/js/tinymce/tinymce.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/ajaxupload/js/ajaxupload.js"></script>',
                                   '<link type="text/css" href="ope/mpc/_css/mpc.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mpc/_js/perfil_criminal.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//

  $urlEdicion = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('perfil_criminal') . '&id_perfil='. $objSys->encrypt($id_perfil);
  $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
?>
<div id="dvTool-Bar" class="dvTool-Bar">
  <table>
      <tr>
          <td class="tdNombreModulo">
              <?php $plantilla->mostrarNombreModulo(); ?>
          </td>
          <td class="tdBotonesAccion">
              <a href="<?php echo $urlEdicion?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Editar Informaci�n Principal del Perfil Criminal...">
                  <img src="<?php echo PATH_IMAGES;?>icons/edit24.png" alt="" style="border: none;" /><br />Edicion
              </a>
              
              <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Regresar al Listado de Perfiles Criminales...">
                  <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
              </a>
          </td>
      </tr>
  </table>
</div>

  <form id="frmPanel" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Panel" class="dvForm-Data">
            <span class="dvForm-Data-pTitle">
                <img src="<?php echo PATH_IMAGES;?>icons/add.png" class="icono"/>
                  Perfil Crimininal
            </span>

    <!-- Contenido del Formulario -->
<fieldset id="fsetopetbl_mid_incidentes_panel" class="fsetForm-Data">
     <!-- <legend>Informacion del Incidente</legend> -->
  <div>
   <table>
   	<tr>
      	<td>
          <div  id="divPanelInfo">
          <table id="tblPanelInfo">
            <tr>
              <td class="descripcion" style="border-top:none;">Perfil No.:</td>
              <td class="informacion" style="border-top:none;  color:#F40006; font-weight:bold; fon;t-size: 18px;">
               <?php echo $objPC->id_perfil_criminal; ?>
              </td>
              <td rowspan="10"  style="text-align: center; vertical-align: top;">
            <div id="fotografia" class="fotografia" title="fotografia">
                <img id="fotoimg" src="ope/mpc/fotografias/<?php
                                        if(!empty($objPC->id_perfil_criminal) && file_exists("ope/mpc/fotografias/".$objPC->fotografia_frente)){
                                            echo $objPC->fotografia_frente;
                                        }else{
                                            echo "sin_fotografia.png";
                                        }
                                    //echo $objPC->id_perfil_criminal; 
                                ?>" alt="" />
            </div>
            </tr>
            <tr>
              <td class="descripcion">Situaci�n:</td>
              <td class="informacion">
                <?php 
                    $objPC->OpecatMpcSituaciones->select($objPC->id_situacion);
                    echo $objPC->OpecatMpcSituaciones->situacion;
                ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Nombre:</td>
              <td class="informacion">
                <strong><?php echo $objPC->nombre . " " . $objPC->apellido_paterno ." ". $objPC->apellido_materno; ?></strong>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Alias:</td>
              <td class="informacion">
                <?php echo $objPC->alias; ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Sexo:</td>
              <td class="informacion">
                <?php echo $objPC->sexo; ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Localidad:</td>
              <td class="informacion">
                <?php 
                    $objPC->OpecatMpcNacionalidades->select($objPC->id_nacionalidad);
                    echo $objPC->OpecatMpcNacionalidades->nacionalidad;
                ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Nacionalidad:</td>
              <td class="informacion">
                <?php 
                    $objPC->OpecatMpcNacionalidades->select($objPC->id_nacionalidad);
                    echo $objPC->OpecatMpcNacionalidades->nacionalidad;
                ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Lugar de Origen:</td>
              <td class="informacion">
                <?php echo $objPC->lugar_origen; ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Complexi�n:</td>
              <td class="informacion">
                <?php echo $objPC->complexion; ?>
              </td>
              </tr>
            <tr>
              <td class="descripcion">Se�as Particulares:</td>
              <td class="informacion">
                <?php 
                    $particulares .= "<strong>ESTATURA:</strong>" . $objPC->estatura . "; ";
                    $particulares .= "<strong>PESO:</strong>" . $objPC->Peso . ";<br>";
                    $particulares .= "<strong>CABELLO:</strong>" . $objPC->cabello . "; ";
                    $particulares .= "<strong>PIEL:</strong>" . $objPC->colorPiel;
                    echo $particulares
                ?>
              </td>
              </tr>
          </table>
          </div>
          </td>
          <td style="text-align: center; vertical-align: top;">
            <!--<div id="fotografia" class="fotografia" title="Haga click aqui para buscar una fotografia.">

            </div>-->
          </td>
          <td style="text-align: center; vertical-align: top;">
            <table>
               <tr>
                <td class="botones">
                  <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('datos_criminales') . "&id=" . $objSys->encrypt($id_perfil); ?>" class="BotonPanel" title="Agregar Datos Criminales de la Persona">
                  <img src="<?php echo PATH_IMAGES;?>icons/Jerarquia64.png" alt="Dato Criminales ..." />
                  <br />Dat. Criminales<br />
                  <div class="numeroCentro" title="Numero de Datos Criminales"><?php echo str_pad($objPC->TotalDatosCriminales($id_perfil),2,"0",STR_PAD_LEFT);  ?></div>
                </a>
               </td>
               <td class="botones">
                  <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('antecedentes_criminales'). "&id=" . $objSys->encrypt($id_perfil); ?>" class="BotonPanel" title="Agregar Antecedentes Criminales  e la Persona">
                  <img src="<?php echo PATH_IMAGES;?>icons/antededentes64.png" alt="Antecedentes Criminales ..." />
                  <br />Antecedentes<br />
                  <div class="numeroCentro" title="Numero de Antecdenetes Criminales"><?php echo str_pad($objPC->TotalAntecedentesCriminales($id_perfil),2,"0",STR_PAD_LEFT); ?></div>
                </a>
               </td>
               </tr>
               <tr>
                 <td class="botones">
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('analisis_criminal') . "&id=" . $objSys->encrypt($id_perfil); ?>" class="BotonPanel" title="Agregar Analisis de Investigacion Criminal">
                        <img src="<?php echo PATH_IMAGES;?>icons/Analisis64.png" alt="Analisis Criminal ...." />
                        <br />An�lisis<br />
                    <div class="numeroCentro" title="Numero de Analisis Realizados"><?php echo str_pad($objPC->TotalAnalisisCriminales($id_perfil),2,"0",STR_PAD_LEFT); ?></div>
                 </td>
                 <td class="botones">
                        <a href="" class="BotonPanel" style="display:none;" title="">
                        <br /><br />
                </td>
               </tr>
             </table>
          </td>
      <tr>
   </table>

  <hr/>
  
</form>

<?php
  //-----------------------------------------------------------------//
  //-- Bloque de cerrado de la plantilla...
  //-----------------------------------------------------------------//
$plantilla->paginaFin();
?>