<?php
/**
 * Complemento del llamado ajax para cargar un archivo en una carpeta tempora del servidor.
 * Lista de parámetros recibidos por POST
 * @param FILE uploadfile, contiene el texto especificado por el usuario para una búsqueda.
 */
    session_start();
    if (isset($_SESSION['admitted_xsisp'])) {
        if(!$_FILES['userfile']['error']){
            $uploaddir = '../uploads/'; // change this
            $file = $_FILES['userfile'];
            $ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);
            $fileName =uniqid(time(), false).'.'.$ext; //mt_rand().'.'.$ext;
            $destinationfile = $uploaddir . $fileName;
            //echo $destinationfile;
            if(move_uploaded_file($_FILES['userfile']['tmp_name'], $destinationfile)){
                echo $fileName;
            } else {
                echo 'upload_error';
            }
        } else {
            echo 'upload_error';
        }
    }
?>