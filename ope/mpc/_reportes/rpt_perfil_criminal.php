<?php
session_start();

//function exportarPDF($archivoHTML,$nombrePDF){
    // get the HTML
    ob_start();
    include('puebaPDF.php');
    $content = ob_get_clean();

    // convert in PDF
     require_once('html2pdf/html2pdf.class.php');

    try
    {
        $html2pdf = new HTML2PDF('L', 'Letter', 'es', true, 'UTF-8', array(10, 5, 10, 5));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->writeHTML('<strong>'.$seccion.'</strong>');
        $html2pdf->Output("nombrePDF" . '.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
      
//} 
?>