<?php
/**
 * Complemento ajax para guardar un archivo multimedia del incidente actual 
 *  
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_multimedia.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objMult = new OpetblMidIncidentesMultimedia();
    
    $id_incidente = $_SESSION["xIdIncidente"];
        
    $path_upload = '../_upload/';
    $path_exped = '../_multimedia/';
    //Si la carpeta del incidente no existe, entonces la crea...
    $band_dir = true;
    if( !is_dir($path_exped.$id_incidente) ){		 
		if( !mkdir($path_exped.$id_incidente, 0777) )
			$band_dir = false;
	}
    if( $band_dir ){
        // Se asignan los valores a los campos
        $objMult->id_multimedia     = 0;
        $objMult->id_folio_incidente= $id_incidente;        
        $objMult->descripcion       = utf8_decode($_POST["txtDescripcion"]);
        // Se procesa el nombre del archivo original
        $objMult->archivo           = utf8_decode($_POST["txtNombreArchivo"]);
        // Se obtiene la extensi�n del archivo ubicado en la carpeta temporal
        $pos_ext = strrpos($_POST["hdnFileMultimed"], '.');
        $ext = substr($_POST["hdnFileMultimed"], ($pos_ext + 1));
        $ext = strtolower($ext);
        // Se obtiene y asigna el id del tipo de archivo multimedia
        $objMult->OpecatMidMultimediaTipo->getIcon($ext);
        $objMult->id_multimedia_tipo = $objMult->OpecatMidMultimediaTipo->id_multimedia_tipo;
        // Se genera la ruta completa de la carpeta correspondiente al incidente actual
        $path_exped .= $id_incidente . '/';
        if( $result = $objMult->insert() ) {
            $nvo_id_reg = $result;
            // Se genera el nombre con que se guardar� la imagen del documento
            $name_file = $id_incidente . '_' . $nvo_id_reg;
            // Se pasa(n) los(el) archivo(s) a la carpeta correspondiente
            copy($path_upload . $_POST["hdnFileMultimed"], $path_exped . $name_file . '.' . $ext);            
            // Se elimina(n) los(el) archivo(s) temporal(es)
            unlink($path_upload . $_POST["hdnFileMultimed"]);
            // Si el archivo multimedia es una imagen
            $pos = strpos("jpg,jpeg,png,gif,bmp", $ext);
            if( $pos !== false ){
                // Se obtiene el nombre del archivo thb                        
                $file_thb = substr($_POST["hdnFileMultimed"], 0, $pos_ext) . '-thb.' . $ext;
                copy($path_upload . $file_thb, $path_exped . $name_file . '-thb.' . $ext);
                unlink($path_upload . $file_thb);
            }
                        
            // Se registra el log
            $objSys->registroLog($objUsr->idUsr, 'opetbl_mid_incidentes_multimedia', $id_incidente . '-' . $nvo_id_reg, "Ins");
            /*
            // Se asignan los valores que seran devueltos
            $ajx_datos['file0']  = $path_upload . $_POST["hdnFileMultimed"];
            $ajx_datos['file1']  = $path_exped . $name_file . $ext;
            $ajx_datos['file2']  = $path_exped . $name_file . '-thb' . $ext;
            */
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objExped->msjError;
        }
    } else {
        $ajx_datos["rslt"] = false;
        $ajx_datos["error"] = "No se pudo crear la carpeta del incidente actual";
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>