<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');    
    //-- Bloque de inclusi�n de las clases...
    $path = '../../../';    
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_personas.class.php';
    $objUsr  = new Usuario(); 
    $objSys  = new System();
    $objIncidPer = new OpetblMidIncidentesPersonas();
   
    // datos de la persona
    $idPersona = ( $_POST["hdnIdPersona"] > 0 ) ? $_POST["hdnIdPersona"] : 0;
    $objIncidPer->id_persona            = $idPersona;
    $objIncidPer->id_folio_incidente    = $_SESSION["xIdIncidente"];
    $objIncidPer->id_persona_rol        = $_POST["cbxIdRolPersona"];    
    $objIncidPer->id_persona_causa      = $_POST["cbxIdCausaPersona"];
    $objIncidPer->nombre                = utf8_decode($_POST["txtNombre"]);
    $objIncidPer->apellido_paterno      = utf8_decode($_POST["txtApellidoPaterno"]);
    $objIncidPer->apellido_materno      = utf8_decode($_POST["txtApellidoMaterno"]);
    $objIncidPer->edad                  = ( !empty($_POST["txtEdad"]) ) ? $_POST["txtEdad"] : null;
    $objIncidPer->sexo                  = ( !empty($_POST["rbnSexo"]) ) ? $_POST["rbnSexo"] : null;
    $objIncidPer->domicilio             = utf8_decode($_POST["txtAreaDomicilio"]);
    
    if( $_POST["hdnTypeOper"] == 1 ){
        $result = $objIncidPer->insert();
        $oper = "Ins";
    } else {
        $result = $objIncidPer->update();
        $oper = "Edt";
    }
    if ($result) {
        if( $_POST["hdnTypeOper"] == 1 ){
            $id_reg = $result;
        } else {
            $id_reg = $idPersona;
        }
        $objSys->registroLog($objUsr->idUsr, "opetbl_mid_incidentes_personas", $id_reg, $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['error'] = '';
        $_SESSION["xIdPersona"] = $idPersona;
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objIncidPer->msjError;
    }   
          
    echo json_encode($ajx_datos);

}else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>