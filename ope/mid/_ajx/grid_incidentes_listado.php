<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par?metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b?squeda.
 * @param int colSort, contiene el ?ndice de la columna por la cual se ordenar?n los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n?mero de fila por la cual inicia la paginaci?n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar?n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes.class.php';
    $objSys = new System();
    $objDataGridIncidentes = new OpetblMidIncidentes();

    //--------------------- Recepci?n de par?metros --------------------------//
    // B?squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {

        $sql_where = 'i.id_folio_incidente LIKE ? '
                   . 'OR i.prioridad LIKE ? '
                   . 'OR r.region LIKE ? '
                   . 'OR c.cuartel LIKE ? '
                   . 'OR a.asunto LIKE ?'
                   . 'OR f.fuente LIKE ?'
                   . 'OR i.hechos_text LIKE ?';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }
    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'i.prioridad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
         $sql_order = 'i.id_folio_incidente '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'i.fecha_incidente ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'r.region ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'c.cuartel ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'a.asunto ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 7) {
        $sql_order = 'f.fuente ' . $_GET['typeSort'];
    }
    // Paginaci?n...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridIncidentes->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $dato["id_folio_incidente"]; //$objSys->encrypt($dato["id_folio_incidente"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_folio_incidente"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 1%;">';
                // $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 2%;"><img src="includes/css/imgs/icons/'. $dato["prioridad"] .'.png"/></td>';
                $html .= '<td style="text-align: center; width: 5%; font-size:13px; color:#FF0000;"><strong>'.$dato["id_folio_incidente"].'</strong></td>';
               //<a href="#" class="toolTipTrigger" rel="' . $id_crypt . '">' . $dato["id_folio_incidente"] . '</a></td>';
                $html .= '<td style="text-align: center; width: 6%;">' . $dato["fecha_incidente"] . '</td>';
                $html .= '<td style="text-align: center; width: 6%;">' . $dato["region"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["cuartel"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["asunto"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["fuente"] . '</td>';
                $html .= '<td style="text-align: left; width: 35%;" title="'. str_replace('"','*',$dato["hechos_text"]).'">' . substr($dato["hechos_text"],0,150) .' ... </td>';

                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("incidentes_panel") . "&id_folio_incidente=" . $objSys->encrypt($id_crypt);
                $url_del = "ope/mid/_reportes/reporte_incidente.pdf";
                $html .= '<td style="text-align: center; width: 7%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Editar Incidente ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="' . $url_del . '" target="_blank" class="lnkBtnOpcionGrid" title="Imprimir Incidente ..." ><img src="' . PATH_IMAGES . 'icons/pdf24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//... '.$dato["hechos_text"] .'
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de incidentes ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi?n...";
}
?>