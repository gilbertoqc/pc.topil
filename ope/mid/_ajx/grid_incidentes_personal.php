<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par?metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b?squeda.
 * @param int colSort, contiene el ?ndice de la columna por la cual se ordenar?n los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n?mero de fila por la cual inicia la paginaci?n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar?n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_personal.class.php';
    $objSys = new System();
    $objDataGridPersonal = new OpetblMidIncidentesPersonal();

    //--------------------- Recepci?n de par?metros --------------------------//
    // B?squeda...
    $sql_where = 'a.id_folio_incidente=' . $_SESSION["xIdIncidente"] .' ';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where .= 'AND ((b.nombre LIKE ? OR b.a_paterno LIKE ? OR b.a_materno LIKE ?) '
                    . 'OR b.cuip LIKE ? '
                    . 'OR d.categoria LIKE ? '
                    . 'OR e.nivel_mando LIKE ? '
                    . 'OR f.status LIKE ?)';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }
    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
         $sql_order = 'b.nombre, b.a_paterno, b.a_materno ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'b.cuip ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'd.categoria ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'e.nivel_mando ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'f.status ' . $_GET['typeSort'];
    }
    // Paginación...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridPersonal->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_reg = $dato["curp"];
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["curp"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 4%;">';
                    $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 25%;">' . $dato["nombre"] .' '. $dato["a_paterno"] .' '. $dato["a_materno"] . '</td>';
                $html .= '<td style="text-align: center; width: 19%;">' . $dato["cuip"] . '</td>';
                $html .= '<td style="text-align: left; width: 18%;">' . $dato["categoria"] . '</td>';
                $html .= '<td style="text-align: center; width: 12%;">' . $dato["nivel_mando"] . '</td>';
                $html .= '<td style="text-align: center; width: 12%;">' . $dato["status"].'</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" rel="' . $id_reg . '" class="lnkBtnOpcionGrid" title="Excluir a este elemento de la lista..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de personal operativo...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi?n...";
}
?>