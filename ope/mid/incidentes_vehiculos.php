<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
 include 'includes/class/opetbl_mid_incidentes_vehiculos.class.php';
 $objDataGridVehiculos = new OpetblMidIncidentesVehiculos();

//se reciben parametros
//id_folio_incidente
$id_folio_incidente = $objSys->decrypt( $_GET["id_folio_incidente"] );

//-----------------------------------------------------------------//
//-- Bloque de definici?n de par?metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidente vehiculos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes_vehiculos.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('incidentes_panel') . "&id_folio_incidente=" . $_GET["id_folio_incidente"];  

?>
  <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>
                <td class="tdNombreModulo" style="width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion" style="width: 50%;">
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                    <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="width: 80px;" title="Agregar vehiculo involucrado...">
                      <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                  </a>
                </td>
            </tr>
        </table>
  </div>

<div id="dvForm-Vehiculos" class="dvForm-Data" style="border: none; height: 500px; margin: auto auto; margin-top: 10px; width: auto;">
    <span class="dvForm-Data-pTitle">
        <img src="<?php echo PATH_IMAGES;?>icons/vehiculos24.png" class="icono"/>
         Vehiculos
    </span>

    <!--****** INICIO TABLA DE DATOS  *****-->
<div id="dvGridVehiculos" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 3%;" class="xGrid-tbCols-ColSortable">ID</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ROL</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">MARCA</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">MODELO</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">PLACAS</th>
                    <th style="width: 13%;" class="xGrid-tbCols-ColSortable">NUMERO SERIE</th>
                    <th style="width: 13%;" class="xGrid-tbCols-ColSortable">NUMERO MOTOR</th>

                    <th style="width: 9%;" class="xGrid-tbCols-ColSortable"></th>
                  </tr>
              </table>
             </div>
             <div class="xGrid-dvBody"  style="min-height: 100px;">
                <table class="xGrid-tbBody">
                   
                </table>
             </div>
    </div>
    <!--****** FIN TABLA DE DATOS  ******-->
</div>

    <!-- CONTENEDOR PARA EL FORMULARIO DE VEHICULOS AGREGADOS ------------- -->
    <div id="dvFormIncidentesVehiculos" style="display: none;" title="x-">    
        <div id="dvIncVehCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 300px; min-width: 500px; overflow-y: hidden;"></div>
    </div>


    <input type="hidden" id="hdnUrlDatos" name="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mid/_ajx/grid_incidentes_vehiculos.php'); ?>" />
    <input type="hidden" id="hdnUrlVeh" name="hdnUrlVeh" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_frm_vehiculos.php'); ?>" />
    <input type="hidden" id="hdnUrlSave" name="hdnUrlSave" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_reg_vehiculos.php'); ?>" />    
    <input type="hidden" id="id_vehiculo" value="" />
    <input type="hidden" id="id_folio_incidente" name="id_folio_incidente" value="<?php echo $_GET["id_folio_incidente"]; ?>" />

<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
