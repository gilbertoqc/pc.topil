var hWind = 0,
    wWind = 0,
    cancelUpload = false,
    loadComplete = false;
$(document).ready(function(){
    // Se obtienen las medidas de la ventana general
    hWind = $(window).height();
    wWind = $(window).width();
    
    /********************************************************************/
    /* Controla el scroll para seguir mostrando el header y el toolbar. */
    /********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });
    
    /********************************************************************/
    /** Convierte a may�sculas el contenido de los textbox y textarea. **/
    /********************************************************************/
    $('#txtDescripcion, #txtArchivo').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });

    /*
     * Funciones para el control de carga de im�genes
     */
    new AjaxUpload('#btnAgregar', {
        action: xDcrypt($('#hdnUrlUploadFile').val()),        
		onSubmit : function(file, ext){
            loadComplete = false;
            var tipo_val = $('#hdnTypeFile').val().indexOf(ext);            
  		    if ( tipo_val < 0 ){
  			   // extensiones permitidas
  			   shwError('Solo se permiten archivos multimedia (' + $('#hdnTypeFile').val() + ')', 450);
  			   // cancela upload
  			   return false;
  		    } else {
  		        $('#dvPrevImg').html('<span class="spnLoading"></span>');
                $('#dvForm-Add').dialog('open'); 
  		    }
        },
		onComplete: function(file, response){
            var result = $.parseJSON(response);
            if( !result.rslt ){
                $('#dvForm-Add').dialog('close');
                shwError(result.error, 400);
            } else {
                loadComplete = true;
                $('#hdnFileMultimed').val(result.file);
                $('#txtNombreArchivo').val(file);
                var path_file = ( result.path == '' ) ? xDcrypt($('#hdnUrlDirTempUpload').val()) : result.path;
                var imgHtml = '<img src="' + path_file + result.file_thb + '" />';
                $('#dvPrevImg').html(imgHtml);
            }
        }
    });

    /********************************************************************/
    /********* Asignacion de validacion para los controles **************/
    /********************************************************************/
    /***** Aplicaci�n de las reglas de validaci�n de los campos del formulario *****/
    $('#frmAddFile').validate({
        rules: {
            /***** INICIO de Reglas de validacion para el formulario *****/
            /*
            txtIdMultimedia:{
                required:true,digits:true,
            },
            cbxIdTipoMultimedia:{
                required:true,min: 1,
            },
            */
            txtNombreArchivo:{
                required:true,
            },
            txtDescripcion:{
                required:true,
            },           
            /***** FIN de Reglas de validacion para el formulario *****/
        },
        messages:{
            /***** INICIO de Mensajes de validacion para el formulario *****/
            /*
            txtIdMultimedia:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
            },
            cbxIdTipoMultimedia:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            */
            txtNombreArchivo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtDescripcion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            }
        /***** FIN de Mensajes de validacion para el formulario *****/
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('error').addClass('success');
        }
    /***** Fin de validacion *****/
    });

    /********************************************************************/
    /***** Control del formulario para agregar/modifiar una persona *****/
    /********************************************************************/
    $('#dvForm-Add').dialog({
        /*
        show: {
            effect: "fade", duration: 500
        },
        hide: {
            effect: "puff", duration: 500
        },
        */
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 800,
        open: function(){
            resetFormulario();
            $('#txtDescripcion').focus();
        },
        buttons: {
            'Guardar Multimedia': function(){
                cancelUpload = false;
                if( loadComplete )
                    guardarMultimedia();
                else{
                    var frmMsj = $(getHTMLMensaje('El archivo a�n no se ha cargado completamente, por favor sea paciente y espere a que el archivo termine de cargarse', 1));
                    frmMsj.dialog({
                        autoOpen: true,
                        modal: true,
                        width: 450,
                        buttons:{
                            'Aceptar': function(){
                                frmMsj.dialog('close');
                            }
                        }
                    });
                }
            },
            'Cancelar': function(){
                cancelUpload = true;
                $(this).dialog('close');
            }
        },
        beforeClose: function(){
            //-- Se elimina el archivo previamente cargado...
            if( cancelUpload )
                borrarImgTemp();
        }
    });

    $('#dvListaContMult a.btnDeleteFile').live('click', function(){
        var xId = $(this).attr("rel");
        var frmPreg = $(getHTMLMensaje('�Est� seguro de eliminar el registro seleccionado?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){    
                    frmPreg.dialog('close');
                    borrarMultimedia(xId);
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    });
    
    //--- Vista pevia o descarga del contenido multimedia
    $('#dvForm-Prev').dialog({
        autoOpen: false,
        draggable: true,
        height: hWind - 100,
        modal: true,        
        resizable: false,
        width: 1020,
    });
    $('#dvListaContMult div.dvPreviewFile>a').live('click', function(){
        var xId = $(this).attr("rel").split('-');
        var titulo = $(this).attr('title');
        switch(xId[1]){
            case 'jpg':        
            case 'png':
            case 'bmp':
            case 'gif':
            case 'jpeg':                
            case 'mp4':
            case 'ogg':       
            case 'mp3':
            case 'pdf': 
                $('#dvForm-Prev').dialog({
                    open: function(){
                        $('#dvShowFile').load(xDcrypt($('#hdnUrlLoadFile').val()), {'id': xId[0], 'h': (hWind - 180)});
                    },
                    close: function(){
                        $('#dvShowFile').empty();
                    }
                });
                $('#dvForm-Prev').dialog('option', 'title', titulo);
                $('#dvForm-Prev').dialog('open');
                break;
            case '3gp': 
            case 'mpeg':
            case 'avi':
            case 'wmv':
            case 'flv':
            case 'amr':
            case 'doc':
            case 'docx':
            case 'xls':
            case 'xlsx':
            case 'ppt':
            case 'pptx':
            case 'pps':
            case 'ppsx':
                location.href = xDcrypt($('#hdnUrlLoadFile').val()) + '?id=' + xId[0];
                break;
        }
    });
/***** Fin del document.ready *****/
});


function guardarMultimedia(){
    var valida = $('#frmAddFile').validate().form();
    if (valida) {
        /*
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar el archivo actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
        */
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Guardando, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSaveFile').val()),
                        data: $('#frmAddFile').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        beforeSend: function () {
                            //frmPreg.dialog('close');
                            // D�alogo de espera
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            });
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                location.reload();
                                /*
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGridIncidentesMultimedia div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGridIncidentesMultimedia table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGridIncidentesMultimedia table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvForm-Add').dialog('close');
                                */
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
        /*
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
        */
    }
}

function borrarImgTemp(){
    $.ajax({
        url: xDcrypt($('#hdnUrlDltFileTemp').val()),
        data: {'file': $('#hdnFileMultimed').val()},
        type: 'post',
        dataType: 'html',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

function borrarMultimedia(id){
    // Se genera el di�logo de espera
    var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Eliminando, por favor espere...</div></div>');
    $.ajax({
        url: xDcrypt($('#hdnUrlDltFile').val()),
        data: {'id': id},
        type: 'post',
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            frmWait.dialog({
                autoOpen: true,
                modal: true,
                width: 400,
            });
        },
        success: function (xdata) {
            // Cierra el di�logo de espera
            frmWait.dialog('close');
            if( xdata.rslt ){
                location.reload();
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

function resetFormulario(){
    $('#txtIdMultimedia').val(0);
    $('#cbxIdTipoMultimedia').val(0); 
    $('#txtArchivo').val('');
    $('#txtDescripcion').val('');
    $('#fileUpdArchivo').val('');

    //Removemos las clases de error por si las moscas
    $('span').removeClass('ValidateError'); //Con esto elimina los iconos de error en la validacion
    $('td').removeClass('error'); //Elimina la clase para quitar el contorno de color rojo
}