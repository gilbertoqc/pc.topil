$(document).ready(function(){

    //latitud 17.551535
    //longitud -99.500632
    //chilpo

    $('#mapaIncidente').locationpicker({        
        location: { latitude: $('#txtLatitud').val() , longitude: $('#txtLongitud').val() },
        locationName: $('#txtDireccion').val() ,
        radius: 200,
        zoom: 15,
        scrollwheel: true,
        inputBinding: {
            latitudeInput: $('#txtLatitud'),
            longitudeInput: $('#txtLongitud'),
            radiusInput: $('#txtRadio'),
            locationNameInput: $('#txtDireccion'),
            icono:"police_maker.png"
        },
        enableAutocomplete: false,
        enableReverseGeocode: true
    });
    
});//fin de document.ready