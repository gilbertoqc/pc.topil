var xGridPer, 
    xGridPol,
    xGridBuscaPol;
$(document).ready(function(){
    // Control de tabs
    $('#tabs').tabs({
        active: 0,
        activate: function( event, ui ) {
            
        }
    });
    
    //--------------------------------------------------------------------//
    //------------- Asignacion de funcionalidad a controles --------------//
    //--------------------------------------------------------------------//
    //-- Crea e inicializa el control de los grids...
    xGridPer = $('#dvGridPer').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 1,
        xUrlData: xDcrypt($('#hdnUrlDatosPer').val()),
        xTypeDataAjax: 'json'
    });
    
    xGridPol = $('#dvGridPol').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 1,
        xRowsDisplay: 10,
        xTypeSelect: 1,
        xUrlData: xDcrypt($('#hdnUrlDatosPol').val()),
        xTypeDataAjax: 'json'
    });
    
    //--------------------------------------------------------------------//
    //-- Convierte a may�sculas el contenido de los textbox y textarea. --//
    //--------------------------------------------------------------------//
    $('#txtNombre,#txtApellidoPaterno,#txtApellidoMaterno,#txtEdad,#txtAreaDomicilio').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    
    //-- Se filtra el listado de causas en base al rol seleccionado...
    $('#cbxIdRolPersona').change(function(){
        obtenerCausas($(this).val());
    });

    //--------------------------------------------------------------------//
    //---------- Asignacion de validacion para los controles -------------//
    //--------------------------------------------------------------------//
    //-- Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmPersona').validate({
        rules: {
            cbxIdRolPersona:{
                required:true,min:1,
            },
            cbxIdCausaPersona:{
                required:true,min:1,
            },
            txtNombre:{
                required:true,
            },            
        },
        messages:{
            cbxIdRolPersona:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxIdCausaPersona:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtNombre:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('error').addClass('success');
        }
    });

    //--------------------------------------------------------------------//
    //---- Control del formulario para agregar/modifiar una persona ------//
    //--------------------------------------------------------------------//
    $('#dvFormPersona').dialog({
        hide: {
            effect: "puff",duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetFormulario();
            if ($('#hdnIdPersona').val() == 0) {
                $(this).dialog('option', 'title', 'SISP :: Agregar Persona');
            } else{
                $(this).dialog('option', 'title', 'SISP :: Modificar Persona');
                datosPersona();
            }
        },
        buttons: {
            'Guardar datos': function(){
                guardarPersona();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });

    $('#btnAddPer').click(function(){
        $('#hdnIdPersona').val('0');
        $('#hdnTypeOper').val('1');
        $('#dvFormPersona').dialog('open');
    });

    $('#dvGridPer a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel").split('-');
        $('#hdnIdPersona').val( xId[1] );
        if( xId[0] == 'edt' ){
            $('#hdnTypeOper').val('2');
            $('#dvFormPersona').dialog('open');
        } else if( xId[0] == 'dlt' ){
            eliminarPersona();
        }
    });
    
    //--------------------------------------------------------------------//
    //-- Funcionalidad de la b�squeda y selecci�n de personal operativo --//
    //--------------------------------------------------------------------//
    //-- Grid...
    xGridBuscaPol = $('#dvGrid-BuscaPol').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart: 0,
        xRowsDisplay: 10,
        xTypeSelect: 1,
        xUrlData: xDcrypt($('#hdnUrlGridPol').val()),
        xTypeDataAjax: 'json',
        xTypeSearch: 2,
    });
    //-- Ventana de d�alogo...
    $('#dvFormBuscaPersonal').dialog({
        hide: {
            effect: "puff",duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: true,
        width: 970,
        open: function(){
            
        },
        buttons: {
            '- Terminar -': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Bot�n para buscar y seleccionar un elemento...
    $('#btnAddPol').click(function(){
        $('#dvFormBuscaPersonal').dialog('open');
    });
    //-- Controla el click de selecci�n de un registro...
    $('#dvGrid-BuscaPol a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        agregarPersonal(xId);
    });
    //-- Controla el click de selecci�n de un registro...
    $('#dvGridPol a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        excluirPersonal(xId);
    });
});

function guardarPersona(){
    var valida = $('#frmPersona').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos actuales?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    $(this).dialog('close'); 
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Guardando, por favor espere...</div></div>');                                        
                    $.ajax({
                        url: xDcrypt( $('#hdnUrlSavePer').val() ),
                        data: $('#frmPersona').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                   
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el dialog de espere
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso                             
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de personas
                                xGridPer.refreshGrid();
                                // Se cierra el formulario principal
                                $('#dvFormPersona').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmPreg.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosPersona(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetPer').val()),
        data: {'id': $('#hdnIdPersona').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function (){

        },
        success: function (xdata) {
            if (xdata.rslt) {
                $('#cbxIdRolPersona').val(xdata.id_persona_rol);
                obtenerCausas(xdata.id_persona_rol);
                $('#cbxIdCausaPersona').val(xdata.id_persona_causa);
                $('#txtNombre').val(xdata.nombre);
                $('#txtApellidoPaterno').val(xdata.apellido_paterno);
                $('#txtApellidoMaterno').val(xdata.apellido_materno);
                $('#txtEdad').val(xdata.edad);
                if( xdata.sexo == 1 )
                    $('input:radio[name=rbnSexo][value=1]').attr('checked', true);
                else
                    $('input:radio[name=rbnSexo][value=2]').attr('checked', true);
                $('#txtAreaDomicilio').val(xdata.domicilio);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function eliminarPersona(){
    var frmPreg = $(getHTMLMensaje('�Est� seguro de eliminar el registro de esta persona?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 380,
        buttons:{
            'Aceptar': function(){
                // Se genera el di�logo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Eliminando, por favor espere...</div></div>'); 
                $.ajax({
                    url: xDcrypt($('#hdnUrlDltPer').val()),
                    data: {'id': $('#hdnIdPersona').val()},
                    dataType: 'json',
                    async: false,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // D�alogo de espera                   
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        });
                    },
                    success: function (xdata) {
                        // Cierra el dialog de espere
                        frmWait.dialog('close');
                        if (xdata.rslt)
                            xGridPer.refreshGrid();
                        else
                            shwError(xdata.error);
                    },
                    error: function(objeto, detalle, otroobj){
                        frmPreg.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });            
}

function resetFormulario(){
    $('#cbxIdRolPersona').val(0);
    $('#cbxIdCausaPersona').val(0);
    $('#txtNombre').val('');
    $('#txtApellidoPaterno').val('');
    $('#txtApellidoMaterno').val('');
    $('#txtEdad').val('');
    $('input:radio[name=rbnSexo]').attr('checked',false);
    $('#txtAreaDomicilio').val('');
    
    //Removemos las clases de error por si las moscas
    $('span').removeClass('ValidateError'); //Con esto elimina los iconos de error en la validacion
    $('td').removeClass('error'); //Elimina la clase para quitar el contorno de color rojo
}

function obtenerCausas(id){
    $.ajax({
        url: xDcrypt($('#hdnUrlCausas').val()),
        data: {'id_rol': id},
        dataType: 'json',
        async: false,
        cache: false,
        beforeSend: function () {
            $('#cbxIdCausaPersona').html('<option>Cargando causas...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxIdCausaPersona').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

//------------------------------------------------//
function agregarPersonal(id){
    // Se genera el di�logo de espera
    var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Agregando el registro, por favor espere...</div></div>');
    $.ajax({
        url: xDcrypt($('#hdnUrlProcesPol').val()),
        data: {'id': id, 'tp': 1},
        type: 'post',
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            // D�alogo de espera                   
            frmWait.dialog({
                autoOpen: true,
                modal: true,
                width: 400,
            });
        },
        success: function (xdata) {
            // Cierra el dialog de espere
            frmWait.dialog('close');
            if (xdata.rslt){
                xGridPol.refreshGrid();
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

function excluirPersonal(id){
    var frmPreg = $(getHTMLMensaje('�Est� seguro de excluir el registro de este elemento?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 380,
        buttons:{
            'Aceptar': function(){
                // Se genera el di�logo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData" style="margin-top: 20px; padding-top: 50px;">Eliminando el registro, por favor espere...</div></div>');
                $.ajax({
                    url: xDcrypt($('#hdnUrlProcesPol').val()),
                    data: {'id': id, 'tp': 2},
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // D�alogo de espera                   
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        });
                    },
                    success: function (xdata) {
                        // Cierra el dialog de espere
                        frmWait.dialog('close');
                        if (xdata.rslt){
                            xGridPol.refreshGrid();
                        } else {
                            shwError(xdata.error);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        frmPreg.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });
}