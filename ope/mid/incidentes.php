<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//
//getIdUltimoIncidente()
include 'includes/class/opetbl_mid_incidentes.class.php';
include 'includes/class/ope_funciones.class.php';

$objInc             =   new OpetblMidIncidentes;
$objFunciones       =   new Ope_Funciones;

//se recibe el id encriptado
$id_folio_incidente = $objSys->decrypt( $_GET["id_folio_incidente"] );

if( !empty( $id_folio_incidente ) ){
    $objInc->select( $id_folio_incidente );
    $oper=1;    
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('incidentes_panel')  . '&id_folio_incidente=' . $_GET["id_folio_incidente"];
}else{
    $id_folio_incidente=0;
    $oper=2;
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
}
//-----------------------------------------------------------------//
//-- Bloque de definicion de par?metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidentes',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="includes/js/tinymce_4.0.20/js/tinymce/tinymce.min.js"></script>',
                                   '<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//

$urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_rg');
$urlNext = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_panel');
?>

<div id="dvTool-Bar" class="dvTool-Bar">
  <table>
    <tr>
      <td class="tdNombreModulo">
          <?php $plantilla->mostrarNombreModulo();?>
      </td>
      <td class="tdBotonesAccion">
          <!--
          <a href="<?php //echo $urlNext?>" id="btnSiguiente" class="Tool-Bar-Btn gradient" style="width: 60px;" title="Siguiente">
              <img src="<?php //echo PATH_IMAGES;?>icons/next24.png" alt="" style="border: none;" /><br />Siguiente
          </a>
          
          -->
          <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style=" width: 90px;" title="Guardar los datos del nuevo elemento...">
              <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
          </a>
          <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Cancelar la alta del nuevo elemento...">
              <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
          </a>
      </td>
    </tr>
  </table>
</div>

<form id="frmIncidentes" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
  <div id="dvForm-Incidentes" class="dvForm-Data">
      <span class="dvForm-Data-pTitle">
          <img src="<?php echo PATH_IMAGES;?>icons/police_maker24.png" class="icono"/>
          Incidentes
      </span>

      <fieldset id="fsetopetbl_mid_incidentes" class="fsetForm-Data"  style="table-layout: auto;">
          <!--<legend>opetbl_mid_incidentes</legend>-->
          <table id="tbfrmopetbl_mid_incidentes" class="tbForm-Data">
            <tr>
              <td class="descripcion"><label for="txtIdFolioIncidente">Folio Incidente:</label></td>
              <td  class="validation controles">
                <?php
                    if( $objInc->id_folio_incidente == 0 ){
                        $id = str_pad($objInc->getIdUltimoIncidente() + 1, 8, "0", STR_PAD_LEFT);
                    }else{
                        $id = $objInc->id_folio_incidente;
                    }
                ?>
                <input type="text" name="txtIdFolioIncidente" id="txtIdFolioIncidente" value="<?php echo $id; ?>" style="width: 100px;" maxlength="" readonly="true" />
              </td>
              <td rowspan="12" class="" style="vertical-align: top;">
              	<div id="mapaIncidente" class="mapaIncidente">
                  <!-- Aqui mapa para Georeferenciar el Incidente -->
                </div>
              </td>
            </tr>
            <tr>
            <td class="descripcion">Prioridad:</td>
            <td>
                 <?php 
                    switch( $objInc->prioridad ){
                        case 1:
                            $checked1 = 'checked="true"';
                            $checked2 = '';
                            $checked3 = '';
                            //echo $objInc->prioridad;
                            break;
                        case 2:
                            $checked1 = '';
                            $checked2 = 'checked="true"';
                            $checked3 = '';
                            //echo $objInc->prioridad;
                            break;
                        case 3:
                            $checked1 = '';
                            $checked2 = '';
                            $checked3 = 'checked="true"';
                            //echo $objInc->prioridad;
                            break;
                        default:
                            $checked1 = '';
                            $checked2 = '';
                            $checked3 = '';
                            break;
                    }                    
                 ?>
              <div class="dvRadioGroup">
                  <label class="label-Radio Baja"><input type="radio" name="RadioGroupPrioridad" value="1" id="RadioGroup_Baja" <?php echo $checked1; ?> />BAJA</label>
                  <label class="label-Radio Media"><input  type="radio" name="RadioGroupPrioridad" value="2" id="RadioGroup_Media" <?php echo $checked2; ?> />MEDIA</label>
                  <label class="label-Radio Alta"><input type="radio" name="RadioGroupPrioridad" value="3" id="RadioGroup_Alta" <?php echo $checked3; ?> />ALTA</label>
              </div>
            </td>
            </tr>
            <tr>
            <?php
                $fecha_incidente = $objInc->fecha_incidente ? date('d/m/Y', strtotime($objInc->fecha_incidente) ): "";
            ?>
              <td class="descripcion"><label for="txtFechaIncidente">Fecha del Incidente:</label></td>
              <td class="validation controles">
                 <input type="text" name="txtFechaIncidente" id="txtFechaIncidente" value="<?php echo $fecha_incidente; ?>" placeholder="DD/MM/AAAA" maxlength="10"  style="width: 100px;"/>
                 <span class="pRequerido">*</span>
                 <!--<label id="DiaFecha">Lunes</label>-->
              </td>
            </tr>
            <tr>
                <?php
                    $hora       = explode(":", $objInc->hora_incidente);
                    $horas      = $hora[0];
                    $minutos    = $hora[1];
                ?>
              <td class="descripcion"><label for="txtHoraIncidente">Hora del Incidente:</label></td>
              <td class="validation controles">
                  <!--</a><input type="text" name="txtHoraIncidente" id="txtHoraIncidente" value="" style="width: 100px;" maxlength="5" />-->
                 <select name="selHoras" id="selHoras">
                    <?php echo $objFunciones->getCat_Horas( $horas ); ?>
                 </select>
                 <strong>:</strong>
                 <select name="selMinutos" id="selMinutos">
                    <?php echo $objFunciones->getCat_Minutos( $minutos ); ?>
                 </select> Horas.
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="cbxIdEstado">Estado: <?php //echo $objInc->hora_incidente; ?></label></td>
              <td class="validation">
                  <select name="cbxIdEstado" id="cbxIdEstado" style="width: 320px;">
                    <?php echo $objInc->OpecatMidEstados->getCat_mid_Estados( $objInc->id_estado );?>
                  </select>
                  <span class="pRequerido">*</span>
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="cbxIdRegion">Regi�n:</label></td>
              <td class="validation">
                   <select name="cbxIdRegion" id="cbxIdRegion" style="width: 320px;">
                    <?php echo $objInc->OpecatMidRegionesOperativas->getCat_mid_Regiones_Operativas( $objInc->id_region ); ?>
                  </select>
                   <span class="pRequerido">*</span>
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="cbxIdCuartel">Cuartel:</label></td>
              <td class="validation">
                   <select name="cbxIdCuartel" id="cbxIdCuartel"  style="width: 320px;">
                      <?php
                         echo $objInc->OpecatMidCuarteles->getCat_mid_Cuarteles( $objInc->id_cuartel );
                      ?>
                    </select>
                   <span class="pRequerido">*</span>
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="cbxIdMunicipio">Municipio:</label></td>
              <td class="validation">
                   <select name="cbxIdMunicipio" id="cbxIdMunicipio"  style="width: 320px;">
                    <?php echo $objInc->OpecatMidMunicipios->getCat_mid_Municipios( $objInc->id_municipio );?>
                  </select>
                   <span class="pRequerido">*</span>
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="txtLocalidad">Localidad:</label></td>
              <td class="validation">
                   <input type="text" name="txtLocalidad" id="txtLocalidad" value="<?php echo $objInc->localidad; ?>" style="width: 320px;" maxlength="100"  />
                   <span class="pRequerido">*</span>
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="txtColonia">Colonia:</label></td>
              <td class="validation">
                   <input type="text" name="txtColonia" id="txtColonia" value="<?php echo $objInc->colonia; ?>" style="width: 320px;" maxlength="45"  />
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="txtCalle">Calle:</label></td>
              <td class="validation">
                  <input type="text" name="txtCalle" id="txtCalle" value="<?php echo $objInc->calle; ?>" style="width: 320px;" maxlength="45"  />
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="cbxIdAsunto">Asunto:</label></td>
              <td class="validation">
                   <select name="cbxIdAsunto" id="cbxIdAsunto"  style="width: 320px;">
                    <?php echo $objInc->OpecatMidAsuntos->getCat_mid_asuntos( $objInc->id_asunto ); ?>
                  </select>
                   <span class="pRequerido">*</span>
              </td>
            </tr>
            <tr>
              <td class="descripcion"><label for="cbxIdFuente">Fuente:</label></td>
              <td class="validation">
                  <select name="cbxIdFuente" id="cbxIdFuente" style="width: 320px;">
                    <?php echo $objInc->OpecatMidFuentes->getCat_mid_fuentes( $objInc->id_fuente ); ?>
                  </select>
                  <span class="pRequerido">*</span>
              </td>
              <?php
                if( ( $objInc->latitud == '' ) || ( $objInc->longitud == '' ) ){
                    $objInc->latitud =  17.551535;
                    $objInc->longitud = -99.500632;
                }
              ?>
              <td style="text-align:center;">
                  Latitud: <input type="text" name="txtLatitud" id="txtLatitud" value="<?php echo $objInc->latitud; ?>" style="width: 150px;" maxlength="" readonly="true"/>
                  &ensp;
                  Longitud:  <input type="text" name="txtLongitud" id="txtLongitud" value="<?php echo $objInc->longitud; ?>" style="width: 150px;" maxlength="" readonly="true"/>
              </td>
            </tr>
            <tr>
              <td class="descripcion">Relato de Hechos:</td>
              <td class="validation"></td>
              <td style="text-align:center;">
                  <input type="text" name="txtDireccion" id="txtDireccion" value="<?php echo $objInc->direccion; ?>" style="width: 500px; text-align:center;" maxlength=""  readonly="true"/>
              </td>
            </tr>
            <tr>
             <!--  <td class="descripcion"><label for="txtHechosHtml"></label></td> -->
              <td class="validation" colspan="3">
                  <textarea name="txtHechosHtml" id="txtHechosHtml"  rows="12" class="tinymce" style="width:100%">
                    <!--Editor de Texto para la descripcion de los hechos-->
                    <?php echo $objInc->hechos_html; ?>
                  </textarea>
                  <!--<span class="pRequerido">*</span>-->
              </td>
            </tr>
          </table>
      </fieldset>
  </div>
  <input type="hidden" name="oper" id="oper" value="<?php echo $oper; ?>"/>
  
</form>


<!-- Cargamos las librerias al final para obtimizar la caraga del modulo -->
  <script type="text/javascript" src="includes/js/locationpicker/locationpicker.jquery.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&libraries=places"></script>
  <script type="text/javascript" src="ope/mid/_js/incidentes_mapa.js"></script>

<?php
  //-----------------------------------------------------------------//
  //-- Bloque de cerrado de la plantilla...
  //-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
