<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Conflictos Sociales
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - ',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mcs/_css/mcs.css" rel="stylesheet"/>',                                                                    
                                   //'<link  type="text/css" href="includes/js/fullcalendar/css/cupertino/jquery-ui.min.css" rel="stylesheet" />',
                                   '<link  type="text/css" href="includes/js/fullcalendar/css/fullcalendar.css" rel="stylesheet" />',
                                   '<link  type="text/css"href="includes/js/fullcalendar/css/fullcalendar.print.css" rel="stylesheet" media="print"/>',
                                   '<script src="includes/js/fullcalendar/js/moment.min.js"></script>',
                                   //'<script src="includes/js/fullcalendar/js/jquery.min.js"></script>',
                                   //'<script src="includes/js/fullcalendar/js/jquery-ui.custom.min.js"></script>',
                                   '<script src="includes/js/fullcalendar/js/fullcalendar.min.js"></script>',
                                   '<script src="includes/js/fullcalendar/js/es.js"></script>',
                                   '<script src="ope/mcs/_js/fullcalendar_config.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('');
?>

  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="width: 60px;" title="Agregar Nuevo Evento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                    </a>
                    <a href="#" id="btnSiguiente" class="Tool-Bar-Btn gradient" style=" display: none; width: 60px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/next24.png" alt="" style="border: none;" /><br />Siguiente
                    </a>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="display: none; width: 60px;" title="Guardar los datos del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 60px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
  </div>

  <form id="frmAgenda" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Agenda" class="dvForm-Data">
          <span class="dvForm-Data-pTitle">
              <img src="<?php echo PATH_IMAGES;?>icons/add.png" class="icono"/>
               Agenda Conflictos Sociales
          </span>

    <!-- Contenido del Formulario -->
<fieldset id="fsetopetbl_mcs_conflictos_agenda" class="fsetForm-Data" >
    <div style="text-align: center;">
        Agenda de eventos pr�ximos
        <div id='calendar'>
            <!-- Aqui Fullcalendar -->
        </div>
        <!--<img src="<?php echo PATH_IMAGES;?>icons/agenda.png"/>-->
    </div>

     <!--<legend>opetbl_mcs_agenda</legend>-->
     <table id="tbfrmopetbl_mcs_conflictos_agenda" class="tbForm-Data" style="display:none;">
         <tr>
             <td class="descripcion"><label for="txtIdAgenda">IdAgenda:</label></td>
             <td class="validation">
                <input type="text" name="txtIdAgenda" id="txtIdAgenda" value="" maxlength="" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtIdMunicipio">IdMunicipio:</label></td>
             <td class="validation">
                <select name="selIdMunicipio" id="selIdMunicipio"><?php ?></select>
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtIdMedioConvocatoria">IdMedioConvocatoria:</label></td>
             <td class="validation">
                <select name="selIdMedioConvocatoria" id="selIdMedioConvocatoria"><?php ?></select>
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtFecha">Fecha:</label></td>
             <td class="validation">
                <input type="text" name="txtFechaFecha" id="txtFecha" value="" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtHoraInicio">HoraInicio:</label></td>
             <td class="validation">
                <input type="text" name="txtHoraInicio" id="txtServer" value="" maxlength="" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtLugar">Lugar:</label></td>
             <td class="validation">
                <input type="text" name="txtLugar" id="txtLugar" value="" maxlength="100" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtIdOrganizacionGpo">IdOrganizacionGpo:</label></td>
             <td class="validation">
                <select name="selIdOrganizacionGpo" id="selIdOrganizacionGpo"><?php ?></select>
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td><label for="txtActividadRealizar">ActividadRealizar:</label></td>
             <td class="validation">
                <textarea name="textarea" id="txtAreaActividadRealizar" cols="45" rows="2"></textarea>
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtItinerariosPreviso">ItinerariosPreviso:</label></td>
             <td class="validation">
                <textarea name="textarea" id="txtAreaItinerariosPreviso" cols="45" rows="2"></textarea>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtMotivoEvento">MotivoEvento:</label></td>
             <td class="validation">
                <input type="text" name="txtMotivoEvento" id="txtMotivoEvento" value="" maxlength="45" class="" />
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtQuienInforma">QuienInforma:</label></td>
             <td class="validation">
                <input type="text" name="txtQuienInforma" id="txtQuienInforma" value="" maxlength="45" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtIdUsuario">IdUsuario:</label></td>
             <td class="validation">
                <input type="text" name="txtIdUsuario" id="txtIdUsuario" value="" maxlength="45" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtResultados">Resultados:</label></td>
             <td class="validation">
                <input type="text" name="txtResultados" id="txtResultados" value="" maxlength="45" class="" />
              </td>
         </tr>
     </table>
</fieldset>

  <!-- Fin del Contenido del Formulario -->
  <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podra continuar hasta que los complete.</p>
 </div>
</form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
