$(document).ready(function() {
		var currentLangCode = 'es';

		var Hoy=fechaHoy('aaaa-mm-dd','/');

		var calendar = $('#calendar').fullCalendar({
			theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay',
			},			
			lang: currentLangCode,
			buttonIcons: true, // show the prev/next text
			weekNumbers: true,
			editable: true,
			//events: "http://localhost/fullcalendar/events.php",
			// selectable: true,
			// selectHelper: true,
			// select: function(start, end, allDay) {
			// 	 var title = prompt('Event Title:');

			// 	 if (title) {
			// 		 start = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
			// 		 end = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");
			// 		$.ajax({
			// 			 url: 'http://localhost/fullcalendar/add_events.php',
			// 			 data: 'title='+ title+'&start='+ start +'&end='+ end ,
			// 			 type: "POST",
			// 			success: function(json) {
			// 			 	alert('OK');
			// 			 }
			// 		 });
			// 		 calendar.fullCalendar('renderEvent',
			// 			 {
			// 				 title: title,
			// 				 start: start,
			// 				 end: end,
			// 				 allDay: allDay
			// 			 },
			// 			 	true // make the event "stick"
			// 		 );
			// 	 }
			// 	 calendar.fullCalendar('unselect');
			// },
			// editable: true,
			// eventDrop: function(event, delta) {
			//  start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
			//  end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
			//  $.ajax({
			// 	 url: 'http://localhost/fullcalendar/update_events.php',
			// 	 data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
			// 	 type: "POST",
			// 	 success: function(json) {
			// 	 	alert("OK");
			// 	 }
			//  });
			// },
			// eventResize: function(event) {
			//  start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
			//  end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
			//  $.ajax({
			// 	 url: 'http://localhost/fullcalendar/update_events.php',
			// 	 data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id,
			// 	 type: "POST",
			// 	 success: function(json) {
			// 	 	alert("OK");
			// 	 }
			//  });

			// }

		});//Fin de Fullcalendar
	//Fin de Document.Ready
	});


function fechaHoy(formato,separador){
	var fecha=new Date();
	separador='-';
	var dia = fecha.getDate();
	var mes = fecha.getMonth() + 1;
	var anio =fecha.getFullYear();
	var Hoy='';


	switch (formato) {
		case "aaaa-mm-dd":
			Hoy = anio + "-" + mes + "-" + dia;
			break;
		case  "dd-mm-aa":
			Hoy = dia + "-" + mes + "-" + anio;
			break;
		default:
			Hoy = dia + "-" + mes + "-" + anio;
	}

 return Hoy;
}
