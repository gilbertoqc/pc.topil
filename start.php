<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Inicio',
                'usr' => $_SESSION['xlogin_id_sisp'],                
                /*'scripts' => array('<script type="text/javascript" src="includes/js/modulos/ini_inicio.js"></script>'),*/                
                'header' => true,                
                'menu' => true,
                'id_menu' => $_SESSION['xIdMenu'],
                'txt_mod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
$plantilla->accesosDirectos();

//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>