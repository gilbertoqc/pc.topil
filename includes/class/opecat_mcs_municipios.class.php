<?php
/**
 *
 */
class OpecatMcsMunicipios
{
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_estado; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_region; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: SI, @Llave: MUL, @Default: NULL */
    public $municipio; /** @Tipo: varchar(80), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMcsEstados; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMcsRegiones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mcs_estados.class.php';
        require_once 'opecat_mcs_regiones.class.php';
        $this->OpecatMcsEstados = new OpecatMcsEstados();
        $this->OpecatMcsRegiones = new OpecatMcsRegiones();
    }


    /**
     * Funci�n para mostrar la lista de Situaciones dentro de un combobox.
     * @param int $id, id de la situacion seleccionada por deafult
     * @return array html(options)
     */
    public function getMunicipios($id=0, $id_estado=0, $id_region=0)
    {
        $sql_where = '';
        if( $id_estado > 0 )
            $sql_where = "a.id_estado=" . $id_estado;
        if( $id_region > 0 ){
            if( !empty($sql_where) )
                $sql_where .= " AND a.id_region=" . $id_region;
            else
                $sql_where = " a.id_region=" . $id_region;
        }
        
        $aryDatos = $this->selectAll($sql_where, 'municipio asc',"");
        $html = '<option value="0">- Seleccione -</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_municipio"] )
                $html .= '<option value="'.$datos["id_municipio"].'" selected>'.$datos["municipio"].'</option>';
            else
                $html .= '<option value="'.$datos["id_municipio"].'" >'.$datos["municipio"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_municipio)
    {
        $sql = "SELECT id_municipio, id_estado, id_region, municipio
                FROM opecat_mcs_municipios
                WHERE id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_municipio' => $id_municipio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_municipio = $data['id_municipio'];
            $this->id_estado = $data['id_estado'];
            $this->id_region = $data['id_region'];
            $this->municipio = $data['municipio'];

            $this->OpecatMcsEstados->select($this->id_estado);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para filtrar los datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere="", $sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_municipio, a.id_estado, a.id_region, a.municipio,
                  b.estado,
                  c.region
                FROM opecat_mcs_municipios a
                 LEFT JOIN opecat_mcs_estados b ON a.id_estado=b.id_estado 
                 LEFT JOIN opecat_mcs_regiones c ON a.id_region=c.id_region";
        if (!empty($sqlWhere))
            $sql .= "\nWHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= "\nORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= "\nLIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_municipio' => $data['id_municipio'],
                               'id_estado' => $data['id_estado'],
                               'id_region' => $data['id_region'],
                               'municipio' => $data['municipio'],
                               'estado' => $data['estado'],
                               'region' => $data['region'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mcs_municipios(id_municipio, id_estado, id_region, municipio)
                VALUES(:id_municipio, :id_estado, :id_region, :municipio);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":id_estado" => $this->id_estado, ":id_region" => $this->id_region, ":municipio" => $this->municipio));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mcs_municipios
                   SET id_estado=:id_estado, id_region=:id_region, municipio=:municipio
                WHERE id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":id_estado" => $this->id_estado, ":id_region" => $this->id_region, ":municipio" => $this->municipio));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>