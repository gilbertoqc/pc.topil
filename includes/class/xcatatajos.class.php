<?php
/**
 *
 */
class Xcatatajos
{
    public $id_menu; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $hint; /** @Tipo: varchar(150), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $icono; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $Xtblmenu; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xtblmenu.class.php';
        $this->Xtblmenu = new Xtblmenu();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_menu)
    {
        $sql = "SELECT id_menu, descripcion, hint, icono, xstat
                FROM xcatatajos
                WHERE id_menu=:id_menu;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_menu' => $id_menu));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_menu = $data['id_menu'];
            $this->descripcion = $data['descripcion'];
            $this->hint = $data['hint'];
            $this->icono = $data['icono'];
            $this->xstat = $data['xstat'];

            $this->Xtblmenu->select($this->id_menu);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_menu, a.descripcion, a.hint, a.icono, a.xstat,
                  b.id_menu, b.menu, b.url, b.icono, b.stat, b.id_padre, b.orden
                FROM xcatatajos a 
                 LEFT JOIN xtblmenu b ON a.id_menu=b.id_menu";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_menu' => $data['id_menu'],
                               'descripcion' => $data['descripcion'],
                               'hint' => $data['hint'],
                               'icono' => $data['icono'],
                               'xstat' => $data['xstat'],
                               'xtblmenu_menu' => $data['menu'],
                               'xtblmenu_url' => $data['url'],
                               'xtblmenu_icono' => $data['icono'],
                               'xtblmenu_stat' => $data['stat'],
                               'xtblmenu_id_padre' => $data['id_padre'],
                               'xtblmenu_orden' => $data['orden'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xcatatajos(id_menu, descripcion, hint, icono, xstat)
                VALUES(:id_menu, :descripcion, :hint, :icono, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_menu" => $this->id_menu, ":descripcion" => $this->descripcion, ":hint" => $this->hint, ":icono" => $this->icono, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xcatatajos
                   SET descripcion=:descripcion, hint=:hint, icono=:icono, xstat=:xstat
                WHERE id_menu=:id_menu;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_menu" => $this->id_menu, ":descripcion" => $this->descripcion, ":hint" => $this->hint, ":icono" => $this->icono, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>