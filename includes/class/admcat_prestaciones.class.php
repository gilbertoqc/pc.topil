<?php
/**
 *
 */
class AdmcatPrestaciones
{
    public $id_prestacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $prestacion; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $genera_pago; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de prestaciones dentro de un combobox.
     * @param int $id, id de la categor�a seleccionada por deafult     
     * @return array html(options)
     */
    public function shwPrestaciones($id=0, $curp){
        $sql = "SELECT a.id_prestacion, a.prestacion, a.genera_pago
                FROM admcat_prestaciones a
                WHERE a.id_prestacion NOT IN(SELECT p.id_prestacion 
    										 FROM admtbl_prestaciones p 
    										 WHERE p.curp=:curp); ";
        /*
        $sql = "SELECT a.id_prestacion, a.prestacion, a.genera_pago
                FROM admcat_prestaciones a
                WHERE a.status=1;";
        */
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $html = '';
            while ($datos = $qry->fetch(PDO::FETCH_ASSOC)) {
                if( $id == $datos["id_prestacion"] )
                    $html .= '<option value="'.$datos["id_prestacion"].'" selected>'.$datos["prestacion"].'</option>';
                else
                    $html .= '<option value="'.$datos["id_prestacion"].'" >'.$datos["prestacion"].'</option>';
            }
            return $html;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_prestacion)
    {
        $sql = "SELECT id_prestacion, prestacion, genera_pago, status
                FROM admcat_prestaciones
                WHERE id_prestacion=:id_prestacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_prestacion' => $id_prestacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_prestacion = $data['id_prestacion'];
            $this->prestacion = $data['prestacion'];
            $this->genera_pago = $data['genera_pago'];
            $this->status = $data['status'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_prestacion, a.prestacion, a.genera_pago, a.status
                FROM admcat_prestaciones a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_prestacion' => $data['id_prestacion'],
                               'prestacion' => $data['prestacion'],
                               'genera_pago' => $data['genera_pago'],
                               'status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_prestaciones(id_prestacion, prestacion, genera_pago, status)
                VALUES(:id_prestacion, :prestacion, :genera_pago, :status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_prestacion" => $this->id_prestacion, ":prestacion" => $this->prestacion, ":genera_pago" => $this->genera_pago, ":status" => $this->status));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_prestaciones
                   SET prestacion=:prestacion, genera_pago=:genera_pago, status=:status
                WHERE id_prestacion=:id_prestacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_prestacion" => $this->id_prestacion, ":prestacion" => $this->prestacion, ":genera_pago" => $this->genera_pago, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>