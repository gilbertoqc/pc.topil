<?php
/**
 *
 */
class OpecatMidCuarteles_
{
    public $id_cuartel; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_region; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_cuartel_tipo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $cuartel; /** @Tipo: varchar(200), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */
    public $oficial; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidRegionesOperativas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidCuartelesTipos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct(){
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_regiones_operativas.class.php';
        require_once 'opecat_mid_cuarteles_tipos.class.php';
         $this->OpecatMidRegionesOperativas = new OpecatMidRegionesOperativas();
         $this->OpecatMidCuartelesTipos = new OpecatMidCuartelesTipos();
    }


    /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Cuarteles($id=0){
       $aryDatos = $this->selectAll('id_cuartel Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';


       foreach( $aryDatos as $datos ){
            if( $id == $datos["id_cuartel"] )
                $html .= '<option value="'.$datos["id_cuartel"].'" selected>'.$datos["cuartel"].'</option>';
            else
                $html .= '<option value="'.$datos["id_cuartel"].'" >'.$datos["cuartel"].'</option>';
        }

        return $html;
    }


 /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_cuartel)
    {
        $sql = "SELECT id_cuartel, id_region, id_cuartel_tipo, cuartel, oficial
                FROM opecat_mid_cuarteles
                WHERE id_cuartel=:id_cuartel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_cuartel' => $id_cuartel));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_cuartel = $data['id_cuartel'];
            $this->id_region = $data['id_region'];
            $this->id_cuartel_tipo = $data['id_cuartel_tipo'];
            $this->cuartel = $data['cuartel'];
            $this->oficial = $data['oficial'];

            $this->OpecatMidRegionesOperativas->select($this->id_region);
            $this->OpecatMidCuartelesTipos->select($this->id_cuartel_tipo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

/**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_cuartel, a.id_region, a.id_cuartel_tipo, a.cuartel, a.oficial,
                  b.id_region, b.region,
                  c.id_cuartel_tipo, c.cuartel_tipo
                FROM opecat_mid_cuarteles a
                 LEFT JOIN opecat_mid_regiones_operativas b ON a.id_region=b.id_region
                 LEFT JOIN opecat_mid_cuarteles_tipos c ON a.id_cuartel_tipo=c.id_cuartel_tipo";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_cuartel' => $data['id_cuartel'],
                               'id_region' => $data['id_region'],
                               'id_cuartel_tipo' => $data['id_cuartel_tipo'],
                               'cuartel' => $data['cuartel'],
                               'oficial' => $data['oficial'],
                               'opecat_mid_regiones_operativas_region' => $data['region'],
                               'opecat_mid_cuarteles_tipos_cuartel_tipo' => $data['cuartel_tipo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }




/**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_cuarteles(id_cuartel, id_region, id_cuartel_tipo, cuartel, oficial)
                VALUES(:id_cuartel, :id_region, :id_cuartel_tipo, :cuartel, :oficial);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cuartel" => $this->id_cuartel, ":id_region" => $this->id_region, ":id_cuartel_tipo" => $this->id_cuartel_tipo, ":cuartel" => $this->cuartel, ":oficial" => $this->oficial));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_cuarteles
                   SET id_region=:id_region, id_cuartel_tipo=:id_cuartel_tipo, cuartel=:cuartel, oficial=:oficial
                WHERE id_cuartel=:id_cuartel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cuartel" => $this->id_cuartel, ":id_region" => $this->id_region, ":id_cuartel_tipo" => $this->id_cuartel_tipo, ":cuartel" => $this->cuartel, ":oficial" => $this->oficial));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }


}

?>