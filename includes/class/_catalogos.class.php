<?php
class Catalogos{
    private $xBD;   
    public $xERROR;
   
    public function __construct() {
        require_once("config/mysql.class.php");
        $this->xBD = new MySQLPDO();
    }
    
    /*
    *--- Cat�logo: Status de personal...
    */
    public function shwStatus($id=-1){
        $aryDatos = $this->getStatus();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_status"] )
                $html .= '<option value="'.$datos["id_status"].'" selected>'.$datos["status"].'</option>';
            else
                $html .= '<option value="'.$datos["id_status"].'" >'.$datos["status"].'</option>';
        }
        return $html;
    }
    public function getStatus(){
        $qSql = "SELECT * 
                 FROM ctstatus                
                 ORDER BY status;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_status" => $f["id_status"], "status" => $f["status"]);
        
        return $DatCat;
    }
     
    /*   
    *--- Cat�logo: Estado Civil...
    */
    public function shwEstadoCivil($id=0){
        $aryDatos = $this->getEstadoCivil();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_estado_civil"] )
                $html .= '<option value="'.$datos["id_estado_civil"].'" selected>'.$datos["estado_civil"].'</option>';
            else
                $html .= '<option value="'.$datos["id_estado_civil"].'" >'.$datos["estado_civil"].'</option>';
        }
        return $html;
    }
    public function getEstadoCivil(){
        $qSql = "SELECT * 
                 FROM ctestado_civil                
                 ORDER BY estado_civil;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_estado_civil" => $f["id_estado_civil"], "estado_civil" => $f["estado_civil"]);
        
        return $DatCat;
    }
    
    /*   
    *--- Cat�logo: Pa�s...
    */
    public function shwPais($id=0){
        $aryDatos = $this->getPais();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_pais"] )
                $html .= '<option value="'.$datos["id_pais"].'" selected>'.$datos["pais"].'</option>';
            else
                $html .= '<option value="'.$datos["id_pais"].'" >'.$datos["pais"].'</option>';
        }
        return $html;
    }
    public function getPais(){
        $qSql = "SELECT * 
                 FROM ctpais 
                 ORDER BY pais;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_pais" => $f["id_pais"], "pais" => $f["pais"]);
        
        return $DatCat;
    }
    public function addPais($data){
        
    }
    public function delPais($id){
        
    }
    
    /*
    *--- Cat�logo: Entidad...
    */
    public function shwEntidad($id=0, $id_pais=0){
        $aryDatos = $this->getEntidad($id_pais);
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_entidad"] )
                $html .= '<option value="'.$datos["id_entidad"].'" selected>'.$datos["entidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_entidad"].'" >'.$datos["entidad"].'</option>';
        }
        return $html;
    }
    public function getEntidad($id_pais=0){
        $sql_pais = ( $id_pais != 0 ) ? "WHERE id_pais=" . $id_pais : "";
        $qSql = "SELECT * 
                 FROM ctentidad 
                 " . $sql_pais . " 
                 ORDER BY entidad;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_entidad" => $f["id_entidad"], "entidad" => $f["entidad"], "abreviacion" => $f["abreviacion"]);
        
        return $DatCat;
    }
    public function addEntidad($data){
        
    }
    public function delEntidad($id){
        
    }
    
    /*
    *--- Cat�logo: Municipio...
    */
    public function shwMunicipio($id_entidad=0, $id_region=0, $id=0){
        $aryDatos = $this->getMunicipio($id_entidad, $id_region);
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_municipio"] )
                $html .= '<option value="'.$datos["id_municipio"].'" selected>'.$datos["municipio"].'</option>';
            else
                $html .= '<option value="'.$datos["id_municipio"].'" >'.$datos["municipio"].'</option>';
        }
        return $html;
    }
    public function getMunicipio($id_entidad=0, $id_region=0){
        $sql_entidad = ( $id_entidad != 0 ) ? "WHERE id_entidad=" . $id_entidad : "";
        if( $id_region != 0 && empty($sql_entidad) )
            $sql_region = "WHERE id_region=" . $id_region;
        else if( $id_region != 0 && !empty($sql_entidad) )
            $sql_region = " AND id_region=" . $id_region;
        else
            $sql_region = "";
        $qSql = "SELECT * 
                 FROM ctmunicipio 
                 " . $sql_entidad . $sql_region . " 
                 ORDER BY municipio;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_municipio" => $f["id_municipio"], "municipio" => $f["municipio"], "cabecera" => $f["cabecera"]);
        
        return $DatCat;
    }
    public function addMunicipio($data){
        
    }
    public function delMunicipio($id){
        
    }
    
    /*
    *--- Cat�logo: Regi�n...
    */
    public function shwRegion($id=0){
        $aryDatos = $this->getRegion();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_region"] )
                $html .= '<option value="'.$datos["id_region"].'" selected>'.$datos["region"].'</option>';
            else
                $html .= '<option value="'.$datos["id_region"].'" >'.$datos["region"].'</option>';
        }
        return $html;
    }
    public function getRegion(){        
        $qSql = "SELECT * 
                 FROM ctregion 
                 ORDER BY region;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_region" => $f["id_region"], "region" => $f["region"]);
        
        return $DatCat;
    }
    public function addRegion($data){
        
    }
    public function delRegion($id){
        
    }
    
    /*
    *--- Cat�logo: Nacionalidad...
    */
    public function shwNacionalidad($id=0){
        $aryDatos = $this->getNacionalidad();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nacionalidad"] )
                $html .= '<option value="'.$datos["id_nacionalidad"].'" selected>'.$datos["nacionalidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nacionalidad"].'" >'.$datos["nacionalidad"].'</option>';
        }
        return $html;
    }
    public function getNacionalidad(){        
        $qSql = "SELECT * 
                 FROM ctnacionalidad 
                 ORDER BY nacionalidad;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_nacionalidad" => $f["id_nacionalidad"], "nacionalidad" => $f["nacionalidad"]);
        
        return $DatCat;
    }
    public function addNacionalidad($data){
        
    }
    public function delNacionalidad($id){
        
    }
    
    /*
    *--- Cat�logo: Tipo Sanguineo...
    */
    public function shwTipoSangre($id=0){
        $aryDatos = $this->getTipoSangre();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_sangre"] )
                $html .= '<option value="'.$datos["id_tipo_sangre"].'" selected>'.$datos["tipo_sangre"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_sangre"].'" >'.$datos["tipo_sangre"].'</option>';
        }
        return $html;
    }
    public function getTipoSangre(){        
        $qSql = "SELECT * 
                 FROM cttipo_sangre 
                 ORDER BY tipo_sangre;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_tipo_sangre" => $f["id_tipo_sangre"], "tipo_sangre" => $f["tipo_sangre"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: Tipo N�mina...
    */
    public function shwTipoNomina($id=0){
        $aryDatos = $this->getTipoNomina();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_nomina"] )
                $html .= '<option value="'.$datos["id_tipo_nomina"].'" selected>'.$datos["tipo_nomina"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_nomina"].'" >'.$datos["tipo_nomina"].'</option>';
        }
        return $html;
    }
    public function getTipoNomina(){        
        $qSql = "SELECT * 
                 FROM cttipo_nomina 
                 ORDER BY tipo_nomina;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_tipo_nomina" => $f["id_tipo_nomina"], "tipo_nomina" => $f["tipo_nomina"]);
        
        return $DatCat;
    }
    public function addTipoNomina($data){
        
    }
    public function delTipoNomina($id){
        
    }
    
    /*
    *--- Cat�logo: Bancos...
    */
    public function shwBancos($id=0){
        $aryDatos = $this->getBancos();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_banco"] )
                $html .= '<option value="'.$datos["id_banco"].'" selected>'.$datos["banco"].'</option>';
            else
                $html .= '<option value="'.$datos["id_banco"].'" >'.$datos["banco"].'</option>';
        }
        return $html;
    }
    public function getBancos(){        
        $qSql = "SELECT * 
                 FROM ctbanco 
                 ORDER BY banco;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_banco" => $f["id_banco"], "banco" => $f["banco"]);
        
        return $DatCat;
    }
    public function addBanco($data){
        
    }
    public function delBanco($id){
        
    }
    
    /*
    *--- Cat�logo: Nivel Estudios...
    */
    public function shwNivelEstudios($id=0){
        $aryDatos = $this->getNivelEstudios();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nivel_estudios"] )
                $html .= '<option value="'.$datos["id_nivel_estudios"].'" selected>'.$datos["nivel_estudios"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nivel_estudios"].'" >'.$datos["nivel_estudios"].'</option>';
        }
        return $html;
    }
    public function getNivelEstudios(){
        $qSql = "SELECT * 
                 FROM ctnivel_estudios 
                 ORDER BY id_nivel_estudios;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_nivel_estudios" => $f["id_nivel_estudios"], "nivel_estudios" => $f["nivel_estudios"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: Tipo Referencia...
    */
    public function shwTipoReferencia($id=0){
        $aryDatos = $this->getTipoReferencia();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_referencia"] )
                $html .= '<option value="'.$datos["id_tipo_referencia"].'" selected>'.$datos["tipo_referencia"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_referencia"].'" >'.$datos["tipo_referencia"].'</option>';
        }
        return $html;
    }
    public function getTipoReferencia(){        
        $qSql = "SELECT * 
                 FROM cttipo_referencia 
                 WHERE status=1 
                 ORDER BY tipo_referencia;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_tipo_referencia" => $f["id_tipo_referencia"], "tipo_referencia" => $f["tipo_referencia"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: Tipo Relaci�n...
    */
    public function shwTipoRelacion($id_tp_ref=0, $id=0){
        $aryDatos = $this->getTipoRelacion($id_tp_ref);
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_relacion"] )
                $html .= '<option value="'.$datos["id_tipo_relacion"].'" selected>'.$datos["tipo_relacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_relacion"].'" >'.$datos["tipo_relacion"].'</option>';
        }
        return $html;
    }
    public function getTipoRelacion($id_tp_ref=0){
        $sql_refer = ( $id_tp_ref != 0 ) ? "AND id_tipo_referencia=" . $id_tp_ref : "";
        $qSql = "SELECT * 
                 FROM cttipo_relacion 
                 WHERE status=1 " . $sql_refer . " 
                 ORDER BY tipo_relacion;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_tipo_relacion" => $f["id_tipo_relacion"], "tipo_relacion" => $f["tipo_relacion"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: Categor�a...
    */
    public function shwCategoria($id=0){
        $aryDatos = $this->getCategoria();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_categoria"] )
                $html .= '<option value="'.$datos["id_categoria"].'" selected>'.$datos["categoria"].'</option>';
            else
                $html .= '<option value="'.$datos["id_categoria"].'" >'.$datos["categoria"].'</option>';
        }
        return $html;
    }
    public function getCategoria(){
        $qSql = "SELECT * 
                 FROM ctcategoria 
                 WHERE status=1 
                 ORDER BY categoria;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_categoria" => $f["id_categoria"], "categoria" => $f["categoria"]);
        
        return $DatCat;
    }
    public function addCategoria($data){
        
    }
    public function delCategoria($id){
        
    }
    
    /*
    *--- Cat�logo: Tipo Funci�n...
    */
    public function shwTipoFuncion($id=0){
        $aryDatos = $this->getTipoFuncion();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_funcion"] )
                $html .= '<option value="'.$datos["id_tipo_funcion"].'" selected>'.$datos["tipo_funcion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_funcion"].'" >'.$datos["tipo_funcion"].'</option>';
        }
        return $html;
    }
    public function getTipoFuncion(){
        $qSql = "SELECT * 
                 FROM cttipo_funcion 
                 ORDER BY tipo_funcion;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_tipo_funcion" => $f["id_tipo_funcion"], "tipo_funcion" => $f["tipo_funcion"]);
        
        return $DatCat;
    }
    public function addTipoFuncion($data){
        
    }
    public function delTipoFuncion($id){
        
    }
    
    /*
    *--- Cat�logo: Especialidad...
    */
    public function shwEspecialidad($id=0){
        $aryDatos = $this->getEspecialidad();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_especialidad"] )
                $html .= '<option value="'.$datos["id_especialidad"].'" selected>'.$datos["especialidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_especialidad"].'" >'.$datos["especialidad"].'</option>';
        }
        return $html;
    }
    public function getEspecialidad(){
        $qSql = "SELECT * 
                 FROM ctespecialidad 
                 ORDER BY especialidad;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_especialidad" => $f["id_especialidad"], "especialidad" => $f["especialidad"]);
        
        return $DatCat;
    }
    public function addEspecialidad($data){
        
    }
    public function delEspecialidad($id){
        
    }
    
    /*
    *--- Cat�logo: Horario...
    */
    public function shwHorario($id=0){
        $aryDatos = $this->getHorario();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_horario"] )
                $html .= '<option value="'.$datos["id_horario"].'" selected>'.$datos["horario"].'</option>';
            else
                $html .= '<option value="'.$datos["id_horario"].'" >'.$datos["horario"].'</option>';
        }
        return $html;
    }
    public function getHorario(){
        $qSql = "SELECT * 
                 FROM cthorario 
                 WHERE status=1 
                 ORDER BY horario;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_horario" => $f["id_horario"], "horario" => $f["horario"]);
        
        return $DatCat;
    }
    public function addHorario($data){
        
    }
    public function delHorario($id){
        
    }
    
    /*
    *--- Cat�logo: Ubicaci�n...
    */
    public function shwUbicacion($id=0){
        $aryDatos = $this->getUbicacion();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_ubicacion"] )
                $html .= '<option value="'.$datos["id_ubicacion"].'" selected>'.$datos["ubicacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_ubicacion"].'" >'.$datos["ubicacion"].'</option>';
        }
        return $html;
    }
    public function getUbicacion(){
        $qSql = "SELECT * 
                 FROM ctubicacion 
                 WHERE status=1 
                 ORDER BY ubicacion;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_ubicacion" => $f["id_ubicacion"], "ubicacion" => $f["ubicacion"]);
        
        return $DatCat;
    }
    public function addUbicacion($data){
        
    }
    public function delUbicacion($id){
        
    }
    
    /*
    *--- Cat�logo: Corporaci�n...
    */
    public function shwCorporacion($id=0){
        $aryDatos = $this->getCorporacion();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_corporacion"] )
                $html .= '<option value="'.$datos["id_corporacion"].'" selected>'.$datos["corporacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_corporacion"].'" >'.$datos["corporacion"].'</option>';
        }
        return $html;
    }
    public function getCorporacion(){
        $qSql = "SELECT * 
                 FROM ctcorporacion  
                 ORDER BY corporacion;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_corporacion" => $f["id_corporacion"], "corporacion" => $f["corporacion"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: Nivel de Mando...
    */
    public function shwNivelMando($id=0){
        $aryDatos = $this->getNivelMando();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nivel_mando"] )
                $html .= '<option value="'.$datos["id_nivel_mando"].'" selected>'.$datos["nivel_mando"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nivel_mando"].'" >'.$datos["nivel_mando"].'</option>';
        }
        return $html;
    }
    public function getNivelMando(){
        $qSql = "SELECT * 
                 FROM ctnivel_mando  
                 ORDER BY nivel_mando;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_nivel_mando" => $f["id_nivel_mando"], "nivel_mando" => $f["nivel_mando"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: Nivel de �rea...
    */
    public function shwNivelArea($id=0){
        $aryDatos = $this->getNivelArea();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nivel_area"] )
                $html .= '<option value="'.$datos["id_nivel_area"].'" selected>'.$datos["nivel_area"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nivel_area"].'" >'.$datos["nivel_area"].'</option>';
        }
        return $html;
    }
    public function getNivelArea(){
        $qSql = "SELECT * 
                 FROM ctnivel_area  
                 ORDER BY gerarquia;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_nivel_area" => $f["id_nivel_area"], "nivel_area" => $f["nivel_area"]);
        
        return $DatCat;
    }
    
    /*
    *--- Cat�logo: �reas...
    */
    public function shwAreas($id=0){
        $aryDatos = $this->getNivelArea();
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nivel_area"] )
                $html .= '<option value="'.$datos["id_nivel_area"].'" selected>'.$datos["nivel_area"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nivel_area"].'" >'.$datos["nivel_area"].'</option>';
        }
        return $html;
    }
    public function getAreas(){
        $qSql = "SELECT * 
                 FROM ctnivel_area  
                 ORDER BY gerarquia;";
        $qDat = $this->xBD->dbh->prepare($qSql);
        $qDat->execute();
        $qDat->setFetchMode(PDO::FETCH_ASSOC);
        
        $DatCat = array();
        while( $f = $qDat->fetch() )
            $DatCat[] = array("id_nivel_area" => $f["id_nivel_area"], "nivel_area" => $f["nivel_area"]);
        
        return $DatCat;
    }
}
?>