<?php
/**
 *
 */
class AdmtblPrestaciones
{
    public $id_prest_personal; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_alta; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_reg; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_prestacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_status_prest; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatPrestaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatStatusPrestacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_prestaciones.class.php';
        require_once 'admcat_status_prestacion.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatPrestaciones = new AdmcatPrestaciones();
        $this->AdmcatStatusPrestacion = new AdmcatStatusPrestacion();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_prest_personal)
    {
        $sql = "SELECT id_prest_personal, curp, fecha_alta, fecha_reg, observaciones, id_prestacion, id_status_prest
                FROM admtbl_prestaciones
                WHERE id_prest_personal=:id_prest_personal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_prest_personal' => $id_prest_personal));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_prest_personal = $data['id_prest_personal'];
            $this->curp = $data['curp'];
            $this->fecha_alta = $data['fecha_alta'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->observaciones = $data['observaciones'];
            $this->id_prestacion = $data['id_prestacion'];
            $this->id_status_prest = $data['id_status_prest'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatPrestaciones->select($this->id_prestacion);
            $this->AdmcatStatusPrestacion->select($this->id_status_prest);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_prest_personal, a.curp, a.fecha_alta, a.fecha_reg, a.observaciones, a.id_prestacion, a.id_status_prest,
                  b.prestacion, b.genera_pago,
                  c.status_prest
                FROM admtbl_prestaciones a 
                 LEFT JOIN admcat_prestaciones b ON a.id_prestacion=b.id_prestacion
                 LEFT JOIN admcat_status_prestacion c ON a.id_status_prest=c.id_status_prest";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_prest_personal' => $data['id_prest_personal'],
                               'curp' => $data['curp'],
                               'fecha_alta' => $data['fecha_alta'],
                               'fecha_reg' => $data['fecha_reg'],
                               'observaciones' => $data['observaciones'],
                               'id_prestacion' => $data['id_prestacion'],
                               'id_status_prest' => $data['id_status_prest'],
                               'prestacion' => $data['prestacion'],
                               'genera_pago' => $data['genera_pago'],
                               'status_prest' => $data['status_prest'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_prestaciones(id_prest_personal, curp, fecha_alta, fecha_reg, observaciones, id_prestacion, id_status_prest)
                VALUES(:id_prest_personal, :curp, :fecha_alta, curdate(), :observaciones, :id_prestacion, :id_status_prest);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_prest_personal" => $this->id_prest_personal, ":curp" => $this->curp, ":fecha_alta" => $this->fecha_alta, ":observaciones" => $this->observaciones, ":id_prestacion" => $this->id_prestacion, ":id_status_prest" => $this->id_status_prest));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_prestaciones
                   SET curp=:curp, fecha_alta=:fecha_alta, observaciones=:observaciones, id_prestacion=:id_prestacion, id_status_prest=:id_status_prest
                WHERE id_prest_personal=:id_prest_personal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_prest_personal" => $this->id_prest_personal, ":curp" => $this->curp, ":fecha_alta" => $this->fecha_alta, ":observaciones" => $this->observaciones, ":id_prestacion" => $this->id_prestacion, ":id_status_prest" => $this->id_status_prest));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>