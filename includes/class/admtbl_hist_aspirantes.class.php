<?php
/**
 *
 */
class AdmtblHistAspirantes
{
    public $curp; /** @Tipo: varchar(20), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nombre; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_paterno; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_materno; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $genero; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio_ife; /** @Tipo: char(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $mat_cartilla; /** @Tipo: varchar(10), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblPlantillaPlazas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_plantilla_plazas.class.php';
        $this->AdmtblPlantillaPlazas = new AdmtblPlantillaPlazas();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, nombre, a_paterno, a_materno, genero, folio_ife, mat_cartilla, id_plaza
                FROM admtbl_hist_aspirantes
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->nombre = $data['nombre'];
            $this->a_paterno = $data['a_paterno'];
            $this->a_materno = $data['a_materno'];
            $this->genero = $data['genero'];
            $this->folio_ife = $data['folio_ife'];
            $this->mat_cartilla = $data['mat_cartilla'];
            $this->id_plaza = $data['id_plaza'];

            $this->AdmtblPlantillaPlazas->select($this->id_plaza);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.genero, a.folio_ife, a.mat_cartilla, a.id_plaza,
                  b.id_plaza, b.id_categoria, b.id_area, b.observaciones
                FROM admtbl_hist_aspirantes a 
                 LEFT JOIN admtbl_plantilla_plazas b ON a.id_plaza=b.id_plaza";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'genero' => $data['genero'],
                               'folio_ife' => $data['folio_ife'],
                               'mat_cartilla' => $data['mat_cartilla'],
                               'id_plaza' => $data['id_plaza'],
                               'admtbl_plantilla_plazas_id_categoria' => $data['id_categoria'],
                               'admtbl_plantilla_plazas_id_area' => $data['id_area'],
                               'admtbl_plantilla_plazas_observaciones' => $data['observaciones'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_hist_aspirantes(curp, nombre, a_paterno, a_materno, genero, folio_ife, mat_cartilla, id_plaza)
                VALUES(:curp, :nombre, :a_paterno, :a_materno, :genero, :folio_ife, :mat_cartilla, :id_plaza);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":genero" => $this->genero, ":folio_ife" => $this->folio_ife, ":mat_cartilla" => $this->mat_cartilla, ":id_plaza" => $this->id_plaza));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_hist_aspirantes
                   SET nombre=:nombre, a_paterno=:a_paterno, a_materno=:a_materno, genero=:genero, folio_ife=:folio_ife, mat_cartilla=:mat_cartilla, id_plaza=:id_plaza
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":genero" => $this->genero, ":folio_ife" => $this->folio_ife, ":mat_cartilla" => $this->mat_cartilla, ":id_plaza" => $this->id_plaza));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>