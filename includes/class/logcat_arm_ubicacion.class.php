<?php
/**
 *
 */
class LogcatArmUbicacion
{
    public $id_ubicacion; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $ubicacion; /** @Tipo: varchar(75), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $orden; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de ubicaciones dentro de un combobox.
     * @param int $id, id de ubicacion seleccionado por deafult     
     * @return array html(options)
     */
    public function shwUbicacion( $id=0 ){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.ubicacion Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_ubicacion"] )
                $html .= '<option value="'.$datos["id_ubicacion"].'" selected>'.$datos["ubicacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_ubicacion"].'" >'.$datos["ubicacion"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_ubicacion)
    {
        $sql = "SELECT id_ubicacion, ubicacion, orden, xstat
                FROM logcat_arm_ubicacion
                WHERE id_ubicacion=:id_ubicacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_ubicacion' => $id_ubicacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_ubicacion = $data['id_ubicacion'];
            $this->ubicacion = $data['ubicacion'];
            $this->orden = $data['orden'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_ubicacion, a.ubicacion, a.orden, a.xstat
                FROM logcat_arm_ubicacion a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_ubicacion' => $data['id_ubicacion'],
                               'ubicacion' => $data['ubicacion'],
                               'orden' => $data['orden'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_ubicacion(id_ubicacion, ubicacion, orden, xstat)
                VALUES(:id_ubicacion, :ubicacion, :orden, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_ubicacion" => $this->id_ubicacion, ":ubicacion" => $this->ubicacion, ":orden" => $this->orden, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_ubicacion
                   SET ubicacion=:ubicacion, orden=:orden, xstat=:xstat
                WHERE id_ubicacion=:id_ubicacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_ubicacion" => $this->id_ubicacion, ":ubicacion" => $this->ubicacion, ":orden" => $this->orden, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>