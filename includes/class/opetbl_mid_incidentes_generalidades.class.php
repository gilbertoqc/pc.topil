<?php
/**
 *
 */
class OpetblMidIncidentesGeneralidades
{
    public $id_igeneralidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_folio_incidente; /** @Tipo: int(10) unsigned zerofill, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_rol_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_unidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $Cantidad; /** @Tipo: decimal(10,3), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Observaciones; /** @Tipo: varchar(500), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidGeneralidadesUnidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */    
    public $OpecatMidRolesGeneralidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpetblMidIncidentes; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_generalidades_unidades.class.php';        
        require_once 'opecat_mid_roles_generalidades.class.php';
        require_once 'opetbl_mid_incidentes.class.php';
        $this->OpecatMidGeneralidadesUnidades = new OpecatMidGeneralidadesUnidades();        
        $this->OpecatMidRolesGeneralidades = new OpecatMidRolesGeneralidades();
        $this->OpetblMidIncidentes = new OpetblMidIncidentes();
    }
    
     /**
    * Funci�n para obtener un el id maximo de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getIdUltimaGeneralidad(){        
        $sql = "SELECT MAX(id_igeneralidad) as id
                  FROM opetbl_mid_incidentes_generalidades;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
    * Funci�n para obtener un registro espec�fico de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getTotalGeneralidades( $id_folio_incidente ){        
        $sql = "SELECT count(*) as id
                  FROM opetbl_mid_incidentes_generalidades
                  WHERE id_folio_incidente = " . $id_folio_incidente;
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n que controla la obtenci?n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci?n para obtener los registros m?nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT g.id_igeneralidad, g.id_folio_incidente, gr.rol_generalidad, gc.generalidad,
                gu.unidad, g.Cantidad, g.Observaciones,
                tip.id_tipo_generalidad, tip.tipo_generalidad
                FROM opetbl_mid_incidentes_generalidades g                
                LEFT JOIN opecat_mid_roles_generalidades gr ON g.id_rol_generalidad=gr.id_rol_generalidad
                LEFT JOIN opecat_mid_generalidades gc ON g.id_generalidad=gc.id_generalidad
                LEFT JOIN opecat_mid_unidades_generalidades gu ON g.id_unidad=gu.id_unidad
                LEFT JOIN opecat_mid_tipos_generalidades tip ON gc.id_tipo_generalidad=tip.id_tipo_generalidad";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_igeneralidad' =>  $data['id_igeneralidad'],
                                'id_folio_incidente' =>  $data['id_folio_incidente'],
                                'rol_generalidad' =>  $data['rol_generalidad'],
                                'tipo_generalidad' =>  $data['tipo_generalidad'],
                                'generalidad' =>  $data['generalidad'],
                                'unidad' =>  $data['unidad'],
                                'Cantidad' =>  $data['Cantidad'],
                                'Observaciones' =>  $data['Observaciones'],
                              );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mid_incidentes_generalidades g
                LEFT JOIN opecat_mid_roles_generalidades gr ON g.id_rol_generalidad=gr.id_rol_generalidad
                LEFT JOIN opecat_mid_generalidades gc ON g.id_generalidad=gc.id_generalidad
                LEFT JOIN opecat_mid_unidades_generalidades gu ON g.id_unidad=gu.id_unidad";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_igeneralidad)
    {
        $sql = "SELECT id_igeneralidad, id_folio_incidente, id_rol_generalidad, id_generalidad, id_unidad, Cantidad, Observaciones
                FROM opetbl_mid_incidentes_generalidades
                WHERE id_igeneralidad=:id_igeneralidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_igeneralidad' => $id_igeneralidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_igeneralidad = $data['id_igeneralidad'];
            $this->id_folio_incidente = $data['id_folio_incidente'];
            $this->id_rol_generalidad = $data['id_rol_generalidad'];
            $this->id_generalidad = $data['id_generalidad'];
            $this->id_unidad = $data['id_unidad'];
            $this->Cantidad = $data['Cantidad'];
            $this->Observaciones = $data['Observaciones'];

            $this->OpecatMidGeneralidadesUnidades->select($this->id_generalidad, $this->id_unidad);
            //$this->OpecatMidGeneralidadesUnidades->select($this->id_unidad);
            $this->OpecatMidRolesGeneralidades->select($this->id_rol_generalidad);
            $this->OpetblMidIncidentes->select($this->id_folio_incidente);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_igeneralidad, a.id_folio_incidente, a.id_rol_generalidad, a.id_generalidad, a.id_unidad, a.Cantidad, a.Observaciones,
                  b.id_generalidad, b.id_unidad,
                  c.id_generalidad, c.id_unidad,
                  d.id_rol_generalidad, d.rol_generalidad,
                  e.id_folio_incidente, e.prioridad, e.fecha_hora_captura, e.fecha_incidente, e.dia, e.mes, e.annio, e.hora_incidente, e.id_estado, e.id_region, e.id_cuartel, e.id_municipio, e.direccion, e.localidad, e.colonia, e.calle, e.latitud, e.longitud, e.id_asunto, e.id_fuente, e.hechos_html, e.hechos_text, e.ip_usuario, e.id_usuario
                  gen.id_generalidad,
                  tip.id_tipo_generalidad, tip.tipo_generalidad
                FROM opetbl_mid_incidentes_generalidades a 
                 LEFT JOIN opecat_mid_generalidades_unidades b ON a.id_generalidad=b.id_generalidad
                 LEFT JOIN opecat_mid_generalidades_unidades c ON a.id_unidad=c.id_unidad
                 LEFT JOIN opecat_mid_roles_generalidades d ON a.id_rol_generalidad=d.id_rol_generalidad
                 LEFT JOIN opetbl_mid_incidentes e ON a.id_folio_incidente=e.id_folio_incidente
                 LEFT JOIN opecat_mid_generalidades gen ON b.id_generalidad=gen.id_generalidad
                 LEFT JOIN opecat_mid_tipos_generalidades tip ON gen.id_tipo_generalidad=tip.id_tipo_generalidad
                 ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_igeneralidad' => $data['id_igeneralidad'],
                               'id_folio_incidente' => $data['id_folio_incidente'],
                               'id_rol_generalidad' => $data['id_rol_generalidad'],
                               'id_generalidad' => $data['id_generalidad'],
                               'id_unidad' => $data['id_unidad'],
                               'Cantidad' => $data['Cantidad'],
                               'Observaciones' => $data['Observaciones'],
                               'opecat_mid_generalidades_unidades_id_unidad' => $data['id_unidad'],
                               'opecat_mid_generalidades_unidades_id_generalidad' => $data['id_generalidad'],
                               'opecat_mid_roles_generalidades_rol_generalidad' => $data['rol_generalidad'],
                               'opetbl_mid_incidentes_prioridad' => $data['prioridad'],
                               'opetbl_mid_incidentes_fecha_hora_captura' => $data['fecha_hora_captura'],
                               'opetbl_mid_incidentes_fecha_incidente' => $data['fecha_incidente'],
                               'opetbl_mid_incidentes_dia' => $data['dia'],
                               'opetbl_mid_incidentes_mes' => $data['mes'],
                               'opetbl_mid_incidentes_annio' => $data['annio'],
                               'opetbl_mid_incidentes_hora_incidente' => $data['hora_incidente'],
                               'opetbl_mid_incidentes_id_estado' => $data['id_estado'],
                               'opetbl_mid_incidentes_id_region' => $data['id_region'],
                               'opetbl_mid_incidentes_id_cuartel' => $data['id_cuartel'],
                               'opetbl_mid_incidentes_id_municipio' => $data['id_municipio'],
                               'opetbl_mid_incidentes_direccion' => $data['direccion'],
                               'opetbl_mid_incidentes_localidad' => $data['localidad'],
                               'opetbl_mid_incidentes_colonia' => $data['colonia'],
                               'opetbl_mid_incidentes_calle' => $data['calle'],
                               'opetbl_mid_incidentes_latitud' => $data['latitud'],
                               'opetbl_mid_incidentes_longitud' => $data['longitud'],
                               'opetbl_mid_incidentes_id_asunto' => $data['id_asunto'],
                               'opetbl_mid_incidentes_id_fuente' => $data['id_fuente'],
                               'opetbl_mid_incidentes_hechos_html' => $data['hechos_html'],
                               'opetbl_mid_incidentes_hechos_text' => $data['hechos_text'],
                               'opetbl_mid_incidentes_ip_usuario' => $data['ip_usuario'],
                               'opetbl_mid_incidentes_id_usuario' => $data['id_usuario'],
                               
                               'opetbl_mid_generalidades_id_generalidad' => $data['id_generalidad'],
                               'opetbl_mid_tipos_generalidades_id_tipo_generalidad' => $data['id_tipo_generalidad'],
                               'opetbl_mid_tipos_generalidades_tipo_generalidad' => $data['tipo_generalidad'],                               
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_incidentes_generalidades(id_folio_incidente, id_rol_generalidad, id_generalidad, 
                id_unidad, Cantidad, Observaciones)
                VALUES(:id_folio_incidente, :id_rol_generalidad, :id_generalidad, 
                :id_unidad, :Cantidad, :Observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folio_incidente" => $this->id_folio_incidente, 
                                ":id_rol_generalidad" => $this->id_rol_generalidad, 
                                ":id_generalidad" => $this->id_generalidad, 
                                ":id_unidad" => $this->id_unidad, ":Cantidad" => $this->Cantidad, 
                                ":Observaciones" => $this->Observaciones));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_incidentes_generalidades
                   SET id_folio_incidente=:id_folio_incidente, id_rol_generalidad=:id_rol_generalidad, 
                   id_generalidad=:id_generalidad, id_unidad=:id_unidad, 
                   Cantidad=:Cantidad, Observaciones=:Observaciones
                WHERE id_igeneralidad=:id_igeneralidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_igeneralidad" => $this->id_igeneralidad, ":id_folio_incidente" => $this->id_folio_incidente, 
                                ":id_rol_generalidad" => $this->id_rol_generalidad, 
                                ":id_generalidad" => $this->id_generalidad, 
                                ":id_unidad" => $this->id_unidad, ":Cantidad" => $this->Cantidad, 
                                ":Observaciones" => $this->Observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>