<?php
/**
 *
 */
class OpecatMpcNacionalidades
{
    public $id_nacionalidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nacionalidad; /** @Tipo: varchar(80), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

     /**
     * Funci�n para mostrar la lista de Nacionalidades dentro de un combobox.
     * @param int $id, id de la nacionalidad seleccionada por deafult
     * @return array html(options)
     */
    public function getCat_Nacionalidades($id=81){
        $aryDatos = $this->selectAll('nacionalidad Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nacionalidad"] )
                $html .= '<option value="'.$datos["id_nacionalidad"].'" selected>'.$datos["nacionalidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nacionalidad"].'" >'.$datos["nacionalidad"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_nacionalidad)
    {
        $sql = "SELECT id_nacionalidad, nacionalidad
                FROM opecat_mpc_nacionalidades
                WHERE id_nacionalidad=:id_nacionalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_nacionalidad' => $id_nacionalidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_nacionalidad = $data['id_nacionalidad'];
            $this->nacionalidad = $data['nacionalidad'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_nacionalidad, a.nacionalidad
                FROM opecat_mpc_nacionalidades a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_nacionalidad' => $data['id_nacionalidad'],
                               'nacionalidad' => $data['nacionalidad'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mpc_nacionalidades(id_nacionalidad, nacionalidad)
                VALUES(:id_nacionalidad, :nacionalidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_nacionalidad" => $this->id_nacionalidad, ":nacionalidad" => $this->nacionalidad));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mpc_nacionalidades
                   SET nacionalidad=:nacionalidad
                WHERE id_nacionalidad=:id_nacionalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_nacionalidad" => $this->id_nacionalidad, ":nacionalidad" => $this->nacionalidad));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>