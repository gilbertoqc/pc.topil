<?php
/**
 *
 */
class OpetblMcsXcorreos
{
    public $id_correo; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $correo; /** @Tipo: varchar(60), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $Nombre; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ApellidoPaterno; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ApellidoMaterno; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_correo)
    {
        $sql = "SELECT id_correo, correo, Nombre, ApellidoPaterno, ApellidoMaterno
                FROM opetbl_mcs_xcorreos
                WHERE id_correo=:id_correo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_correo' => $id_correo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_correo = $data['id_correo'];
            $this->correo = $data['correo'];
            $this->Nombre = $data['Nombre'];
            $this->ApellidoPaterno = $data['ApellidoPaterno'];
            $this->ApellidoMaterno = $data['ApellidoMaterno'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_correo, a.correo, a.Nombre, a.ApellidoPaterno, a.ApellidoMaterno
                FROM opetbl_mcs_xcorreos a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_correo' => $data['id_correo'],
                               'correo' => $data['correo'],
                               'Nombre' => $data['Nombre'],
                               'ApellidoPaterno' => $data['ApellidoPaterno'],
                               'ApellidoMaterno' => $data['ApellidoMaterno'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mcs_xcorreos(id_correo, correo, Nombre, ApellidoPaterno, ApellidoMaterno)
                VALUES(:id_correo, :correo, :Nombre, :ApellidoPaterno, :ApellidoMaterno);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_correo" => $this->id_correo, ":correo" => $this->correo, ":Nombre" => $this->Nombre, ":ApellidoPaterno" => $this->ApellidoPaterno, ":ApellidoMaterno" => $this->ApellidoMaterno));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mcs_xcorreos
                   SET correo=:correo, Nombre=:Nombre, ApellidoPaterno=:ApellidoPaterno, ApellidoMaterno=:ApellidoMaterno
                WHERE id_correo=:id_correo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_correo" => $this->id_correo, ":correo" => $this->correo, ":Nombre" => $this->Nombre, ":ApellidoPaterno" => $this->ApellidoPaterno, ":ApellidoMaterno" => $this->ApellidoMaterno));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>