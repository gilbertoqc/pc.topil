<?php
/**
 *
 */
class AdmtblReferencias
{
    public $id_referencia; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_referencia; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_tipo_relacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $nombre; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_paterno; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_materno; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $sexo; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ocupacion; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $calle_num; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $colonia; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ciudad; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cp; /** @Tipo: varchar(5), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_fijo; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_movil; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoRelacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    
    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_municipios.class.php';
        require_once 'admtbl_datos_personales.class.php';        
        require_once 'admcat_tipo_relacion.class.php';
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatTipoRelacion = new AdmcatTipoRelacion();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_referencia)
    {
        $sql = "SELECT id_referencia, curp, id_tipo_referencia, id_tipo_relacion, nombre, a_paterno, a_materno, sexo, ocupacion, calle_num, colonia, ciudad, cp, id_municipio, id_entidad, tel_fijo, tel_movil
                FROM admtbl_referencias
                WHERE id_referencia=:id_referencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_referencia' => $id_referencia));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_referencia = $data['id_referencia'];
            $this->curp = $data['curp'];
            $this->id_tipo_referencia = $data['id_tipo_referencia'];
            $this->id_tipo_relacion = $data['id_tipo_relacion'];
            $this->nombre = $data['nombre'];
            $this->a_paterno = $data['a_paterno'];
            $this->a_materno = $data['a_materno'];
            $this->sexo = $data['sexo'];
            $this->ocupacion = $data['ocupacion'];
            $this->calle_num = $data['calle_num'];
            $this->colonia = $data['colonia'];
            $this->ciudad = $data['ciudad'];
            $this->cp = $data['cp'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_entidad = $data['id_entidad'];
            $this->tel_fijo = $data['tel_fijo'];
            $this->tel_movil = $data['tel_movil'];

            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatTipoRelacion->select($this->id_tipo_relacion, $this->id_tipo_referencia);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_referencia, a.curp, a.id_tipo_referencia, a.id_tipo_relacion, a.nombre, a.a_paterno, a.a_materno, a.sexo, a.ocupacion, a.calle_num, a.colonia, a.ciudad, a.cp, a.id_municipio, a.id_entidad, a.tel_fijo, a.tel_movil,
                  b.municipio, b.cabecera,
                  d.tipo_referencia,
                  e.tipo_relacion 
                FROM admtbl_referencias a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio 
                    LEFT JOIN admcat_tipo_referencia d ON a.id_tipo_referencia=d.id_tipo_referencia
                    LEFT JOIN admcat_tipo_relacion e ON a.id_tipo_relacion=e.id_tipo_relacion";
        if (!empty($sqlWhere))
            $sql .= "\nWHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= "\nORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= "\nLIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_referencia' => $data['id_referencia'],
                               'curp' => $data['curp'],
                               'id_tipo_referencia' => $data['id_tipo_referencia'],
                               'id_tipo_relacion' => $data['id_tipo_relacion'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'sexo' => $data['sexo'],
                               'ocupacion' => $data['ocupacion'],
                               'calle_num' => $data['calle_num'],
                               'colonia' => $data['colonia'],
                               'ciudad' => $data['ciudad'],
                               'cp' => $data['cp'],
                               'id_municipio' => $data['id_municipio'],
                               'id_entidad' => $data['id_entidad'],
                               'tel_fijo' => $data['tel_fijo'],
                               'tel_movil' => $data['tel_movil'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],
                               'tipo_referencia' => $data['tipo_referencia'],
                               'tipo_relacion' => $data['tipo_relacion'],                               
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_referencias(id_referencia, curp, id_tipo_referencia, id_tipo_relacion, nombre, a_paterno, a_materno, sexo, ocupacion, calle_num, colonia, ciudad, cp, id_municipio, id_entidad, tel_fijo, tel_movil)
                VALUES(:id_referencia, :curp, :id_tipo_referencia, :id_tipo_relacion, :nombre, :a_paterno, :a_materno, :sexo, :ocupacion, :calle_num, :colonia, :ciudad, :cp, :id_municipio, :id_entidad, :tel_fijo, :tel_movil);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_referencia" => $this->id_referencia, ":curp" => $this->curp, ":id_tipo_referencia" => $this->id_tipo_referencia, ":id_tipo_relacion" => $this->id_tipo_relacion, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":sexo" => $this->sexo, ":ocupacion" => $this->ocupacion, ":calle_num" => $this->calle_num, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":cp" => $this->cp, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_referencias
                   SET id_tipo_referencia=:id_tipo_referencia, id_tipo_relacion=:id_tipo_relacion, nombre=:nombre, a_paterno=:a_paterno, a_materno=:a_materno, sexo=:sexo, ocupacion=:ocupacion, calle_num=:calle_num, colonia=:colonia, ciudad=:ciudad, cp=:cp, id_municipio=:id_municipio, id_entidad=:id_entidad, tel_fijo=:tel_fijo, tel_movil=:tel_movil
                WHERE id_referencia=:id_referencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_referencia" => $this->id_referencia, ":id_tipo_referencia" => $this->id_tipo_referencia, ":id_tipo_relacion" => $this->id_tipo_relacion, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":sexo" => $this->sexo, ":ocupacion" => $this->ocupacion, ":calle_num" => $this->calle_num, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":cp" => $this->cp, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}

?>