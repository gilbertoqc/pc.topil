<?php
/**
 *
 */
class LogtblExpExpediente
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_estatus; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $constancia; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cartilla_smn; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $medico; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $toxicologico; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $psicologico; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ant_np; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatExpEstatus; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_exp_estatus.class.php';
        require_once 'admtbl_datos_personales.class.php';
        $this->LogcatExpEstatus = new LogcatExpEstatus();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, id_estatus, constancia, cartilla_smn, medico, toxicologico, psicologico, ant_np
                FROM logtbl_exp_expediente
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->id_estatus = $data['id_estatus'];
            $this->constancia = $data['constancia'];
            $this->cartilla_smn = $data['cartilla_smn'];
            $this->medico = $data['medico'];
            $this->toxicologico = $data['toxicologico'];
            $this->psicologico = $data['psicologico'];
            $this->ant_np = $data['ant_np'];

            $this->LogcatExpEstatus->select($this->id_estatus);
            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para verificar el estatus del expediente de un elemento 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function chkEstatus( $curp, $valor )
    {
        $sql = "SELECT *
                FROM logtbl_exp_expediente
                WHERE curp=:curp
                AND constancia=:constancia
                AND cartilla_smn=:cartilla_smn
                AND medico=:medico
                AND toxicologico=:toxicologico
                AND psicologico=:psicologico
                AND ant_np=:ant_np;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute( array(":curp" => $curp, ":constancia" => $valor, ":cartilla_smn" => $valor, ":medico" => $valor, ":toxicologico" => $valor, ":psicologico" => $valor, ":ant_np" => $valor) );
            if( $qry->fetch(PDO::FETCH_NUM) > 0 ){                
                return true;
            }else{                        
                return false;
            }
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.id_estatus, a.constancia, a.cartilla_smn, a.medico, a.toxicologico, a.psicologico, a.ant_np,
                  b.id_estatus, b.estatus, b.xstat,
                  c.curp, c.nombre, c.a_paterno, c.a_materno, c.fecha_nac, c.genero, c.rfc, c.cuip, c.folio_ife, c.mat_cartilla, c.licencia_conducir, c.pasaporte, c.id_estado_civil, c.id_tipo_sangre, c.id_nacionalidad, c.id_entidad, c.id_municipio, c.lugar_nac, c.id_status
                FROM logtbl_exp_expediente a 
                 LEFT JOIN logcat_exp_estatus b ON a.id_estatus=b.id_estatus
                 LEFT JOIN admtbl_datos_personales c ON a.curp=c.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'id_estatus' => $data['id_estatus'],
                               'constancia' => $data['constancia'],
                               'cartilla_smn' => $data['cartilla_smn'],
                               'medico' => $data['medico'],
                               'toxicologico' => $data['toxicologico'],
                               'psicologico' => $data['psicologico'],
                               'ant_np' => $data['ant_np'],
                               'logcat_exp_estatus_estatus' => $data['estatus'],
                               'logcat_exp_estatus_xstat' => $data['xstat'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     
    public function insert()
    {
        $sql = "INSERT INTO logtbl_exp_expediente(curp, id_estatus, constancia, cartilla_smn, medico, toxicologico, psicologico, ant_np)
                VALUES(:curp, :id_estatus, :constancia, :cartilla_smn, :medico, :toxicologico, :psicologico, :ant_np);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_estatus" => $this->id_estatus, ":constancia" => $this->constancia, ":cartilla_smn" => $this->cartilla_smn, ":medico" => $this->medico, ":toxicologico" => $this->toxicologico, ":psicologico" => $this->psicologico, ":ant_np" => $this->ant_np));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    */

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert( $curp )
    {
        $sql = "INSERT INTO logtbl_exp_expediente(curp)
                VALUES(:curp);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     
    public function update()
    {
        $sql = "UPDATE logtbl_exp_expediente
                   SET id_estatus=:id_estatus, constancia=:constancia, cartilla_smn=:cartilla_smn, medico=:medico, toxicologico=:toxicologico, psicologico=:psicologico, ant_np=:ant_np
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_estatus" => $this->id_estatus, ":constancia" => $this->constancia, ":cartilla_smn" => $this->cartilla_smn, ":medico" => $this->medico, ":toxicologico" => $this->toxicologico, ":psicologico" => $this->psicologico, ":ant_np" => $this->ant_np));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    */
    
    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update( $documento, $valor )
    {
        //instrucciones que evaluan que dato se actualiza.
        switch( $documento ){
            //el estatus puede ser el siguiente
            //estatus = 0 sin documentacion
            //estatus = 1 COMPLETO
            //estatus = 2 incompleto
            case 'estatus':
                $set = " id_estatus=:id_estatus ";
                $arry = array(":curp" => $this->curp, ":id_estatus" => $valor );
                break;
                
            case 'constancia':
                $set = " constancia=:constancia ";
                $arry = array(":curp" => $this->curp, ":constancia" => $valor );
                break;
            
            case 'cartilla_smn':
                $set = " cartilla_smn=:cartilla_smn ";
                $arry = array(":curp" => $this->curp, ":cartilla_smn" => $valor );
                break;
            
            case 'medico':
                $set = " medico=:medico ";
                $arry = array(":curp" => $this->curp, ":medico" => $valor );
                break;
                
            case 'toxicologico':
                $set = " toxicologico=:toxicologico ";
                $arry = array(":curp" => $this->curp, ":toxicologico" => $valor );
                break;
                
            case 'psicologico':
                $set = " psicologico=:psicologico ";
                $arry = array(":curp" => $this->curp, ":psicologico" => $valor );
                break;
                
            case 'ant_np':
                $set = " ant_np=:ant_np ";
                $arry = array(":curp" => $this->curp, ":ant_np" => $valor );
                break;
                
            default:
                $set = "";
                $arry = "";
                break;                          
        
        }
        
        $sql = "UPDATE logtbl_exp_expediente
                   SET " . $set . "
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute( $arry );
            if ($qry) 
                return true;
            else
                return 'chafa';
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return 'nada';
        }
    }
    
    /**
     * Funci�n para actualizar el estatus del expediente del elemento
     * @return boolean true si el proceso es satisfactorio
    */ 
    public function updEstatus( $curp, $estatus )
    {
        $sql = "UPDATE logtbl_exp_expediente
                   SET id_estatus=:id_estatus 
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_estatus" => $estatus));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    public function delete()
    {

    }
}


?>