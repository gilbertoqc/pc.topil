<?php
/**
 *
 */
class Ope_Funciones
{


/**
 * Función para mostrar la lista de Horas dentro de un combobox.
 * @param int $id, id .... seleccionado por deafult
 * @return array html(options)
 */
  public function getCat_Horas($id=00){
	    for($h=0;$h<=23;$h++){
	          if(strlen($h)<=1){$h="0".$h;}

	          if($id==$h){
	          	$html .= '<option value="'. $h .'" selected>'. $h .'</option>';
	          }else{
	          	$html .= '<option value="'. $h .'">'. $h .'</option>';
	          }
			}

	return $html;
  }


/**
 * Función para mostrar la lista de Minutos dentro de un combobox.
 * @param int $id, id .... seleccionado por deafult
 * @return array html(options)
 */
  public function getCat_Minutos($id=00){
	    for($m=0;$m<=59;$m++){
	          if(strlen($m)<=1){$m="0".$m;}

	          if($id==$m){
	          	$html .= '<option value="'. $m .'" selected>'. $m .'</option>';
	          }else{
	          	$html .= '<option value="'. $m .'">'. $m .'</option>';
	          }
			}

	return $html;
  }


/*Fin de la Clase Ope_Funciones   */
}
?>