<?php
/**
 *
 */
class AdmtblDatosPersonales
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nombre; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_paterno; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $a_materno; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_nac; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $genero; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $rfc; /** @Tipo: varchar(14), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cuip; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio_ife; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $mat_cartilla; /** @Tipo: varchar(10), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $licencia_conducir; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $pasaporte; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_estado_civil; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_sangre; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_nacionalidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $lugar_nac; /** @Tipo: varchar(250), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_status; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEstadoCivil; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatNacionalidad; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatStatus; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoSangre; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_municipios.class.php';
        require_once 'admcat_estado_civil.class.php';
        require_once 'admcat_nacionalidad.class.php';
        require_once 'admcat_status.class.php';
        require_once 'admcat_tipo_sangre.class.php';
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmcatEstadoCivil = new AdmcatEstadoCivil();
        $this->AdmcatNacionalidad = new AdmcatNacionalidad();
        $this->AdmcatStatus = new AdmcatStatus();
        $this->AdmcatTipoSangre = new AdmcatTipoSangre();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, nombre, a_paterno, a_materno, fecha_nac, genero, rfc, cuip, folio_ife, mat_cartilla, licencia_conducir, 
                    pasaporte, id_estado_civil, id_tipo_sangre, id_nacionalidad, id_entidad, id_municipio, lugar_nac, id_status
                FROM admtbl_datos_personales
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->nombre = $data['nombre'];
            $this->a_paterno = $data['a_paterno'];
            $this->a_materno = $data['a_materno'];
            $this->fecha_nac = $data['fecha_nac'];
            $this->genero = $data['genero'];
            $this->rfc = $data['rfc'];
            $this->cuip = $data['cuip'];
            $this->folio_ife = $data['folio_ife'];
            $this->mat_cartilla = $data['mat_cartilla'];
            $this->licencia_conducir = $data['licencia_conducir'];
            $this->pasaporte = $data['pasaporte'];
            $this->id_estado_civil = $data['id_estado_civil'];
            $this->id_tipo_sangre = $data['id_tipo_sangre'];
            $this->id_nacionalidad = $data['id_nacionalidad'];
            $this->id_entidad = $data['id_entidad'];
            $this->id_municipio = $data['id_municipio'];
            $this->lugar_nac = $data['lugar_nac'];
            $this->id_status = $data['id_status'];

            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmcatEstadoCivil->select($this->id_estado_civil);
            $this->AdmcatNacionalidad->select($this->id_nacionalidad);
            $this->AdmcatStatus->select($this->id_status);
            $this->AdmcatTipoSangre->select($this->id_tipo_sangre);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.fecha_nac, a.genero, a.rfc, a.cuip, a.folio_ife, a.mat_cartilla, a.licencia_conducir, 
                    a.pasaporte, a.id_estado_civil, a.id_tipo_sangre, a.id_nacionalidad, a.id_entidad, a.id_municipio, a.lugar_nac, a.id_status,
                    b.municipio, b.cabecera, c.estado_civil, d.nacionalidad, e.status, f.tipo_sangre
                FROM admtbl_datos_personales a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                    LEFT JOIN admcat_estado_civil c ON a.id_estado_civil=c.id_estado_civil
                    LEFT JOIN admcat_nacionalidad d ON a.id_nacionalidad=d.id_nacionalidad
                    LEFT JOIN admcat_status e ON a.id_status=e.id_status
                    LEFT JOIN admcat_tipo_sangre f ON a.id_tipo_sangre=f.id_tipo_sangre";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'fecha_nac' => $data['fecha_nac'],
                               'genero' => $data['genero'],
                               'rfc' => $data['rfc'],
                               'cuip' => $data['cuip'],
                               'folio_ife' => $data['folio_ife'],
                               'mat_cartilla' => $data['mat_cartilla'],
                               'licencia_conducir' => $data['licencia_conducir'],
                               'pasaporte' => $data['pasaporte'],
                               'id_estado_civil' => $data['id_estado_civil'],
                               'id_tipo_sangre' => $data['id_tipo_sangre'],
                               'id_nacionalidad' => $data['id_nacionalidad'],
                               'id_entidad' => $data['id_entidad'],
                               'id_municipio' => $data['id_municipio'],
                               'lugar_nac' => $data['lugar_nac'],
                               'id_status' => $data['id_status'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],                               
                               'estado_civil' => $data['estado_civil'],
                               'nacionalidad' => $data['nacionalidad'],
                               'status' => $data['status'],
                               'tipo_sangre' => $data['tipo_sangre'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }

    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.fecha_nac, a.genero, a.rfc, a.cuip, a.folio_ife, 
                    a.id_estado_civil, a.id_tipo_sangre, a.id_entidad, a.id_municipio, a.lugar_nac, a.id_status,
                    b.municipio, b.cabecera, c.estado_civil, e.status, f.tipo_sangre,
                    h.categoria, i.area, j.especialidad, k.nivel_mando 
                FROM admtbl_datos_personales a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                    LEFT JOIN admcat_estado_civil c ON a.id_estado_civil=c.id_estado_civil                    
                    LEFT JOIN admcat_status e ON a.id_status=e.id_status
                    LEFT JOIN admcat_tipo_sangre f ON a.id_tipo_sangre=f.id_tipo_sangre 
                    JOIN admtbl_adscripcion g ON a.curp=g.curp 
                    LEFT JOIN admcat_categorias h ON g.id_categoria=h.id_categoria 
                    LEFT JOIN admcat_areas i ON g.id_area=i.id_area 
                    LEFT JOIN admcat_especialidades j ON g.id_especialidad=j.id_especialidad 
                    LEFT JOIN admcat_nivel_mando k ON g.id_nivel_mando=k.id_nivel_mando";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'fecha_nac' => $data['fecha_nac'],
                               'genero' => $data['genero'],
                               'rfc' => $data['rfc'],
                               'cuip' => $data['cuip'],
                               'folio_ife' => $data['folio_ife'],                               
                               'id_estado_civil' => $data['id_estado_civil'],
                               'id_tipo_sangre' => $data['id_tipo_sangre'],                               
                               'id_entidad' => $data['id_entidad'],
                               'id_municipio' => $data['id_municipio'],
                               'lugar_nac' => $data['lugar_nac'],
                               'id_status' => $data['id_status'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],                               
                               'estado_civil' => $data['estado_civil'],
                               'status' => $data['status'],
                               'tipo_sangre' => $data['tipo_sangre'],
                               'area' => $data['area'],
                               'categoria' => $data['categoria'],
                               'especialidad' => $data['especialidad'],
                               'nivel_mando' => $data['nivel_mando'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*) 
                FROM admtbl_datos_personales a                     
                    JOIN admtbl_adscripcion g ON a.curp=g.curp 
                    LEFT JOIN admcat_categorias h ON g.id_categoria=h.id_categoria 
                    LEFT JOIN admcat_areas i ON g.id_area=i.id_area 
                    LEFT JOIN admcat_especialidades j ON g.id_especialidad=j.id_especialidad";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para agregar un nuevo registro a la tabla     
     * @return boolean verdadero si se ejectu� correctamente o falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_datos_personales(curp, nombre, a_paterno, a_materno, fecha_nac, genero, rfc, cuip, folio_ife, mat_cartilla, licencia_conducir, pasaporte, id_estado_civil, id_tipo_sangre, id_nacionalidad, id_entidad, id_municipio, lugar_nac, id_status)
                VALUES(:curp, :nombre, :a_paterno, :a_materno, :fecha_nac, :genero, :rfc, :cuip, :folio_ife, :mat_cartilla, :licencia_conducir, :pasaporte, :id_estado_civil, :id_tipo_sangre, :id_nacionalidad, :id_entidad, :id_municipio, :lugar_nac, :id_status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":fecha_nac" => $this->fecha_nac, ":genero" => $this->genero, ":rfc" => $this->rfc, ":cuip" => $this->cuip, ":folio_ife" => $this->folio_ife, ":mat_cartilla" => $this->mat_cartilla, ":licencia_conducir" => $this->licencia_conducir, ":pasaporte" => $this->pasaporte, ":id_estado_civil" => $this->id_estado_civil, ":id_tipo_sangre" => $this->id_tipo_sangre, ":id_nacionalidad" => $this->id_nacionalidad, ":id_entidad" => $this->id_entidad, ":id_municipio" => $this->id_municipio, ":lugar_nac" => $this->lugar_nac, ":id_status" => $this->id_status));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_datos_personales
                   SET nombre=:nombre, a_paterno=:a_paterno, a_materno=:a_materno, fecha_nac=:fecha_nac, genero=:genero, rfc=:rfc, cuip=:cuip, folio_ife=:folio_ife, mat_cartilla=:mat_cartilla, licencia_conducir=:licencia_conducir, pasaporte=:pasaporte, id_estado_civil=:id_estado_civil, id_tipo_sangre=:id_tipo_sangre, id_nacionalidad=:id_nacionalidad, id_entidad=:id_entidad, id_municipio=:id_municipio, lugar_nac=:lugar_nac, id_status=:id_status
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":nombre" => $this->nombre, ":a_paterno" => $this->a_paterno, ":a_materno" => $this->a_materno, ":fecha_nac" => $this->fecha_nac, ":genero" => $this->genero, ":rfc" => $this->rfc, ":cuip" => $this->cuip, ":folio_ife" => $this->folio_ife, ":mat_cartilla" => $this->mat_cartilla, ":licencia_conducir" => $this->licencia_conducir, ":pasaporte" => $this->pasaporte, ":id_estado_civil" => $this->id_estado_civil, ":id_tipo_sangre" => $this->id_tipo_sangre, ":id_nacionalidad" => $this->id_nacionalidad, ":id_entidad" => $this->id_entidad, ":id_municipio" => $this->id_municipio, ":lugar_nac" => $this->lugar_nac, ":id_status" => $this->id_status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete($curp)
    {
        $sql = "UPDATE admtbl_datos_personales
                   SET  id_status=2 
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $curp));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>