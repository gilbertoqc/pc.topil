<?php
/**
 *
 */
class AdmtblPagosPrestaciones
{
    public $id_motivo_pago; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_prest_personal; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_reg; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_pago; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMotivoPago; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblPrestaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_motivo_pago.class.php';
        require_once 'admtbl_prestaciones.class.php';
        $this->AdmcatMotivoPago = new AdmcatMotivoPago();
        $this->AdmtblPrestaciones = new AdmtblPrestaciones();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_motivo_pago, $id_prest_personal)
    {
        $sql = "SELECT id_motivo_pago, id_prest_personal, fecha_reg, fecha_pago, status
                FROM admtbl_pagos_prestaciones
                WHERE id_motivo_pago=:id_motivo_pago AND id_prest_personal=:id_prest_personal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_motivo_pago' => $id_motivo_pago, ':id_prest_personal' => $id_prest_personal));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_motivo_pago = $data['id_motivo_pago'];
            $this->id_prest_personal = $data['id_prest_personal'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->fecha_pago = $data['fecha_pago'];
            $this->status = $data['status'];

            $this->AdmcatMotivoPago->select($this->id_motivo_pago);
            $this->AdmtblPrestaciones->select($this->id_prest_personal);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_motivo_pago, a.id_prest_personal, a.fecha_reg, a.fecha_pago, a.status,
                  b.id_motivo_pago, b.motivo_pago, b.status,
                  c.id_prest_personal, c.curp, c.fecha_alta, c.fecha_reg, c.observaciones, c.id_prestacion, c.id_status_prest
                FROM admtbl_pagos_prestaciones a 
                 LEFT JOIN admcat_motivo_pago b ON a.id_motivo_pago=b.id_motivo_pago
                 LEFT JOIN admtbl_prestaciones c ON a.id_prest_personal=c.id_prest_personal";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_motivo_pago' => $data['id_motivo_pago'],
                               'id_prest_personal' => $data['id_prest_personal'],
                               'fecha_reg' => $data['fecha_reg'],
                               'fecha_pago' => $data['fecha_pago'],
                               'status' => $data['status'],
                               'admcat_motivo_pago_motivo_pago' => $data['motivo_pago'],
                               'admcat_motivo_pago_status' => $data['status'],
                               'admtbl_prestaciones_curp' => $data['curp'],
                               'admtbl_prestaciones_fecha_alta' => $data['fecha_alta'],
                               'admtbl_prestaciones_fecha_reg' => $data['fecha_reg'],
                               'admtbl_prestaciones_observaciones' => $data['observaciones'],
                               'admtbl_prestaciones_id_prestacion' => $data['id_prestacion'],
                               'admtbl_prestaciones_id_status_prest' => $data['id_status_prest'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_pagos_prestaciones(id_motivo_pago, id_prest_personal, fecha_reg, fecha_pago, status)
                VALUES(:id_motivo_pago, :id_prest_personal, :fecha_reg, :fecha_pago, :status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_motivo_pago" => $this->id_motivo_pago, ":id_prest_personal" => $this->id_prest_personal, ":fecha_reg" => $this->fecha_reg, ":fecha_pago" => $this->fecha_pago, ":status" => $this->status));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_pagos_prestaciones
                   SET fecha_reg=:fecha_reg, fecha_pago=:fecha_pago, status=:status
                WHERE id_motivo_pago=:id_motivo_pago AND id_prest_personal=:id_prest_personal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_motivo_pago" => $this->id_motivo_pago, ":id_prest_personal" => $this->id_prest_personal, ":fecha_reg" => $this->fecha_reg, ":fecha_pago" => $this->fecha_pago, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>