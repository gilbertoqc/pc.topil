<?php
/**
 *
 */
class LogcatVehMarca
{
    public $id_marca; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $marca; /** @Tipo: varchar(75), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_marca_sn; /** @Tipo: int(5), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de marcas dentro de un combobox.
     * @param int $id, id de la marca seleccionada por deafult     
     * @return array html(options)
     */
    public function shwMarca($id=0){
        $aryDatos = $this->selectAll('a.xstat=1 ', 'marca Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_marca"] )
                $html .= '<option value="'.$datos["id_marca"].'" selected>'.$datos["marca"].'</option>';
            else
                $html .= '<option value="'.$datos["id_marca"].'" >'.$datos["marca"].'</option>';
        }
        return $html;
    }

	public function getIdMarca( $id_tipo ){
	
		$sql = "SELECT m.id_marca
                FROM logcat_veh_marca as m
				INNER JOIN logcat_veh_tipo as t On t.id_marca=m.id_marca
				WHERE t.id_tipo=:id_tipo
				AND t.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo' => $id_tipo, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            return $data['id_marca'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_marca)
    {
        $sql = "SELECT id_marca, marca, id_marca_sn, xstat
                FROM logcat_veh_marca
                WHERE id_marca=:id_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_marca = $data['id_marca'];
            $this->marca = $data['marca'];
            $this->id_marca_sn = $data['id_marca_sn'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_marca, a.marca, a.id_marca_sn, a.xstat
                FROM logcat_veh_marca a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_marca' => $data['id_marca'],
                               'marca' => $data['marca'],
                               'id_marca_sn' => $data['id_marca_sn'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_marca(id_marca, marca, id_marca_sn, xstat)
                VALUES(:id_marca, :marca, :id_marca_sn, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":marca" => $this->marca, ":id_marca_sn" => $this->id_marca_sn, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_marca
                   SET marca=:marca, id_marca_sn=:id_marca_sn, xstat=:xstat
                WHERE id_marca=:id_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":marca" => $this->marca, ":id_marca_sn" => $this->id_marca_sn, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>