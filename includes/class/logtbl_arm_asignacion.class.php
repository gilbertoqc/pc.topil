<?php
/**
 *
 */
class LogtblArmAsignacion
{
    public $matricula; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogtblArmArmamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'logtbl_arm_armamento.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->LogtblArmArmamento = new LogtblArmArmamento();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($matricula, $curp)
    {
        $sql = "SELECT matricula, curp
                FROM logtbl_arm_asignacion
                WHERE matricula=:matricula AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':matricula' => $matricula, ':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->matricula = $data['matricula'];
            $this->curp = $data['curp'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->LogtblArmArmamento->select($this->matricula);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * PARA PERSONAL EN LICENCIA CON SUS RESPECTIVOS CRITERIOS PARA EL GRID PRINCIPAL
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGridPer($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCountPer($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMinPer($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     *  PARA PERSONAL EN LICENCIA CON SUS RESPECTIVOS CRITERIOS PARA EL GRID PRINCIPAL
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMinPer($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.genero, a.id_entidad, a.id_municipio, a.id_status,         
                    b.municipio, b.cabecera, h.categoria, i.area, j.especialidad  
                    FROM admtbl_datos_personales a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio                                        
                    LEFT JOIN admcat_status e ON a.id_status=e.id_status                     
                    JOIN admtbl_adscripcion g ON a.curp=g.curp 
                    LEFT JOIN admcat_categorias h ON g.id_categoria=h.id_categoria 
                    LEFT JOIN admcat_areas i ON g.id_area=i.id_area 
                    LEFT JOIN admcat_especialidades j ON g.id_especialidad=j.id_especialidad
                    WHERE ( a.id_status=1 OR a.id_status=3 ) 
                        AND g.id_corporacion=1 
                        AND ( g.id_tipo_funcion=1 OR g.id_tipo_funcion=3 )";
        if (!empty($sqlWhere)) {
            $sql .= "\n AND $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               //'fecha_nac' => $data['fecha_nac'],
                               'genero' => $data['genero'],
                               //'rfc' => $data['rfc'],
                               //'cuip' => $data['cuip'],
                               //'folio_ife' => $data['folio_ife'],                               
                               //'id_estado_civil' => $data['id_estado_civil'],
                               //'id_tipo_sangre' => $data['id_tipo_sangre'],                               
                               'id_entidad' => $data['id_entidad'],
                               'id_municipio' => $data['id_municipio'],
                               //'lugar_nac' => $data['lugar_nac'],
                               'id_status' => $data['id_status'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],                               
                               //'estado_civil' => $data['estado_civil'],                               
                               //'status' => $data['status'],
                               //'tipo_sangre' => $data['tipo_sangre'],
                               'area' => $data['area'],
                               'categoria' => $data['categoria'],
                               'especialidad' => $data['especialidad'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * PARA PERSONAL EN LICENCIA CON SUS RESPECTIVOS CRITERIOS PARA EL GRID PRINCIPAL
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCountPer($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*) 
                FROM admtbl_datos_personales a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio                                        
                    LEFT JOIN admcat_status e ON a.id_status=e.id_status                     
                    JOIN admtbl_adscripcion g ON a.curp=g.curp 
                    LEFT JOIN admcat_categorias h ON g.id_categoria=h.id_categoria 
                    LEFT JOIN admcat_areas i ON g.id_area=i.id_area 
                    LEFT JOIN admcat_especialidades j ON g.id_especialidad=j.id_especialidad
                    WHERE ( a.id_status=1 OR a.id_status=3 ) 
                        AND g.id_corporacion=1 
                        AND ( g.id_tipo_funcion=1 OR g.id_tipo_funcion=3 )";
        if (!empty($sqlWhere)) {
            $sql .= "\nAND $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllPer($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.nombre, a.a_paterno, a.a_materno, a.fecha_nac, a.genero, a.rfc, a.cuip, a.folio_ife, a.mat_cartilla, a.licencia_conducir, 
                    a.pasaporte, a.id_estado_civil, a.id_tipo_sangre, a.id_nacionalidad, a.id_entidad, a.id_municipio, a.lugar_nac, a.id_status,
                    b.municipio, b.cabecera, c.estado_civil, d.nacionalidad, e.status, f.tipo_sangre
                FROM admtbl_datos_personales a 
                    LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                    LEFT JOIN admcat_estado_civil c ON a.id_estado_civil=c.id_estado_civil
                    LEFT JOIN admcat_nacionalidad d ON a.id_nacionalidad=d.id_nacionalidad
                    LEFT JOIN admcat_status e ON a.id_status=e.id_status
                    LEFT JOIN admcat_tipo_sangre f ON a.id_tipo_sangre=f.id_tipo_sangre";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'nombre' => $data['nombre'],
                               'a_paterno' => $data['a_paterno'],
                               'a_materno' => $data['a_materno'],
                               'fecha_nac' => $data['fecha_nac'],
                               'genero' => $data['genero'],
                               'rfc' => $data['rfc'],
                               'cuip' => $data['cuip'],
                               'folio_ife' => $data['folio_ife'],
                               'mat_cartilla' => $data['mat_cartilla'],
                               'licencia_conducir' => $data['licencia_conducir'],
                               'pasaporte' => $data['pasaporte'],
                               'id_estado_civil' => $data['id_estado_civil'],
                               'id_tipo_sangre' => $data['id_tipo_sangre'],
                               'id_nacionalidad' => $data['id_nacionalidad'],
                               'id_entidad' => $data['id_entidad'],
                               'id_municipio' => $data['id_municipio'],
                               'lugar_nac' => $data['lugar_nac'],
                               'id_status' => $data['id_status'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],                               
                               'estado_civil' => $data['estado_civil'],
                               'nacionalidad' => $data['nacionalidad'],
                               'status' => $data['status'],
                               'tipo_sangre' => $data['tipo_sangre'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }    

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.matricula, a.curp,
                  b.curp,
                  c.matricula, c.serie, c.id_loc, c.id_marca, c.id_foliod, c.id_situacion, c.id_propietario, c.id_estado, c.id_estatus, c.id_motivo_movimiento                  
                FROM logtbl_arm_asignacion a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp
                 LEFT JOIN logtbl_arm_armamento c ON a.matricula=c.matricula";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'matricula' => $data['matricula'],
                               'curp' => $data['curp'],
                               'logtbl_arm_armamento_serie' => $data['serie'],
                               'logtbl_arm_armamento_id_loc' => $data['id_loc'],
                               'logtbl_arm_armamento_id_marca' => $data['id_marca'],
                               'logtbl_arm_armamento_id_foliod' => $data['id_foliod'],
                               'logtbl_arm_armamento_id_situacion' => $data['id_situacion'],
                               'logtbl_arm_armamento_id_propietario' => $data['id_propietario'],                               
                               'logtbl_arm_armamento_id_estado' => $data['id_estado'],
                               'logtbl_arm_armamento_id_estatus' => $data['id_estatus'],
                               'logtbl_arm_armamento_id_motivo_movimiento' => $data['id_motivo_movimiento'],
                               'logtbl_arm_armamento_id_ubicacion' => $data['id_ubicacion'],
                               'logtbl_arm_armamento_id_corporacion' => $data['id_corporacion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlValues, cadena que contiene el parametro de busqueda.
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
     
     /*
create view viewarmlstasignadas as
SELECT a.matricula, a.serie, c.estado, d.estatus, e.foliod, g.loc, h.marca,
       mo.modelo, cal.calibre, cla.clase, tip.tipo,
       i.motivo_movimiento, j.propietario, k.situacion, asig.curp
       FROM logtbl_arm_armamento a
       LEFT JOIN logtbl_arm_asignacion asig ON a.matricula=asig.matricula
       LEFT JOIN logcat_arm_estado c ON a.id_estado=c.id_estado
       LEFT JOIN logcat_arm_estatus d ON a.id_estatus=d.id_estatus
       LEFT JOIN logcat_arm_foliod e ON a.id_foliod=e.id_foliod
       LEFT JOIN logcat_arm_loc g ON a.id_loc=g.id_loc
       LEFT JOIN logcat_arm_motivo_movimiento i ON a.id_motivo_movimiento=i.id_motivo_movimiento
       LEFT JOIN logcat_arm_propietario j ON a.id_propietario=j.id_propietario
       LEFT JOIN logcat_arm_situacion k ON a.id_situacion=k.id_situacion
       LEFT JOIN logcat_arm_marca h ON a.id_marca=h.id_marca
       LEFT JOIN logcat_arm_modelo mo ON h.id_modelo=mo.id_modelo
       LEFT JOIN logcat_arm_calibre cal ON mo.id_calibre=cal.id_calibre
       LEFT JOIN logcat_arm_clase cla ON h.id_clase=cla.id_clase
       LEFT JOIN logcat_arm_tipo tip ON cla.id_tipo=tip.id_tipo
     */
    public function selectLstAsignadas($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select matricula, tipo, id_marca, marca, modelo, clase, calibre from viewarmlstasignadas ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'matricula' => $data['matricula'],
                               'tipo' => $data['tipo'],
                               'marca' => $data['marca'],
                               'id_marca' => $data['id_marca'],
                               'modelo' => $data['modelo'],
                               'clase' => $data['clase'],
                               'calibre' => $data['calibre'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_asignacion(matricula, curp)
                VALUES(:matricula, :curp);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":curp" => $this->curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_asignacion
                   SET 
                WHERE matricula=:matricula AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":curp" => $this->curp));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para eliminar un egistro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function delete()
    {

        $sql = "DELETE FROM logtbl_arm_asignacion
                WHERE matricula=:matricula 
                AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":curp" => $this->curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }

    }
}


?>