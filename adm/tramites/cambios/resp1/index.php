<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_personales.class.php';

$objDatPer = new AdmtblDatosPersonales();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Tr�mites de Personal - Cambios de Adscripci�n',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="adm/_js/tramites/cambios/index.js"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->
                    <!--<a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>-->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <!-- GRID PRINCIPAL DATOS DEL ELEMENTO -->
    <div id="dvGrid-Personal" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                    <th style="width: 17%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">ADSCRIPCI�N</th>
                    <th style="width: 15%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE TRAMITES ------------- -->
    <div id="dvTramites" style="display: none;"  >    
        
        <!-- FICHA DEL ELEMENTO CON FOTOGRAFIA MINIATURA -->
        <div id="dvDatos" style="margin: auto auto; margin-top: 10px; min-height: 100px; width: auto;"></div>
            
        <!-- div para el boton de asignaciones-->
        <!--<div id="dvAgregar" style="margin: auto auto; margin-top: 10px; min-height: 15px; width: auto; position: relative;">
            <table style="width: 100%; border: 0px;">
                <tr>
                <td style="width: 95%;">&nbsp;</td>
                <td style="text-align: center; width:  5%;">  
                    <a href="#" class="lnkBtnOpcionGrid classBusca" title="Asignar armamento..."> 
                    <img src="<?php echo PATH_IMAGES; ?>icons/add.png" alt="Asignar armamento." />Asignar</a>
                </td>
                </tr>
            </table>
        </div> -->
        
        <!--
        //CAMBIOS DE ADSCRIPCION DEL ELEMENTO EN CUESTION.                                                                        
            $html.='<div id="dvGrid-Port" style="border: none; height: 150px; margin: auto auto; margin-top: 10px; width: auto;">';
                $html.='<div class="xGrid-dvHeader gradient">';
                    $html.='<table class="xGrid-tbCols">';
                        $html.='<tr>';
                            $html.='<th style="width: 5%; text-align: center;">No.</th>';
                            $html.='<th style="width: 10%;">FECHA</th>';
                            $html.='<th style="width: 6%;">TIPO</th>';  
                            $html.='<th style="width: 15%;">ADSCRIPCION</th>';
                            $html.='<th style="width: 20%;">NO OFICIO</th>';
                            $html.='<th style="width: 19%;">FECHA NOTIFICACION</th>';                                                        
                            $html.='<th style="width: 5%; text-align: center;">&nbsp;</th>';                            
                        $html.='</tr>';
                    $html.='</table>';
                $html.='</div>';
                $html.='<div class="xGrid-dvBody" id="divAsig" style="overflow-y: hidden;">';                                       
                    $html.= '<table class="xGrid-tbBody" >'; 
                    
                    $html .= '</table>';
                $html.='</div>';        
            $html.='</div>';--> 
            
        <!-- LISTA DE TRAMITES DEL ELEMENTO EN CUESTION -->
        <div id="dvGrid-Tramites" style="border: none; height: 240px; margin: auto auto; margin-top: 10px; width: auto;">
        
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbCols">
                    <tr>
                        <th style="width:  4%; text-align: center;" >No.</th>
                        <th style="width: 12%;" >FECHA</th>
                        <th style="width:  7%;" >TIPO</th>  
                        <th style="width: 15%;" >FOLIO</th>
                        <th style="width: 27%;" >ADSCRIPCION</th>
                        <th style="width: 15%;" >NO OFICIO</th>                                                        
                        <th style="width: 10%;" >FECHA OFICIO</th>
                        <th style="width:  10%;" >&nbsp;</th>                                                         
                    </tr>
                </table>
                
            </div>
            
            <div class="xGrid-dvBody">
            <!-- aqui van datos de las armas -->
            </div>
        </div>                                 
            
        <!-- CONTENEDOR PARA EL AGREGAR NUEVO TRAMITES ------------- -->
        <div id="dvNuevo" style="display: none;" title="Nuevo cambio de Adscripcion" >      </div>
                     
        <!-- datos necesarios para pasarlos por formulario                                                    
        $html.='<input type="hidden" id="hdnUrlCurp"        value="' . $objSys->encrypt( $curp ) . '" /> -->
        <input type="hidden" id="hdnUrlCurp" />
        <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_obt_datos_grid.php');?>" />
        <input type="hidden" id="hdnUrlObtDatosPer" value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_obt_datos_per.php');?>" />
        <input type="hidden" id="hdnUrlObtListTram"  value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_obt_tram_asig.php'); ?>" />
        <input type="hidden" id="hdnUrlNuevo"  value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_frm_nuevo.php'); ?>" />
        <input type="hidden" id="hdnUrlAddRg"  value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_reg_frm.php'); ?>" />
        <input type="hidden" id="hdnUrlMpio"  value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php'); ?>" />
        
                         
    </div>          
    <!--    
     <div id="dvCambios" style="display: none;" title="Cambios de Adscripci�n">    
        <div id="dvCambiosHead" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 400px; min-width: 650px; overflow: hidden;"></div>        
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlAsigna" value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_frm_asigna.php');?>" />
    <input type="hidden" id="hdnUrlObtDatosPer" value="<?php echo $objSys->encrypt('adm/tramites/cambios/ajx_obt_datos_per.php');?>" />
    -->    
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>