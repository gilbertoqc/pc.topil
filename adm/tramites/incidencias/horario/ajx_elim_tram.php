<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../../';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_cambio_horario.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario(); 
    
    $objCambHor = new AdmtblCambioHorario();
    // Datos del tramite
    $objCambHor->id_cambio_horario = $_POST["id_tramite"];
    $objCambHor->curp      = $_POST["curp"];                 

    $result = $objCambHor->delete();
    if ( $result ) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_cambio_horario', $objCambHor->id_cambio_horario, "Dlt");
        $ajx_datos['rslt']  = true; 
            
    } else {
        $ajx_datos['rslt']  = false;                     
        $ajx_datos['error'] = $objCambHor->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>