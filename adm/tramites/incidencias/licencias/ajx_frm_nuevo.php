<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_licencias_medicas.class.php';
    
    $objSys = new System();
    $objLicencias = new AdmtblLicenciasMedicas();
    
    //se incluye archivo javascript para funcionalidad necesario
    $html .= '<script type="text/javascript" src="adm/_js/tramites/incidencias/licencias/ajx_frm_asigna.js"></script>';
    $html .= '<script type="text/javascript" src="includes/js/xgrid.js"></script>';
    $html .= '<link type="text/css" rel="stylesheet" href="includes/js/jquery_editor/jquery-te-1.4.0.css">';
    $html .= '<script type="text/javascript" src="includes/js/jquery_editor/jquery-te-1.4.0.min.js" charset="utf-8"></script>';
    
    
    
    //variables recibidas
    $curp = $_POST["curp"];
    $id_tramite = $_POST["id_tramite"];
    $oper = $_POST["oper"];
    
    // Arreglo de datos estatico
    //$tipoLic[0] = array('value'=>0,'descripcion'=>'');
    //$tipoLic[1] = array('value'=>1,'descripcion'=>'CON SUELDO');
    //$tipoLic[2] = array('value'=>2,'descripcion'=>'SIN SUELDO');

    
   
    if($oper == 0){   
        
        
        $fecha_inicio = "";
        $fecha_fin = "";
        $fecha_oficio = "";
        $fecha_soporte = "";
        $diagnostico = '';
        
        
        
    }
    else{
        
        $objLicencias->select($id_tramite);
        $curp = $objLicencias->curp;
        $fecha_inicio = ($objLicencias->fecha_inicio == NULL) ? "" : date('d/m/Y', strtotime($objLicencias->fecha_inicio));
        $fecha_fin = ($objLicencias->fecha_fin == NULL) ? "" : date('d/m/Y', strtotime($objLicencias->fecha_fin));
        $fecha_oficio = ($objLicencias->fecha_oficio == NULL) ? "" : date('d/m/Y', strtotime($objLicencias->fecha_oficio));
        $fecha_soporte = ($objLicencias->fecha_soporte == NULL) ? "" : date('d/m/Y', strtotime($objLicencias->fecha_soporte));
        $diagnostico = $objLicencias->diagnostico;
        
        if ($objLicencias->indefinida == 1) {
            $rbnDefinida1 = 'checked="true"';
            $rbnDefinida2 = '';
        } else {
            $rbnDefinida1 = '';
            $rbnDefinida2 = 'checked="true"';
        }
    }
        
     

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
    $html .= ' 
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <!--<span class="dvForm-Data-pTitle"><img src="'.PATH_IMAGES.'icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Nuevo</span>-->                                
            <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
                <ul>
                    <li><a href="#tab-1" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Datos Generales</a></li>
                    <li><a href="#tab-2" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Datos del Oficio</a></li>
                  
                </ul>         
                <!-- Datos Generales -->
                <div id="tab-1">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoFolio">No. Folio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoFolio" id="txtNoFolio" value="'.$objLicencias->folio.'" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="txtFechaInicio">Fecha de Inicio:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaInicio" id="txtFechaInicio" value="'.$fecha_inicio.'" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtFechaFin">Fecha de Finalizaci�n:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaFin" id="txtFechaFin" value="'.$fecha_fin.'" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    
                                </td>
                            </tr>     
                            <tr>
                                <td><label for="txtDias">Numero de Dias:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtDias" id="txtDias" value="'.$objLicencias->num_dias.'" maxlength="3" title="..." style="width: 100px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                             <tr>
                                <td><label for="txtNoFolioFin">No. Folio Finanzas:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoFolioFin" id="txtNoFolioFin" value="'.$objLicencias->folio_finanzas.'" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                </td>
                            </tr> 
                            <!--<tr>
                                <td><label></label></td>
                                <td class="validation">
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnDefinida" id="rbnDefinida1" value="1" '.$rbnDefinida1.' />Definida</label>
                                    <label class="label-Radio"><input type="radio" name="rbnDefinida" id="rbnDefinida2" value="2" '.$rbnDefinida2.' />Indefinida</label>
                                </td>
                                <span class="pRequerido">*</span>
                            </tr>-->
                            
                            <script>
                            $(".editor").jqte();
                            
                            
                            </script>
                            <tr>
                                <td><label for="txtDiagnostico">Diagnostico:</label></td>
                                <td class="validation" >
                                    <textarea class="editor" name="txtDiagnostico" id="txtDiagnostico"  >
                                    '.$diagnostico.'
                                    </textarea>
                                    
                                </td>
                            </tr> 
                        </table>
                    </fieldset>
                </div>
                <script> 
                    $("#tabs").tabs (); 
                    $("#txtFechaInicio").datepicker({ yearRange: "1920:", }); 
                    $("#txtFechaFin").datepicker({ yearRange: "1920:", });
                    $("#txtFechaOficio").datepicker({ yearRange: "1920:", });
                    $("#txtFechaSoporte").datepicker({ yearRange: "1920:", });
                </script>
                <div id="tab-2">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoOficio">No. Oficio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoOficio" id="txtNoOficio" value="'.$objLicencias->no_oficio.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtFechaOficio">Fecha Oficio:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaOficio" id="txtFechaOficio" value="'. $fecha_oficio . '"  maxlength="35" title="..." style="width: 120px;" />
                                </td>
                            </tr>
                             <tr>
                                <td><label for="txtNoSoporte">No. de Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoSoporte" id="txtNoSoporte" value="'.$objLicencias->no_soporte.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtFechaSoporte">Fecha de Soporte:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaSoporte" id="txtFechaSoporte" value="'. $fecha_soporte . '"  maxlength="35" title="..." style="width: 120px;" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFirmante">Firmante del Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtFirmante" id="txtFirmante" value="'.$objLicencias->firmante_soporte.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                   
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtCargoFirmante">Cargo del Firmante:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtCargoFirmante" id="txtCargoFirmante" value="'.$objLicencias->cargo_firmante.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                    
                                </td>
                            </tr>     
                            
                        </table>
                    </fieldset>
                </div>
                
            </div>
            
        </div>
         <input type="hidden" id="dtTypeOper" name="dtTypeOper" value="' . $oper . '" />
        <input type="hidden" id="id_tramite" name="id_tramite" value="' . $id_tramite . '" />
        <input type="hidden" id="curp" name="curp" value="' . $objSys->encrypt( $curp ) . '" />
        <input type="hidden" name="safety_precautions" id="safety_precautions">
    </form>     
   
    ';    
                  
   
  
    
    //se inyecta la informacion en los contenedores
    $ajx_datos["html"]  = utf8_encode($html);      
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>