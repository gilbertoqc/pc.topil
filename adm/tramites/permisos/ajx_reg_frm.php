<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
 
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_permisos.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objPermisos  = new AdmtblPermisos();
    //Recibe la variables por POST 
    //Convierte a codificacion iso-8859
    //asigna el nombre de variable a la variable recibida
    foreach($_POST as $nombre_campo => $valor){ 
        $asignacion = "\$" . $nombre_campo . "='" . utf8_decode($valor) . "';"; 
        eval($asignacion); 
    }
    
    
    $curp = $objSys->decrypt( $curp ); 
    
    // Datos de los Cambios
    $objPermisos->curp = $curp;
    $objPermisos->id_permisos = $id_tramite;
    $objPermisos->fecha_ini = ($txtFechaInicio == "") ? NULL : $objSys->convertirFecha($txtFechaInicio , 'yyyy-mm-dd');
    $objPermisos->fecha_fin = ($txtFechaFin == "") ? NULL : $objSys->convertirFecha($txtFechaFin, 'yyyy-mm-dd');
    $objPermisos->folio = $txtNoFolio;
    $objPermisos->num_dias = $_POST["txtDias"];
    $objPermisos->tipo_licencia = $_POST["cbxTipoLicencia"];
    $objPermisos->motivo = $txtMotivo;
    $objPermisos->indefinida = $_POST["rbnDefinida"];
    // Datos del Oficio
    $objPermisos->no_oficio = $txtNoOficio;
    $objPermisos->fecha_oficio = ($_POST["txtFechaOficio"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaOficio"], 'yyyy-mm-dd');
    $objPermisos->no_soporte =$txtNoSoporte;
    $objPermisos->fecha_soporte = ($_POST["txtFechaSoporte"] == "") ? NULL : $objSys->convertirFecha($_POST["txtFechaSoporte"], 'yyyy-mm-dd');
    $objPermisos->firmante_soporte = $txtFirmante;
    $objPermisos->cargo_firmante = $txtCargoFirmante;
    
   
    $result = ($_POST["dtTypeOper"] == 1) ? $objPermisos->update() : $objPermisos->insert();
    $oper = $_POST["dtTypeOper"] == '1' ? 'Edt' : 'Ins';
    
   if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_permisos', $curp, $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($html);            
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
       // $ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objPermisos->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}

?>