<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';  
    include $path . 'includes/class/admtbl_doctos_ident.class.php'; 
     
    
    $objSys = new System();        
    $objDat= new AdmtblAdscripcion();  
    $objDocIdent = new AdmtblDoctosIdent();    
    // se reciben datos    
    $curp = $_POST["curp"];     

    //--------------------- Recepci�n de par�metros --------------------------//
    $datos = $objDat->selectAll("a.curp='" . $curp . "'" );
    $datPer = $objDat->AdmtblDatosPersonales;
    $datPer->select( $curp );
    
    // Foto
    $objDocIdent->select($curp);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? 'adm/expediente/fotos/' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDatPer->genero == 1 ) ? 'adm/expediente/sin_foto_h.jpg' : 'adm/expediente/sin_foto_m.jpg';
                
    //se elabora el encabezado de la ventana
    $titulo = 'VACACIONES [ ' . $datPer->nombre . " " . $datPer->a_paterno . " " . $datPer->a_materno . ' ] ';
                
    $html = '';    
        $html .='<table id="tbForm-Ficha" class="tbForm-Data" style="width: 750px;" border=0>';                                              
                $html .='<colgroup>';
                    $html.='<col style="width : 90px;">';
                    $html.='<col style="width : 110px;">';
                    $html.='<col style="width : 650px;">';
                $html .='</colgroup>';     
                $html .='<tr>';
                    //$html .='<td rowspan="4"> <img src="imgs/sin_foto_h.jpg" title="imgs/sin_foto_h.jpg" /> </td>';
                    $html .='<td rowspan="4">';
                    $html .='<div class="dvFoto-Mini" style="vertical-align: bottom;" >
                                    <img src="'.$foto_fte.'" alt="foto" style="position: relative;" />
                             </div>';      
                    $html .='</td>';                          
                $html .='</tr>';
                $html .='<tr>';
                    $html .='<td>Categoria: </td>';
                    $html .='<td>' . $datos[0]['admcat_categorias_categoria'] . ' </td>';
                $html .='</tr>';
                $html .='<tr>';
                    $html .='<td>Corporaci&oacute;n: </td>';
                    $html .='<td>' . $datos[0]["admcat_corporaciones_corporacion"] . ' </td>';
                $html .='</tr>';  
                $html .='<tr>';                        
                    $html .='<td>Municipio: </td>';
                    $html .='<td>' . $datos[0]["admcat_municipios_municipio"] . ' </td>';
                $html .='<tr>';                                                     		         
         $html .= '</table>';
         $html.='<div id="dvAgregar" style="margin-top: 0px;">';
                $html.='<table style="width: 100%; border: 10px;">';
                    $html.='<tr>';
                    $html.='<td style="width: 95%;">&nbsp;</td>';
                    $html.='<td style="text-align: center; width:  10%;">';  
                    $html.='<a href="#" accion="insertar" id="curp' . $curp . '" class="Tool-Bar-Btn classNuevo"'; 
                    $html.='title="Agregar vacaciones..." ><img src="' . PATH_IMAGES . 'icons/add.png" alt="Agregar Vacaciones." style="border: none;" /><br />Nuevo</a>';
                    $html.='</td>';
                    $html.='</tr>';
                $html.='</table>';
          $html.='</div>';   	
    
    //   
    $ajx_datos["curp"] = $curp;
    $ajx_datos["total"] = $totalReg;  
    $ajx_datos["titulo"] = utf8_encode($titulo);
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>