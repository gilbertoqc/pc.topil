<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_doctos_ident.class.php';
include 'includes/class/admtbl_archivo_exped.class.php';
$objDocIdent = new AdmtblDoctosIdent();
$objExped = new AdmtblArchivoExped();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/file_uploader/AjaxUpload.2.0.min.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/archivo/_js/ctrlexped.js?v=1.0"></script>',
                                   '<link type="text/css" href="adm/rhumanos/archivo/_css/sty_exped.css?v=1.0" rel="stylesheet" />' ),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <!-- Botones de opci�n... -->                    
                    <a href="index.php" id="btnRegresar" class="Tool-Bar-Btn" style="" title="Regresar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $_SESSION['xCurp'] = ( isset($_GET['id']) ) ? $objSys->decrypt($_GET['id']) : $_SESSION['xCurp'];
    // Se obtienen los datos personales de la persona
    $objDocIdent->AdmtblDatosPersonales->select($_SESSION['xCurp']);
    $path_exped = "adm/expediente/doctos/";
    // Se selecciona el expediente de la persona
    $objExped->AdmtblArchivo->select($_SESSION['xCurp']);  
    $num_expediente = ( !empty($objExped->AdmtblArchivo->expediente) ) ? $objExped->AdmtblArchivo->expediente :  "SSP-_ _ _ _ _";  
    // Se obtiene la fotograf�a de la persona
    $objDocIdent->select($_SESSION['xCurp']);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? 'adm/expediente/fotos/' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDocIdent->AdmtblDatosPersonales->genero == 1 ) ? 'adm/expediente/sin_foto_h.jpg' : 'adm/expediente/sin_foto_m.jpg';
    ?>    
    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 5px; text-align: left; width: 1000px;">
        <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Expediente Digital</span>
        <table class="tbForm-Data" style="margin: 0px; width: 100%">
            <tr>
                <td id="tdFoto" rowspan="10" style="text-align: center; vertical-align: top;">
                    <fieldset class="fsetForm-Data">
                        <div class="dvFoto-Grande" style="background-image: url(<?php echo $foto_fte;?>); vertical-align: bottom;"></div>
                        <p style="color: #696565; font-size: 11pt; font-weight: bold;">
                            <?php echo $objDocIdent->AdmtblDatosPersonales->a_paterno .' ' . $objDocIdent->AdmtblDatosPersonales->a_materno . ' ' . $objDocIdent->AdmtblDatosPersonales->nombre;?>                        
                            <span style="display: block; font-size: 10pt; margin-top: 5px;">[<?php echo $objDocIdent->AdmtblDatosPersonales->curp;?>]</span>                        
                            <span style="display: block; font-size: 9pt; margin-top: 5px;"><?php echo $objDocIdent->AdmtblDatosPersonales->AdmcatStatus->status;?></span>
                        </p>
                    </fieldset>
                    <fieldset class="fsetForm-Data">
                        <label for="txtNumExpediente" style="font-size: 11pt;">Expediente: </label>
                        <input type="text" id="txtNumExpediente" value="<?php echo $objExped->AdmtblArchivo->expediente;?>" maxlength="10" style="font-size: 12pt; text-align: center; width: 120px;" />
                        <a href="#" id="lnkBtnSaveExped" class="lnkBtnOpcionGrid" style="vertical-align: middle;">
                            <img src="<?php echo PATH_IMAGES?>icons/save24.png" alt="guardar" />
                        </a>
                        <input type="hidden" id="hdnUrlSaveExped" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_reg_exped.php');?>" />
                    </fieldset>
                    <?php
                    $sty_btn_add = ( !empty($objExped->AdmtblArchivo->expediente) ) ? 'display: block;' : 'display: none;';
                    ?>
                    <a href="#" id="lnkBtnAddDocto" class="lnkBtnAddDoc" title="Agregar nuevo documento..." style="<?php echo $sty_btn_add;?>">
                        <img src="<?php echo PATH_IMAGES?>icons/add_file32.png" alt="agregar" style="border: none; vertical-align: middle;" />Agregar nuevo documento
                    </a>
                </td>                
                <td style="text-align: left; width: 700px;">
                    <ul id="tabs">
                        <li><a href="#" id="lnkDescripExped"><?php echo $num_expediente;?></a></li>                        
                    </ul>
                    <div id="dvExpediente">                   
                    <?php
                    $doctos = $objExped->AdmcatArchivoDoctos->selectAll("status=1");
                    foreach( $doctos As $reg => $doc ){
                        if( $reg == 0 ){
                            $sty_btn_colaps = "lnkBtnColapsUp";
                            $display_doc = "block";
                        } else {
                            $sty_btn_colaps = "lnkBtnColapsDown";
                            $display_doc = "none";
                        }
                        $total_files = $objExped->selectCount($objExped->AdmtblArchivo->id_archivo, $doc["id_documento"]);
                        echo '<div class="dvDocumento">';
                            echo '<span class="spnTituloDoc">
                                    <span class="spnTitulo" id="spnTitulo-' . $doc["id_documento"] . '">' . $doc["documento"] . ' (' . $total_files . ')</span>
                                    <a href="#" id="lnkColaps-' . $doc["id_documento"] . '" rel="dv-' . $doc["id_documento"] . '" class="' . $sty_btn_colaps . '"></a>
                                  </span>';
                            echo '<div id="dv-' . $doc["id_documento"] . '" class="dvContent-Doc" style="display: ' . $display_doc . ';">';
                                echo '<div class="dvDoctos">';
                                if( $reg == 0 && $total_files > 0 ){ 
                                    $files_docto = $objExped->selectAll("a.id_archivo=" . $objExped->AdmtblArchivo->id_archivo . " AND a.id_documento=" . $doc["id_documento"]);
                                    foreach( $files_docto As $rfd => $fd ){
                                        $name_img = $fd["curp"] . '/' . $fd["curp"] . '_' . $fd["id_archivo"] . '_' . $fd["id_documento"] . '_' . $fd["id_registro"];
                                        $img_mini = $name_img . '-thb' . $fd["ext"];                                        
                                        echo '<div class="dvDocto">';
                                            $rel = $fd["id_archivo"] . "-" . $fd["id_documento"] . "-" . $fd["id_registro"] . '-' . $fd["ext"];
                                            echo '<a href="#" class="lnkImgDoc" rel="' . $rel . '" title="' . $fd["descripcion"] . '">';
                                                echo '<img src="' . $path_exped . $img_mini . '" alt="' . $fd["descripcion"] . '" />';                                        
                                            echo '</a>';
                                            echo '<a href="#" class="lnkDocDlt" rel="' . $rel . '" title="Eliminar este documento...">Eliminar</a>';
                                        echo '</div>';
                                    }
                                }
                                echo '</div>';
                                $rel = $doc["id_documento"] . "|" . $doc["documento"];
                                //echo '<a href="#" class="lnkBtnAddDoc" rel="' . $rel . '" title="Agregar nuevo documento..."></a>';                                
                            echo '</div>';
                        echo '</div>';                    
                    }
                    ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvForm-Add" title="SISP :: Agregar imagen de documento">
        <form id="frmAddFile" action="#" autocomplete="off" method="post" enctype="multipart/form-data">
        <fieldset class="fsetForm-Data">
             <table class="tbForm-Data" style="width: auto">
                <tr>
                    <td rowspan="4" colspan="2" style="border-right: 1px solid #9e9c9b;">
                        <div id="dvPrevImg">
                        
                        </div>
                        <input type="hidden" name="hdnFileImg" id="hdnFileImg" value="" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;"><label for="cbxDocumento">Documento:</label></td>
                    <td class="validation">
                        <select name="cbxDocumento" id="cbxDocumento" style="max-width: 350px;">
                            <option value="0"></option>
                            <?php
                            echo $objExped->AdmcatArchivoDoctos->shwArchivoDoctos();
                            ?>
                        </select>
                        <span class="pRequerido">*</span>
                    </td>
                </tr>
                <tr>
                    <td><label for="txtFechaDocto">Fecha del Documento:</label></td>
                    <td class="validation">
                        <input type="text" name="txtFechaDocto" id="txtFechaDocto" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                        <!-- <span class="pRequerido">*</span> -->
                    </td>
                </tr>
                <tr>
                    <td><label for="txtDescripcion">Descripci�n:</label></td>
                    <td class="validation">
                        <input type="text" name="txtDescripcion" id="txtDescripcion" value="" maxlength="90" title="..." style="width: 350px;" />
                        <span class="pRequerido">*</span>
                    </td>
                </tr>
             </table>
         </fieldset>
         <p style="font-size: 8pt; margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
         </form>
    </div>    
    <input type="hidden" id="hdnUrlUpload" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_upload_file.php');?>" />
    <input type="hidden" id="hdnUrlDirFiles" value="<?php echo $objSys->encrypt('adm/_uploadfiles/');?>" />
    <input type="hidden" id="hdnUrlSaveDoc" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_reg_docto.php');?>" />
    <input type="hidden" id="hdnUrlLoadDocs" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_load_doctos.php');?>" />
    <input type="hidden" id="hdnUrlDltFile" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_dlt_docto.php');?>" />
    <input type="hidden" id="hdnUrlDltFileTemp" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_dlt_file_temp.php');?>" />
    
    <div id="dvForm-Prev" title="" style="text-align: center;">
        <div id="dvShowImg"></div>
    </div>
    <input type="hidden" id="hdnUrlShwImg" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_preview_docto.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>