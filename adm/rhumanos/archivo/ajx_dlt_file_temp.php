<?php
/**
 * Complemento del llamado ajax para eliminar los archivos en una carpeta temporal del servidor.
 * Lista de parámetros recibidos por POST 
 * @param String file, contiene el nombre del archivo a eliminar en el servidor.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $uploaddir = '../../_uploadfiles/';	        	
    $pos_ext = strrpos($_POST["file"], '.');
    $ext = substr($_POST["file"], $pos_ext);
    
    $archivo_normal = $_POST["file"];
    $archivo_mini = substr($_POST["file"], 0, $pos_ext) . '-thb' . $ext;
    
    unlink($uploaddir . $archivo_normal);
    unlink($uploaddir . $archivo_mini);
}
?>