<?php
/**
 * Complemento ajax para guardar una imagen de en documento correspondiente al expediente de una persona. 
 * Estructura del nombre del archivo: CURP_ID_ARCHIVO_ID_DOCUMENTO_ID_REGISTRO 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_archivo_exped.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objExped = new AdmtblArchivoExped();
    
    $curp = $_SESSION['xCurp'];
    $objExped->AdmtblArchivo->select($curp);
    // Se asignan los valores a los campos
    $objExped->id_registro  = 0;
    $objExped->id_archivo   = $objExped->AdmtblArchivo->id_archivo;
    $objExped->id_documento = $_POST["cbxDocumento"];
    $objExped->fecha_docto  = ( !empty($_POST["txtFechaDocto"]) ) ? $objSys->convertirFecha($_POST["txtFechaDocto"], "yyyy-mm-dd") : null;
    $objExped->fecha_reg    = date('Y-m-d');
    $objExped->descripcion  = ( !empty($_POST["txtDescripcion"]) ) ? utf8_decode($_POST["txtDescripcion"]) : null;
    
    $path_upload = '../../_uploadfiles/';
    $path_exped = '../../expediente/doctos/';
    //Si la carpeta de la persona no existe, entonces la crea...
    $band_dir = true;
    if( !is_dir($path_exped.$curp) ){		 
		if( !mkdir($path_exped.$curp, 0777) )
			$band_dir = false;
	}
    if( $band_dir ){
        // Se obtiene la extensi�n del archivo
        $pos_ext = strrpos($_POST["hdnFileImg"], '.');
        $ext = substr($_POST["hdnFileImg"], $pos_ext);
        $objExped->ext = $ext;
        // Se genera la ruta completa del expediente personal
        $path_exped .= $curp . '/';
        if( $result = $objExped->insert() ) {
            $nvo_id_reg = $result;
            // Se genera el nombre con que se guardar� la imagen del documento
            $name_file = $curp . '_' . $objExped->id_archivo . '_' . $objExped->id_documento . '_' . $nvo_id_reg;
            // Se obtiene el nombre del archivo thb                        
            $file_thb = substr($_POST["hdnFileImg"], 0, $pos_ext) . '-thb' . $ext;
            // Se pasan los archivos de im�genes a la carpeta correspondiente
            copy($path_upload . $_POST["hdnFileImg"], $path_exped . $name_file . $ext);
            copy($path_upload . $file_thb, $path_exped . $name_file . '-thb' . $ext);
            // Se eliminan los archivos temporales
            unlink($path_upload . $_POST["hdnFileImg"]);
            unlink($path_upload . $file_thb);
            // Se registra el log
            $objSys->registroLog($objUsr->idUsr, 'admtbl_archivo_exped', $curp . '-' . $nvo_id_reg, "Ins");
            // Se asignan los valores que seran devueltos
            $ajx_datos['file0']  = $path_upload . $_POST["hdnFileImg"];
            $ajx_datos['file1']  = $path_exped . $name_file . $ext;
            $ajx_datos['file2']  = $path_exped . $name_file . '-thb' . $ext;
            $ajx_datos['rslt']  = true;
            $ajx_datos['error'] = '';
        } else {
            $ajx_datos['rslt']  = false;
            $ajx_datos['error'] = $objExped->msjError;
        }
    } else {
        $ajx_datos["rslt"] = false;
        $ajx_datos["error"] = "No se pudo crear el expediente de la persona";
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>