var xGrid = "",
    xFormBaja = "",
    xFormOfi = "",
    hWind = 0;
$(document).ready(function(){
    hWind = $(window).height();
    wWind = $(window).width();
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Bajas').css('height', (hWind - 250) + 'px');
    $('#dvGrid-Bajas').css('width', (wWind) + 'px');
    
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Bajas').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaBaja').datepicker({
        yearRange: '1990:',
    });
    $('#txtFechaOficio').datepicker({
        yearRange: '1990:',
    });
    
    //-- Funcionalidad de los botones de la barra de herramientas...
    $('#xRegistrar').click(function(){
        $('#hdnIdBaja').val('0');
        $('#hdnTypeOper').val('1');
        $('#dvForm-Baja').dialog('open');
    });
    $('#xReportes').click(function(){
        //$('#dvForm-Registro').dialog('open'); 
    });
          
    //-- Funcionalidad de los botones de opci�n del grid principal...
    $('#dvGrid-Bajas a.lnkBtnOpcionGrid').live('click', function(){
        var prm = $(this).attr("rel").split(';');
        if( prm[0] == 'edt' ){
            $('#hdnIdBaja').val(prm[1]);
            $('#hdnTypeOper').val('2');            
            xFormBaja.dialog('open');            
        } else if( prm[0] == 'pdf' ){
            /*
            xFormOfi.dialog({
                open: function(){
                    var hgtBody = ((hWind - 150) / 2 ) - 50;
                    $(this).html('<div class="xGrid-dvWait-LoadData" style="padding-top: ' + hgtBody + 'px;">Cargando datos, por favor espere...</div>');
                    getOficio(prm[1]);
                },
                buttons:{
                    '-- Cerrar --': function(evt){
                        $(this).dialog('close');
                    }
                }
            });
            xFormOfi.dialog('open');
            */
        } else if( prm[0] == 'wrd' ){
            /*
            xFormOfi.dialog({
                open: function(){
                    var hgtBody = ((hWind - 150) / 2 ) - 50;
                    $(this).html('<div class="xGrid-dvWait-LoadData" style="padding-top: ' + hgtBody + 'px;">Cargando datos, por favor espere...</div>');
                    getOficio(prm[1]);
                },
                buttons:{
                    '-- Cerrar --': function(evt){
                        $(this).dialog('close');
                    }
                }
            });
            xFormOfi.dialog('open');
            */
        }   
    });
    
    /*--------------------------------------------------------------------//
     * Funcionalidad para el registro o modificaci�n de una baja
     *--------------------------------------------------------------------*/
    //-- Definici�n del dialog como formulario para el registro o edici�n de la baja...
    xFormBaja = $('#dvForm-Baja').dialog({
        autoOpen: false,
        width: 800,
        minHeight: 450,
        modal: true,
        resizable: false,
        draggable: true,
        open: function(){
            resetFormulario();
            if ($('#hdnTypeOper').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nueva baja');
            } else if ($('#hdnTypeOper').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar baja');
                getDatosBaja();
            }
        },
        buttons: {
            'Guardar los datos': function(){
                registrarBaja();
            },
            'Cancelar el proceso': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Validaci�n de los campos...
    $('#frmBaja').validate({
        rules:{
            txtCurp: 'required',
            cbxMotivo: {
                required: true,
                min: 1,
            },
            txtFechaBaja: 'required',
            txtNumOficio: 'required',
            txtFechaOficio: 'required',                        
    	},
    	messages:{    	   
            // Domicilio
            txtCurp: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxMotivo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtFechaBaja: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNumOficio: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaOficio: '<span class="ValidateError" title="Este campo es obligatorio"></span>',            
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    
    /*--------------------------------------------------------------------//
     * Controla la b�squeda del personal activo
     *--------------------------------------------------------------------*/
    $('#btnSelecEmp').click(function(){
        $('#dvForm-BusqEmp').dialog('open');
    });
    // Definici�n del control grid para la b�squeda de personal activo
    xGridEmp = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xTypeSearch: 2,
        xOnlySearch: true,
        xUrlData: xDcrypt($('#hdnUrlDatEmpGrid').val()),
        xLoadDataStart: 0,
        xTypeDataAjax: 'json'
    });
    // Definicion de la ventana de selecci�n de personal activo
    $('#dvForm-BusqEmp').dialog({
        autoOpen: false,
        closeOnEscape: false,
        draggable: true,
        height: 480,
        modal: true,
        width: 1000,
        open: function(){
            
        },
        buttons:{
            '-- Cancelar --': function(){
                $(this).dialog('close');
            }
        }
    });
    // Seleccion de un registro
    $('#dvGrid-Personal a.lnkBtnOpcionGrid').live('click', function(){
        // Se obtienen los datos del elemento seleccionado
        var prm = $(this).attr("rel").split('|');
        $('#txtCurp').val( prm[0] );
        $('#txtNombre').val( prm[1] );
        $('#txtCategoria').val( prm[2] );
        $('#txtArea').val( prm[3] );
         $('#dvForm-BusqEmp').dialog('close');
    });
    
    /*--------------------------------------------------------------------//
     * Funcionalidad para el registro o modificaci�n de una baja
     *--------------------------------------------------------------------*/
    //-- Definici�n del dialog como formulario para la vista previa de los oficios...
    xFormOfi = $('#dvForm-Oficio').dialog({
        autoOpen: false,
        width: 1000,
        height: (hWind - 150),
        modal: true,
        resizable: false,
    });
});


function registrarBaja(){
    var valida = $('#frmBaja').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de continuar con el proceso actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Registrando la baja, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSave').val()),
                        data: $('#frmBaja').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            });
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) principal
                                xGrid.refreshGrid();
                                // Se cierra el formulario principal
                                $('#dvForm-Baja').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function getDatosBaja(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGet').val()),
        data: {'id': $('#hdnIdBaja').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {
                $('#txtCurp').val(xdata.curp);
                $('#txtNombre').val(xdata.nombre);
                $('#txtCategoria').val(xdata.categoria);
                $('#txtArea').val(xdata.area);    
                $('#cbxMotivo').val(xdata.id_motivo);
                $('#txtFechaBaja').val(xdata.fecha_baja);
                $('#txtNumOficio').val(xdata.num_oficio);
                $('#txtFechaOficio').val(xdata.fecha_oficio);
                $('#txtObservaciones').val(xdata.observaciones);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

/*
* Funci�n para obtener los datos del cliente.
*/
function getOficio(xId){
    $('#dvForm-Oficio').load(
        xDcrypt($('#hdnUrlFicha').val()), 
        {'id': xId}, 
        function(){
            $('#objPdf').css('height', (hWind - 290) + 'px');        
        }
    );
}

function resetFormulario(){
    $('#txtCurp').val('');
    $('#txtNombre').val('');
    $('#txtCategoria').val('');
    $('#txtArea').val('');    
    $('#cbxMotivo').val(0);
    $('#txtFechaBaja').val('');
    $('#txtNumOficio').val('');
    $('#txtFechaOficio').val('');
    $('#txtObservaciones').val('');
    if ($('#hdnTypeOper').val() == 1) {
        $('#btnSelecEmp').css('display', 'inline-block');
    } else if ($('#hdnTypeOper').val() == 2) {
        $('#btnSelecEmp').css('display', 'none');
    }
    
    $(".validation span:not([class='pRequerido'])").hide();
    $(".validation").removeClass("error");
    $(".validation").removeClass("success");
}