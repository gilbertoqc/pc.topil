<?php
/**
 * Complemento ajax para obtener los datos de una prestaci�n. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/admtbl_prestaciones.class.php';
    $objPrest = new AdmtblPrestaciones();
    
    $objPrest->select($_GET["id"]);
    if ($objPrest->id_prest_personal > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_prest_personal"] = $objPrest->id_prest_personal;
        $ajx_datos["id_status_prest"] = $objPrest->id_status_prest;        
        $ajx_datos["id_tipo_prest"] = $objPrest->id_prestacion;
        $ajx_datos["tipo_prest"] = $objPrest->AdmcatPrestaciones->prestacion;
        $ajx_datos["fecha_alta"] = ( !empty($objPrest->fecha_alta) ) ? date("d/m/Y", strtotime($objPrest->fecha_alta)) : '';
        $ajx_datos["observaciones"] = utf8_encode($objPrest->observaciones);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objPrest->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>