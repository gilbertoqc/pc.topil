$(document).ready(function(){ 
    $('#tabs').tabs();
       
    /**---------------------------------------------------------------------------------------
     *-- Implementaci�n del m�dulo: Prestaciones
     *----------------------------------------------------------------------------------------*/   
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaAlta').datepicker({
        yearRange: '1980:',
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    jQuery.validator.addMethod("valPrestacion", function(value, element) {
        if( $('#hdnTypeOperPrest').val() == 1 && value > 0 ){
            return true;
        } else if( $('#hdnTypeOperPrest').val() == 2 ) {
            return true;
        }
    });
    jQuery.validator.addMethod("valFechaAlta", function(value, element) {
        if( $('#cbxStatusPrest').val() == 1 && value != '' ){
            return true;
        } else if( $('#cbxStatusPrest').val() == 0 ) {
            return true;
        }
    });
    $('#frmPrest').validate({
        rules:{
            cbxPrestaciones:{
                valPrestacion: true,
            },            
            cbxStatusPrest:{
                required: true,
                min: 0,
            },           
            txtFechaAlta:{
                valFechaAlta: true,
            },
    	},
    	messages:{    
            cbxPrestaciones:{
                valPrestacion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxStatusPrest:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtFechaAlta:{
                valFechaAlta: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormPrest').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(1);
            if ($('#hdnTypeOperPrest').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Agregar prestaci�n');
            } else if ($('#hdnTypeOperPrest').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar prestaci�n');
                datosPrestacion();
            }                
        },
        buttons: {
            'Guardar datos': function(){
                guardarPrestacion();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Boton para agregar una nueva prestaci�n...
    $('#btnAddPrest').click(function(){
        $('#hdnIdPrestPer').val('0');
        $('#hdnTypeOperPrest').val('1');
        $('#dvFormPrest').dialog('open');
    });
    $('#dvGrid-Prest a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        $('#hdnIdPrestPer').val(xId);
        $('#hdnTypeOperPrest').val('2');
        $('#dvFormPrest').dialog('open');
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Prest table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Prest table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
    
    
    /*----------------------------------------------------------------------------------------
     *-- Implementaci�n del m�dulo: Dependientes econ�micos
     *----------------------------------------------------------------------------------------*/
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAPaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAMaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
        
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaNac').datepicker({
        yearRange: '1930:',
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmDependEco').validate({
        rules:{           
            txtNombre: 'required',
            txtAPaterno: 'required',
            txtAMaterno: 'required',
            txtFechaNac: 'required',
            cbxParentesco:{
                required: true,
                min: 1,
            }, 
    	},
    	messages:{
            txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAPaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAMaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaNac: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxParentesco:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    //-- Ventana para captura de datos...
    $('#dvFormDependEco').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 700,
        open: function(){
            resetForm(2);
            if ($('#hdnTypeOperDependEco').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nuevo Dependiente econ�mico');
            } else if ($('#hdnTypeOperDependEco').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar Dependiente econ�mico');
                datosDependEco();
            }                
        },
        buttons: {
            'Guardar datos': function(){
                guardarDependEco();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
    //-- Boton para agregar un nuevo curso...
    $('#btnAddDependEco').click(function(){
        $('#hdnIdDependEco').val('0');
        $('#hdnTypeOperDependEco').val('1');
        $('#dvFormDependEco').dialog('open');
    });
    $('#dvGrid-DependEco a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel");
        $('#hdnIdDependEco').val(xId);
        $('#hdnTypeOperDependEco').val('2');
        $('#dvFormDependEco').dialog('open');
    });
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-DependEco table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-DependEco table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
});

function guardarPrestacion(){
    var valida = $('#frmPrest').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos de la prestaci�n actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando la prestaci�n, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlProcesPrest').val()),
                        data: $('#frmPrest').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Prest div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Prest table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Prest table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormPrest').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosPrestacion(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetPrest').val()),
        data: {'id': $('#hdnIdPrestPer').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {
                $('#cbxPrestaciones').hide();
                $('#txtPrestacion').show();
                $('#txtPrestacion').val(xdata.tipo_prest);
                $('#hdnIdPrestacion').val(xdata.id_tipo_prest);
                $('#cbxStatusPrest').val(xdata.id_status_prest);
                $('#txtFechaAlta').val(xdata.fecha_alta);
                $('#txtObservaPrest').val(xdata.observaciones);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function guardarDependEco(){
    var valida = $('#frmDependEco').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos del dependiente econ�mico actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 400,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando datos, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlProcesDependEco').val()),
                        data: $('#frmDependEco').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: true,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-DependEco div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-DependEco table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-DependEco table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormDependEco').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosDependEco(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGetDependEco').val()),
        data: {'id': $('#hdnIdDependEco').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {
                $('#txtNombre').val(xdata.nombre);
                $('#txtAPaterno').val(xdata.a_paterno);
                $('#txtAMaterno').val(xdata.a_materno);
                $('#txtFechaNac').val(xdata.fecha_nac);
                $('#cbxParentesco').val(xdata.id_parentesco);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function resetForm(form){
    if( form == 1 ){
        $('#cbxPrestaciones').show();
        $('#txtPrestacion').hide();
        $('#cbxPrestaciones').val(0);
        $('#cbxStatusPrest').val(-1);
        $('#txtFechaAlta').val('');
        $('#txtObservaPrest').val('');
        
        $("#dvFormPrest .validation span:not([class='pRequerido'])").hide();
        $("#dvFormPrest .validation").removeClass("error");
        $("#dvFormPrest .validation").removeClass("success");
    } else if( form == 2 ){
        $('#txtNombre').val('');
        $('#txtAPaterno').val('');        
        $('#txtAMaterno').val('');
        $('#txtFechaNac').val('');
        $('#cbxParentesco').val(0);
                
        $("#dvFormDependEco .validation span:not([class='pRequerido'])").hide();
        $("#dvFormDependEco .validation").removeClass("error");
        $("#dvFormDependEco .validation").removeClass("success");
    }
}