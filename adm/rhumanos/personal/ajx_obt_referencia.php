<?php
/**
 * Complemento ajax para obtener los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';    
    include $path . 'includes/class/admtbl_referencias.class.php';
    $objReferencia = new AdmtblReferencias();
    
    $objReferencia->select($_GET["id"]);
    if ($objReferencia->id_referencia > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_tipo_referencia"] = $objReferencia->id_tipo_referencia;
        $ajx_datos["id_tipo_relacion"] = $objReferencia->id_tipo_relacion;
        $ajx_datos["nombre"] = utf8_encode($objReferencia->nombre);
        $ajx_datos["a_paterno"] = utf8_encode($objReferencia->a_paterno);
        $ajx_datos["a_materno"] = utf8_encode($objReferencia->a_materno);
        $ajx_datos["sexo"] = $objReferencia->sexo;
        $ajx_datos["ocupacion"] = utf8_encode($objReferencia->ocupacion);
        $ajx_datos["calle"] = utf8_encode($objReferencia->calle_num);
        $ajx_datos["colonia"] = utf8_encode($objReferencia->colonia);
        $ajx_datos["ciudad"] = utf8_encode($objReferencia->ciudad);
        $ajx_datos["cod_postal"] = $objReferencia->cp;
        $ajx_datos["id_entidad"] = $objReferencia->id_entidad;
        $ajx_datos["id_municipio"] = $objReferencia->id_municipio;
        $ajx_datos["tel_fijo"] = $objReferencia->tel_fijo;
        $ajx_datos["tel_movil"] = $objReferencia->tel_movil;                
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objReferencia->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>