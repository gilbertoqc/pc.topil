<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_nomina.class.php';

$objDatPlaza = new AdmtblDatosNomina();
$objDatPlaza->select($_SESSION['xCurp']);

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/datnomina.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlBack = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('persona_menu');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('datos_nomina_rg');
    
    if( !empty($objDatPlaza->id_plaza) ){
        $tp_oper = 2;
    } else {
        $tp_oper = 1;
    }

?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los cambios realizados...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlBack?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la modificaci�n de datos...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvMsjProcesData" style="margin: auto auto; margin-top: 5px; width: 750px;"></div>
        
    <form id="frmRegistro" method="post" action="#" enctype="multipart/form-data">
        <div id="dvForm-Nomina" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Actualizaci�n : Datos de la N�mina</span>
            <fieldset class="fsetForm-Data" style="width: auto;">                                
                <table class="tbForm-Data">
                    <tr>
                        <td colspan="2" style="text-align: left;">
                            <fieldset class="fsetForm-Data" style="border: 1px solid #c1c1bf; width: 590px;">
                                <legend style="border: 1px solid #c1c1bf; font-size: 9pt;">Plaza</legend>        
                                <table class="tbForm-Data">
                                    <tr>
                                        <td><label for="txtFolioPlaza">Folio de la Plaza:</label></td>
                                        <td class="validation">
                                            <input type="text" name="txtFolioPlaza" id="txtFolioPlaza" value="<?php echo $objDatPlaza->AdmtblPlantillaPlazas->folio;?>" maxlength="6" style="width: 80px;" />
                                            <span class="pRequerido">*</span>                                    
                                            <input type="hidden" name="hdnIdPlaza" id="hdnIdPlaza" value="<?php echo $objDatPlaza->id_plaza;?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="txtCategPlaza">Categor�a:</label></td>
                                        <td class="validation">
                                            <input type="text" name="txtCategPlaza" id="txtCategPlaza" value="<?php echo $objDatPlaza->AdmtblPlantillaPlazas->AdmcatCategorias->categoria;?>" readonly="true" style="width: 350px;" />
                                            <span class="pRequerido">*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="txtAreaPlaza">�rea:</label></td>
                                        <td class="validation">
                                            <input type="text" name="txtAreaPlaza" id="txtAreaPlaza" value="<?php echo $objDatPlaza->AdmtblPlantillaPlazas->AdmcatAreas->area;?>" readonly="true" style="width: 350px;" />
                                            <span class="pRequerido">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxTipoPlaza">Tipo de Plaza:</label></td>
                        <td class="validation">
                            <select name="cbxTipoPlaza" id="cbxTipoPlaza">
                                <option value="0">&nbsp;</option>
                                <?php
                                $id_tp_plaza = ( !empty($objDatPlaza->id_tipo_plaza) ) ? $objDatPlaza->id_tipo_plaza : 0;
                                echo $objDatPlaza->AdmcatTipoPlaza->shwTiposPlaza($id_tp_plaza);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNumEmpleado">No. de Empleado:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumEmpleado" id="txtNumEmpleado" value="<?php echo $objDatPlaza->num_empleado;?>" maxlength="5" title="..." style="width: 80px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="chkNuevaCreacion">Nueva Creaci�n:</label></td>
                        <td class="validation">
                            <?php
                            $chk_nva_plaza = ( $objDatPlaza->nueva_creacion == 1 ) ? 'checked="true"' : '';
                            ?>
                            <input type="checkbox" name="chkNuevaCreacion" id="chkNuevaCreacion" value="1" <?php echo $chk_nva_plaza;?> />
                        </td>
                    </tr>                           
                    <tr>
                        <td><label for="txtSueldo">Sueldo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtSueldo" id="txtSueldo" value="<?php echo $objDatPlaza->sueldo;?>" maxlength="10" title="..." style="text-align: right; width: 100px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxBanco">Banco:</label></td>
                        <td class="validation">
                            <select name="cbxBanco" id="cbxBanco">
                                <option value="0">&nbsp;</option>
                                <?php
                                $id_banco = ( !empty($objDatPlaza->id_banco) ) ? $objDatPlaza->id_banco : 0;
                                echo $objDatPlaza->AdmcatBanco->shwBancos($id_banco);
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNumCuenta">N�mero de Cuenta:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumCuenta" id="txtNumCuenta" value="<?php echo $objDatPlaza->cuenta;?>" maxlength="25" title="..." style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtOficinaPag">Oficina Pagadora:</label></td>
                        <td class="validation">
                            <input type="text" name="txtOficinaPag" id="txtOficinaPag" value="<?php echo $objDatPlaza->oficina_pagadora;?>" maxlength="30" title="..." style="width: 250px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>           
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" id="dtTypeOper" value="<?php echo $tp_oper;?>" />
        <input type="hidden" id="hdnUrlPlaza" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_plaza.php');?>" />
        <input type="hidden" id="hdnUrlSave" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_reg_nomina.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>