<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_aspirantes.class.php';
$objAspi = new AdmtblAspirantes();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/personarg.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Reclutamiento');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $curp = ( $_POST["dtTypeOper"] == 1 ) ? $_POST["txtCurp"] : $_SESSION['xCurp'];
    // Datos Personales
    $objAspi->curp = $curp;
    $objAspi->nombre = $_POST["txtNombre"];
    $objAspi->a_paterno = $_POST["txtAPaterno"];
    $objAspi->a_materno = $_POST["txtAMaterno"];
    $objAspi->fecha_nac = $objSys->convertirFecha($_POST["txtFechaNac"], 'yyyy-mm-dd');
    $objAspi->genero = $_POST["rbnSexo"];
    $objAspi->id_estado_civil = $_POST["cbxEdoCivil"];
    $objAspi->id_status = 1;
    // Adscripci�n
    $objAspi->id_corporacion = 1;
    $objAspi->id_area = $_POST["cbxArea"];
    $objAspi->id_tipo_funcion = $_POST["cbxTipoFuncion"];
    $objAspi->id_especialidad = $_POST["cbxEspecialidad"];
    $objAspi->id_categoria = $_POST["cbxCategoria"];
    $objAspi->cargo = $_POST["txtCargo"];
    $objAspi->id_region_adscrip = $_POST["cbxRegion"];
    // Domicilio
    $objAspi->calle = $_POST['txtCalle'];
    $objAspi->num_ext = $_POST['txtNumExt'];
    $objAspi->colonia = $_POST['txtColonia'];
    $objAspi->ciudad = $_POST['txtCiudad'];
    $objAspi->tel_fijo = $_POST['txtTelFijo'];
    $objAspi->tel_movil = $_POST['txtTelMovil'];
    $objAspi->id_municipio_domi = $_POST['cbxMpioDomicilio'];
    // Nivel de Estudios
    $objAspi->id_nivel_estudios = $_POST['cbxNivelEstudios'];   
    
    //-------------------------------------------------------------------// 
    if( $_POST["dtTypeOper"] == 1 ){
        $result = $objAspi->insert();
        $oper = "Ins";
        $url_red_error = "aspi_add";
        
    } else {
        $result = $objAspi->update();
        $oper = "Upd";
        $url_red_error = "aspi_edit";
    }
    if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_aspirantes', $curp, $oper);
        $error = '';
    }
    else {
        $error = (!empty($objAspi->msjError)) ? $objAspi->msjError : 'Error al guardar los datos del nuevo recluta';        
    }
    
    $mod = (empty($error)) ? $objSys->encrypt("ctrl_aspi") : $objSys->encrypt($url_red_error); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>