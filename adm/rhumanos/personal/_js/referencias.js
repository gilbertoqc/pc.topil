$(document).ready(function(){    
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaNac').datepicker({
        yearRange: '1920:2002',
    });
    
    // M�scara de los campos tel�fonos
    $('#txtTelFijo').mask('(999) 99-99999');
    $('#txtTelMovil').mask('(999) 99-99999');
    
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAPaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAMaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtOcupacion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCalle').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
    $('#txtColonia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCiudad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
        
    // Control para la carga de tipos de relaciones
    $('#cbxTipoReferencia').change(function(){
        obtenerRelaciones($(this).val());
    });
    
    // Control para la carga din�mica de municipios
    $('#cbxEntDomicilio').change(function(){        
        obtenerMunicipios($(this).val());  
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    $('#frmRegistro').validate({
        rules:{
            cbxTipoReferencia:{
                required: true,
                min: 1,
            },
            cbxTipoRelacion:{
                required: true,
                min: 1,
            },
            txtNombre: 'required',            
            txtAPaterno: 'required',  
            txtAMaterno: 'required',
            rbnSexo: 'required',
            txtOcupacion: 'required',
            txtCalle: 'required',            
            txtColonia: 'required',
            txtCiudad: 'required',
            txtCodPostal:{
                digits: true,
                minlength: 5,
            },
            cbxEntDomicilio:{
                required: true,
                min: 1,
            },
            cbxMpioDomicilio:{
                required: true,
                min: 1,
            },
    	},
    	messages:{    
            cbxTipoReferencia:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTipoRelacion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    		txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAPaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAMaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnSexo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCalle: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtOcupacion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtColonia: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCiudad: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCodPostal:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o del C�digo Postal debe de ser 5 digitos"></span>',
            },
            cbxEntDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    
    // Control del formulario para agregar o modifiar una referencia
    $('#dvFormReferencia').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: true,
        width: 750,
        buttons: {
            'Guardar referencia': function(){
                guardarReferencia();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        },
        open: function(){
            resetFormulario();
            if ($('#hdnTypeOper').val() == 1) {
                $(this).dialog('option', 'title', 'SISP :: Nueva Referencia');
            } else if ($('#hdnTypeOper').val() == 2) {
                $(this).dialog('option', 'title', 'SISP :: Modificar Referencia');
                datosReferencia();
            }                
        }
    });
    $('#btnAgregar').click(function(){
        $('#hdnIdRef').val('0');
        $('#hdnTypeOper').val('1');
        $('#dvFormReferencia').dialog('open');
    });
    
    $('#dvGrid-Referencias a.lnkBtnOpcionGrid').live('click', function(){
        var xId = $(this).attr("rel").split('-');
        if( xId[0] == 'edt' ){
            $('#hdnIdRef').val( xId[1] );
            $('#hdnTypeOper').val('2');
            $('#dvFormReferencia').dialog('open');
        }
    });
    
    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGrid-Referencias table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGrid-Referencias table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
});

function guardarReferencia(){
    var valida = $('#frmRegistro').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los datos de la referencia actual?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    // Se genera el di�logo de espera
                    var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Guardando referencia, por favor espere...</div></div>');
                    // Se procesan los datos v�a ajax
                    $.ajax({
                        url: xDcrypt($('#hdnUrlSave').val()),
                        data: $('#frmRegistro').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        beforeSend: function () {
                            frmPreg.dialog('close');
                            // D�alogo de espera                     
                            frmWait.dialog({
                                autoOpen: true,
                                modal: true,
                                width: 400,
                            }); 
                        },
                        success: function (xdata) {
                            // Cierra el di�logo de espera
                            frmWait.dialog('close');
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                // Se actualiza el grid (lista) de referencia
                                $('#dvGrid-Referencias div.xGrid-dvBody').html(xdata.html);
                                //-- Se asigna el estilo intercalado de cada fila...
                                $('#dvGrid-Referencias table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
                                $('#dvGrid-Referencias table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');
                                // Se cierra el formulario principal
                                $('#dvFormReferencia').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            frmWait.dialog('close');
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

function datosReferencia(){
    $.ajax({
        url: xDcrypt($('#hdnUrlGet').val()),
        data: {'id': $('#hdnIdRef').val()},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            
        },
        success: function (xdata) {
            if (xdata.rslt) {                
                $('#cbxTipoReferencia').val(xdata.id_tipo_referencia);
                obtenerRelaciones(xdata.id_tipo_referencia);
                $('#cbxTipoRelacion').val(xdata.id_tipo_relacion);
                $('#txtNombre').val(xdata.nombre);
                $('#txtAPaterno').val(xdata.a_paterno);
                $('#txtAMaterno').val(xdata.a_materno);
                if( xdata.sexo == 1 )
                    $('input:radio[name=rbnSexo][value=1]').attr('checked', true);
                else
                    $('input:radio[name=rbnSexo][value=2]').attr('checked', true);
                $('#txtOcupacion').val(xdata.ocupacion);
                $('#txtCalle').val(xdata.calle);
                $('#txtColonia').val(xdata.colonia);
                $('#txtCiudad').val(xdata.ciudad);
                $('#txtCodPostal').val(xdata.cod_postal);                
                $('#cbxEntDomicilio').val(xdata.id_entidad);                
                obtenerMunicipios(xdata.id_entidad);
                $('#cbxMpioDomicilio').val(xdata.id_municipio);
                $('#txtTelFijo').val(xdata.tel_fijo);
                $('#txtTelMovil').val(xdata.tel_movil);
            } else {
                shwError(xdata.error);
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ' : ' + otroobj, 450);
        }
    });
}

function resetFormulario(){
    $('#cbxTipoReferencia').val(0);
    $('#cbxTipoRelacion').html('<option value="0"></option>');
    $('#txtNombre').val('');
    $('#txtAPaterno').val('');
    $('#txtAMaterno').val('');
    $('input:radio[name=rbnSexo]').attr('checked',false);
    $('#txtOcupacion').val('');
    $('#txtCalle').val('');
    $('#txtColonia').val('');
    $('#txtCiudad').val('');
    $('#txtCodPostal').val('');
    //$('#cbxMpioDomicilio').val(0);
    $('#cbxEntDomicilio').val(12);
    obtenerMunicipios(12);
    $('#txtTelFijo').val('');
    $('#txtTelMovil').val('');
    
    $(".validation span:not([class='pRequerido'])").hide();
    $(".validation").removeClass("error");
    $(".validation").removeClass("success");
}

function obtenerRelaciones(id){    
    $.ajax({
        url: xDcrypt($('#hdnUrlRel').val()),
        data: {'id_tp_referencia': id},
        dataType: 'json',
        async: false,
        cache: false,
        beforeSend: function () {
            $('#cbxTipoRelacion').html('<option>Cargando relaciones...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxTipoRelacion').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

function obtenerMunicipios(id){
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'filtro': 1},
        dataType: 'json',
        async: false,
        cache: false,
        beforeSend: function () {
            $('#cbxMpioDomicilio').html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxMpioDomicilio').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}