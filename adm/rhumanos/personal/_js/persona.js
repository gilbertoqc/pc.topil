var bandSelAspi = false;
$(document).ready(function(){ 
    $('#tabs').tabs();
    // Inicializaci�n de los tabs
    $('#tabsForm').tabs({
        disabled: [1,2,3]
    });    
        
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaNac').datepicker({
        yearRange: '1920:2002',
    });
    $('#txtFechaIng').datepicker({
        yearRange: '1950:',
    });
    
    // M�scara de los campos tel�fonos
    $('#txtTelFijo').mask('(999) 99-99999');
    $('#txtTelMovil').mask('(999) 99-99999');
            
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAPaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAMaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtRfc').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCuip').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCartilla').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtLicencia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtPasaporte').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
    $('#txtLugarNacimiento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCalle').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumExt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNumInt').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEntreCalle1').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEntreCalle2').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtColonia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCiudad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtInstitucion').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCct').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCarrera').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtEspecialidad').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtDocumento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtFolioDocto').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCargo').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    // Control especial del campo CURP.
    $('#txtCurp').blur(function(){
        $(this).val(xTrim( $(this).val().toUpperCase() ));
        if (msjErrorCURP != '') {
            shwError(msjErrorCURP);
        }
        else if ($(this).val() != '') {
            // Obtiene la fecha de nacimiento
            var fecha = $(this).val().substr(4, 6);
            $('#txtFechaNac').val(fecha.substr(4, 2) + '/' + fecha.substr(2, 2) + '/19' + fecha.substr(0, 2) );
            // Asigna el rfc autom�tico
            $("#txtRfc").val( $(this).val().substr(0, 10) );
            // Asigna el g�nero autom�tico
            var genero = $(this).val().substr(10, 1);  
            if (genero == "H")
                $("#rbnSexo1").attr('checked', 'checked');
            else if (genero == "M" )
                $("#rbnSexo2").attr('checked', 'checked');
        }            
    });
    
    // Control para la carga din�mica de municipios
    $('#cbxEntNacimiento').change(function(){        
        obtenerMunicipios('cbxMpioNacimiento', $(this).val());  
    });
    $('#cbxEntDomicilio').change(function(){
        obtenerMunicipios('cbxMpioDomicilio', $(this).val());  
    });
    $('#cbxRegion').change(function(){
        obtenerMunicipios('cbxMpioAdscripcion', $(this).val());  
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    jQuery.validator.addMethod("valCURP", function(value, element) {
        return validaCURP(value);
    });
    $('#frmRegistro').validate({
        rules:{
            // Datos Personales
            txtCurp:{
                required: true,
                minlength: 18,
                valCURP: true,    
            },
            txtNombre: 'required',            
            txtAPaterno: 'required',  
            txtAMaterno: 'required',
            rbnSexo: 'required',
            txtFechaNac: 'required',
            txtRfc: 'required',
            txtCuip:{
                minlength: 16,
            },
            txtFolioIfe: 'digits',
            cbxEdoCivil:{
                required: true,
                min: 1,
            },
            // Domicilio
            txtCalle: 'required',
            txtNumExt: 'required',
            txtColonia: 'required',
            txtCiudad: 'required',
            txtCodPostal:{ 
                digits: true,
                minlength: 5,
            },
            cbxEntDomicilio:{
                required: true,
                min: 1,
            },
            cbxMpioDomicilio:{
                required: true,
                min: 1,
            },
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: true,
                min: 1,
            },
            txtInstitucion: 'required',
            txtCedula:{
                digits: true,
                minlength: 7,
            },
            // Adscripci�n
            cbxArea:{
                min: 1,
            },
            cbxTipoFuncion:{
                min: 1,
            },
            cbxEspecialidad:{
                min: 1,
            },
            cbxCategoria:{
                min: 1,
            },
            txtCargo: 'required',
            cbxNivelMando:{
                min: 1,
            },
            txtFechaIng: 'required',
            cbxHorario:{
                min: 1,
            },
            cbxRegion:{
                min: 1,
            },
            cbxMpioAdscripcion:{
                min: 1,
            },
            cbxUbicacion:{
                min: 1,
            },
    	},
    	messages:{
    	   txtCurp:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la CURP debe de ser de 18 caracteres"></span>',
                valCURP: '<span class="ValidateError" title="La CURP especificada no es v�lida"></span>',    
            },
    		txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAPaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAMaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnSexo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaNac: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtRfc: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCuip:{ 
                minlength: '<span class="ValidateError" title="El tama�o de la CUIP debe de ser de al menos 16 caracteres"></span>',
            },
            txtFolioIfe: '<span class="ValidateError" title="Este campo s�lo acepta d�gitos"></span>',
            cbxEdoCivil:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            }, 
            // Domicilio
            txtCalle: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNumExt: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtColonia: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCiudad: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCodPostal:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o del C�digo Postal debe de ser 5 digitos"></span>',
            },
            cbxEntDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioDomicilio:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },           
            // Nivel de Estudios
            cbxNivelEstudios:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtInstitucion: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCedula:{
                digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la C�dula Profesional debe de ser 7 digitos"></span>',
            },
            // Adscripci�n
            cbxArea:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTipoFuncion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxEspecialidad:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxCategoria:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtCargo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxNivelMando:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            txtFechaIng: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            cbxHorario:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxRegion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMpioAdscripcion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxUbicacion:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    // Acci�n del bot�n Siguiente
    $('#btnSiguiente').click(function(){        
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            // Se obtiene el total de tabs
            var totalTabs = $('#tabsForm >ul >li').size();            
            // Se obtien el id del tab actual activo
            var tabActual = $('#tabsForm').tabs('option', 'active'); 
            // Deshabilita todos los tabs
            $('#tabsForm').tabs('option', 'disabled', [0,1,2,3]);           
            // Se habilita y se muestra el siguiente tab
            $('#tabsForm').tabs('enable', (tabActual + 1));
            $('#tabsForm').tabs('option', 'active', (tabActual + 1));                        
            if (tabActual == (totalTabs - 2)) {            
                $('#btnSiguiente').css('display', 'none');
                $('#btnGuardar').css('display', 'inline-block');
            }            
            if (tabActual == 0)
                $('#btnAnterior').css('display', 'inline-block');            
        }
    });
    // Acci�n del bot�n Anterior
    $('#btnAnterior').click(function(){
        // Se obtien el id del tab actual activo
        var tabActual = $('#tabsForm').tabs('option', 'active');
        // Deshabilita todos los tabs
        $('#tabsForm').tabs('option', 'disabled', [0,1,2,3]);
        if (tabActual > 0){            
            $('#tabsForm').tabs('enable', (tabActual - 1));
            $('#tabsForm').tabs('option', 'active', (tabActual - 1));
        }
        if (tabActual == 3) {
            $('#btnGuardar').css('display', 'none');
            $('#btnSiguiente').css('display', 'inline-block');
        }
        else if (tabActual == 1) {
            $(this).css('display', 'none');
        }
    });
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de continuar con el registro del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });  
    
    /*--------------------------------------------------------------------//
     * Controla la selecci�n del recluta a registrar como personal activo
     *--------------------------------------------------------------------*/
    // Definici�n del control grid para la b�squeda de reclutas
    xGrid = $('#dvGrid-Aspi').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xTypeSearch: 2,
        xOnlySearch: true,
        xUrlData: xDcrypt($('#hdnUrlDatAspiGrid').val()),
        xLoadDataStart: 0,
        xTypeDataAjax: 'json'
    });
    // Definicion de la ventana de selecci�n de reclutas
    $('#dvLista-Aspi').dialog({
        autoOpen: true,
        closeOnEscape: false,
        draggable: true,
        height: 480,
        modal: true,
        width: 1000,
        open: function(){
            
        },
        buttons:{
            '-- Cancelar --': function(){
                location.href = 'index.php';
            }
        },
        close: function(){
            if( !bandSelAspi )
                location.href = 'index.php';
            else
                $(this).dialog('close');
        }
    });
    // Seleccion de un registro
    $('#dvGrid-Aspi a.lnkBtnOpcionGrid').live('click', function(){
        // Se obtienen los datos del recluta seleccionado
        obtieneDatosAspi($(this).attr('rel'));
    });
    
    /*--------------------------------------------------------------------//
     * Controla el scroll para seguir mostrando el header y el toolbar...
     *-------------------------------------------------------------------*/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();    
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');            
        }
    });    
});

function obtenerMunicipios(contenedor, id){
    var tp_filtro = ( contenedor != 'cbxMpioAdscripcion' ) ? 1 : 2;
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'id_region': id, 'filtro': tp_filtro},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}

function obtieneDatosAspi(curp){
    $.ajax({
        url: xDcrypt($('#hdnUrlDatAspi').val()),
        data: {'curp': curp},
        dataType: 'json',
        async: false,
        cache: false,
        beforeSend: function(){
            
        },
        success: function(xdata){
            if( xdata.rslt ){
                // Datos personales
                $('#txtCurp').val(xdata.curp);
                $('#hdnCurpAspi').val(xdata.curp);            
                $('#txtNombre').val(xdata.nombre);
                $('#txtAPaterno').val(xdata.a_paterno);
                $('#txtAMaterno').val(xdata.a_materno);
                $('#txtFechaNac').val(xdata.fecha_nac);
                if( xdata.genero == 1 )
                    $('#rbnSexo1').attr('checked', 'true');
                else
                    $('#rbnSexo2').attr('checked', 'true');
                $('#txtRfc').val(xdata.curp.substr(0,10));
                $('#cbxEdoCivil').val(xdata.id_estado_civil);
                // Domicilio
                $('#txtCalle').val(xdata.calle);
                $('#txtNumExt').val(xdata.num_ext);
                $('#txtColonia').val(xdata.colonia);
                $('#txtCiudad').val(xdata.ciudad);
                if( xdata.id_entidad_domi != $('#cbxEntDomicilio').val() ){
                    $('#cbxEntDomicilio').val(xdata.id_entidad_domi);                
                    $.ajax({
                        url: xDcrypt($('#hdnUrlMpio').val()), 
                        data: {'id_entidad': xdata.id_entidad_domi, 'filtro': 1},
                        dataType: 'json',
                        async: false,
                        cache: false, 
                        success: function(xdat){
                            $('#cbxMpioDomicilio').html(xdat.html);
                            $('#cbxMpioDomicilio').val(xdata.id_municipio_domi);
                        }
                    });
                } else {
                    $('#cbxMpioDomicilio').val(xdata.id_municipio_domi);
                }                               
                $('#txtTelFijo').val(xdata.tel_fijo);
                $('#txtTelMovil').val(xdata.tel_movil);
                // Nivel de estudios
                $('#cbxNivelEstudios').val(xdata.id_nivel_estudios);
                // Adscripci�n
                $('#cbxArea').val(xdata.id_area);
                $('#cbxTipoFuncion').val(xdata.id_tipo_funcion);
                $('#cbxEspecialidad').val(xdata.id_especialidad);
                $('#cbxCategoria').val(xdata.id_categoria);
                $('#txtCargo').val(xdata.cargo);
                $('#cbxRegion').val(xdata.id_region_adscrip);
                obtenerMunicipios('cbxMpioAdscripcion', xdata.id_region_adscrip);
                
                bandSelAspi = true;
                $('#dvLista-Aspi').dialog('close');
            } else {
                bandSelAspi = false;
                shwError(xdata.error);    
            }
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}