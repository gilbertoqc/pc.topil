<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla de aspirantes en el grid.
 * Lista de par�metros recibidos por GET 
 * @param txt, string: folio � n�mero de la plaza
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_plantilla_plazas.class.php';
    $objSys = new System();
    $objPlaza = new AdmtblPlantillaPlazas();

    if( $objPlaza->selectPlaza($_GET["txt"]) ){
        $ajx_datos["rslt"] = true;
        if( $objPlaza->id_plaza > 0 ){
            $ajx_datos["id_plaza"] = $objPlaza->id_plaza;
            $ajx_datos['categoria'] = utf8_encode($objPlaza->AdmcatCategorias->categoria);
            $ajx_datos['area'] = utf8_encode($objPlaza->AdmcatAreas->area);
            $ajx_datos['status'] = $objPlaza->status; 
        } else {
            $ajx_datos["id_plaza"] = 0;
        }
    } else {
        $ajx_datos["rslt"] = false;
        $ajx_datos["error"] = $objPlaza->msjError;
    }    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>