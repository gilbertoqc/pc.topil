<?php
/**
 * Complemento ajax para obtener los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/admtbl_sanciones.class.php';
    $objSancion = new AdmtblSanciones();
    
    $objSancion->select($_GET["id"]);
    if ($objSancion->id_sancion > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_tipo_sancion"] = $objSancion->id_tipo_sancion;
        $ajx_datos["id_motivo_sancion"] = $objSancion->id_motivo_sancion;                
        $ajx_datos["duracion"] = $objSancion->duracion;
        $ajx_datos["observa"] = utf8_encode($objSancion->observaciones);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objSancion->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>