var xGrid = "",
    xFormFicha = "",
    hWind = 0;
$(document).ready(function(){
    hWind = $(window).height();
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Personal').css('height', (hWind - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xRowsDisplay: 35,
        xTypeDataAjax: 'json'
    });
    
    //-- Definici�n del dialog como formulario para la vista previa de la ficha...
    xFormFicha = $('#dvForm-Ficha').dialog({
        autoOpen: false,
        width: 1000,
        height: (hWind - 150),
        modal: true,
        resizable: false,
    });
       
    /*
    //-- Funcionalidad de los campos de autocompletado...
    $('#txtNombre').autocomplete({
        minLength: 3,
        source: 'gventas/ajx_get_clientes.php',
        select: function(event, ui){
            $('#txtModelo').focus();
            event.preventDefault();
            $('#txtNombre').val( ui.item.nombre );
            $('#txtDomicilio').val( ui.item.domicilio );
            $('#txtTelefono').val( ui.item.telefono );
            $('#dtCteReg').val( ui.item.id_cte );
            $('#dtTpOper').val('2');
        } 
    });  
    */
    
    //-- Funcionalidad de los botones de la barra de herramientas...
    $('#xRegistrar').click(function(){
        //$('#dvForm-Registro').dialog('open'); 
    });
    $('#xReportes').click(function(){
        $('#dvForm-Registro').dialog('open'); 
    });
    
    $('#dvGrid-Personal a.lnkBtnOpcionGrid').live('click', function(){
        var prm = $(this).attr("rel").split(';');
        if( prm[0] == 'inf' ){         
            xFormFicha.dialog({
                open: function(){
                    var hgtBody = ((hWind - 150) / 2 ) - 50;
                    $(this).html('<div class="xGrid-dvWait-LoadData" style="padding-top: ' + hgtBody + 'px;">Cargando datos, por favor espere...</div>');
                    getFicha(prm[1]);
                },
                buttons:{
                    '-- Cerrar --': function(evt){
                        $(this).dialog('close');
                    }
                }
            });
            xFormFicha.dialog('open');
        }   
    });
    
});

/*
* Funci�n para obtener los datos del cliente.
*/
function getFicha(xId){
    $('#dvForm-Ficha').load(
        xDcrypt($('#hdnUrlFicha').val()), 
        {'id': xId}, 
        function(){
            $('#objPdf').css('height', (hWind - 290) + 'px');        
        }
    );
}