var xGrid;
var xGrid2;
var xGrid3;
$(document).ready(function(){
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Personal').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });    
    // Definici�n del control grid la muestra de las armas asignadas    
    xGrid2 = $('#dvGrid-Portacion').xGrid({                
        xOnlySearch: true,                           
        xUrlData: xDcrypt($('#hdnUrlObtArmAsig').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });  
    xGrid3 = $('#dvBuscarCont').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xTypeSearch: 2,
        xOnlySearch: true,                    
        xUrlData: xDcrypt($('#hdnUrlObtArmas').val()),
        xLoadDataStart: 0,
        xTypeDataAjax: 'json'
    });
    //-- Definici�n del dialog como formulario para portacion de armas....
    $('#dvArmas').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 930,
        height: 570,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Controla el bot�n para visualizar el formulario de portacion de armas...
    $(".classAsigna").live("click", function(e){        
        $.ajax({
            url: xDcrypt( $('#hdnUrlObtDatosPer').val() ),  
            data: { 'curp': $(this).attr("rel").substring(4) },          
            dataType: 'json',
            type: 'post',
            async: true,
            cache: false,            
            success: function (xdata) {   
                $("#hdnUrlCurp").val( xdata.curp ) 
                xGrid2.resetAjaxParam();
                xGrid2.addParamAjax( "curp="+xdata.curp );  
                xGrid2.refreshGrid();                                     
                $('#dvDatos').html(xdata.html_dat);                                                             
                $('#dvArmas').dialog('option','title',xdata.titulo);
                $('#dvArmas').dialog('open');                    
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });         
    
    //-- Funcion para descargar un arma seleccionada al elemento que la porta.
    $(".classDescarga").live("click", function(e){                   
        var MAT = $(this).attr("rel");
        //se crea un mensaje de confirmacion para descargar el arma.
        var msj = $(getHTMLMensaje(' Confirme la descarga del armamento [ '+ MAT.substring(4) + ' ]', 2));        
        msj.dialog({
            autoOpen: true,                        
            minWidth: 450,
            resizable: false,
            modal: true,
            buttons:{
                'Aceptar': function(){
                    $(this).dialog('close');                                        
                    $.ajax({
                        url: xDcrypt( $('#hdnUrlOperArma').val() ),
                        data: { 'matricula' : MAT.substring(4), 'curp' : $('#hdnUrlCurp').val(), 'oper' : '2' },
                        dataType: 'json',
                        type: 'POST',
                        async: true,
                        cache: false,            
                        success: function (xdata) {      
                                if( xdata.rslt ){                                                                          
                                    alert('Arma descargada.');
                                    //se actualiza el grid
                                    xGrid2.refreshGrid();  
                                }else{
                                    alert('Existe un problema al realizar la acci�n.');
                                }                                                                                                      
                        },
                        error: function(objeto, detalle, otroobj){                            
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }                        
                    });                                        
                },
                'Cancelar': function(){
                    $(this).dialog('close');                    
                }
            }
        });
        
    }); 
    
    //-- Controla el bot�n para visualizar el formulario de busqueda de armas para portacion..
    $('#dvBuscar').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 940,
        height: 420,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                 
                $(this).dialog('close');                                
            }
        }
    }); 
    
    //-- Controla el bot�n para visualizar el formulario de de busqueda de armas...
    $(".classBusca").live("click", function(e){        
        $("#dvBuscar").dialog('open');
    }); 
    
     // Seleccion de una matricula y almacenamiento del registro correspondiente en la tabla de la bd.
    $('#dvBuscarCont a.lnkBtnOpcionGrid').live('click', function(){                    
        var datos = $(this).attr('rel').split('-');
        
        $.ajax({
            url: xDcrypt( $('#hdnUrlOperArma').val() ),  
            data: { 'matricula' : datos[0], 'curp' : $('#hdnUrlCurp').val(), 'oper' : '1' },          
            dataType: 'json',
            type: 'POST',
            async: true,
            cache: false,            
            success: function (xdata) {                               
                    if( xdata.rslt ){     
                        alert('Arma asignada');
                        //se actualiza el grid
                        xGrid2.refreshGrid();                                      
                    }else{
                        alert('Existe un problema al realizar la acci�n.');
                    }                                                                      
            },
            error: function(objeto, detalle, otroobj){                            
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }                        
        });                        
        $('#dvBuscar').dialog('close');              
    });

    $('#xReportes').click(function(){
        $('#dvForm-Registro').dialog('open'); 
    });

});