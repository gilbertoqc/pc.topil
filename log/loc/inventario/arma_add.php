<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_arm_armamento.class.php';
include 'includes/class/logcat_arm_tipo.class.php';
include 'includes/class/logcat_arm_clase.class.php';
include 'includes/class/logcat_arm_marca.class.php';
include 'includes/class/logcat_arm_folioc.class.php';
include 'includes/class/logcat_arm_tipodependencia.class.php';

$objArm             = new LogtblArmArmamento();
$objTipo            = new LogcatArmTipo();
$objClase           = new LogcatArmClase();
$objMarca           = new LogcatArmMarca();
$objFolioc          = new LogcatArmFolioc();
$objTipoDep         = new LogcatArmTipodependencia();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Inventario',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="log/loc/_js/inventario/arma_add.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
        $url_cancel = "index.php?m=" . $_SESSION["xIdMenu"];
        $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('arma_rg');
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                </a>
                <a href="<?php echo $url_cancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                </a>
                </td>
            </tr>
        </table>
    </div>
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Registro</span>                                
            <div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">                         
                <!-- registro de armamento -->                                                                   
                <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                    <tr>
                        <td><label for="txtMatricula">Matricula:</label></td>
                        <td class="validation" style="width: 400px;">
                            <input type="text" name="txtMatricula" id="txtMatricula" maxlength="12" title="..." style="padding: 5px; width: 210px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtSerie">Serie:</label></td>
                        <td class="validation">
                            <input type="text" name="txtSerie" id="txtSerie" value="" maxlength="20" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxLicencia">Licencia:</label></td>
                        <td class="validation">
                            <select name="cbxLicencia" id="cbxLicencia" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmLoc->shwLicencias( $objArm->id_loc );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxTipo">Tipo:</label></td>
                        <td class="validation">
                            <select name="cbxTipo" id="cbxTipo" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipo->shwTipos( $objClase->getIdTipo( $objMarca->getIdClase( $objArm->LogcatArmModelo->getIdMarca( $objArm->id_modelo ) ) ) );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxClase">Clase:</label></td>
                        <td class="validation">
                            <select name="cbxClase" id="cbxClase" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objClase->shwClases( $objMarca->getIdClase( $objArm->id_modelo ), $objArm->id_tipo );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxMarca">Marca:</label></td>
                        <td class="validation">
                            <select name="cbxMarca" id="cbxMarca" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objMarca->shwMarcas( 0, $objMarca->getIdClase( $objArm->id_modelo ), $objArm->id_modelo );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                     <tr>
                        <td><label for="cbxModelo">Modelo:</label></td>
                        <td class="validation">
                            <select name="cbxModelo" id="cbxModelo" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmModelo->shwModelos( $objArm->id_modelo );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxCalibre">Calibre:</label></td>
                        <td class="validation">
                            <select name="cbxCalibre" id="cbxCalibre" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmModelo->LogcatArmCalibre->shwCalibres( $objArm->LogcatArmModelo->getIdCalibre( $objArm->id_modelo ) );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxFolioc">Folio C:</label></td>
                        <td class="validation">
                            <select name="cbxFolioc" id="cbxFolioc" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objFolioc->shwFolioc( $objArm->LogcatArmFoliod->getIdFolioc( $objArm->id_foliod ) );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                     <tr>
                        <td><label for="cbxFoliod">Folio D:</label></td>
                        <td class="validation">
                            <select name="cbxFoliod" id="cbxFoliod" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmFoliod->shwFoliod(  );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                     
                    <tr>
                        <td><label for="cbxPropietario">Propietario:</label></td>
                        <td class="validation">
                            <select name="cbxPropietario" id="cbxPropietario" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmPropietario->shwPropietario( $objArm->id_propietario );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxTipoDep">Tipo dependencia:</label></td>
                        <td class="validation">
                            <select name="cbxTipoDep" id="cbxTipoDep" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipoDep->shwTipoDepdendencias( $objArm->LogcatArmInstituciones->LogcatArmDependencias->getIdTipoDependencia( $objArm->LogcatArmInstituciones->getIdDependencia( $objArm->id_instituciones ) ) );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxDep">Dependencia:</label></td>
                        <td class="validation">
                            <select name="cbxDep" id="cbxDep" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmInstituciones->LogcatArmDependencias->shwDepdendencias( $objArm->id_instituciones );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxTipoDep">Instituciones:</label></td>
                        <td class="validation">
                            <select name="cbxInst" id="cbxInst" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objArm->LogcatArmInstituciones->shwInstituciones( $objArm->id_instituciones );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                    
                    <tr>
                        <td><label for="cbxEstado">Estado:</label></td>
                        <td class="validation">
                            <select name="cbxEstado" id="cbxEstado" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                //echo $objEstado->shwEstado();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEstatus">Estatus:</label></td>
                        <td class="validation">
                            <select name="cbxEstatus" id="cbxEstatus" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                //echo $objEstatus->shwEstatus();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                     <tr>
                        <td><label for="cbxMotivoMov">Motivo movimiento:</label></td>
                        <td class="validation">
                            <select name="cbxMotivoMov" id="cbxMotivoMov" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                //echo $objMotivoMov->shwMotivoMov();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxUbicacion">Ubicaci&oacute;n</label>:</label></td>
                        <td class="validation">
                            <select name="cbxUbicacion" id="cbxUbicacion" title="">
                                <option value="0">&nbsp;</option>
                                <?php
                                //echo $objUbicacion->shwUbicacion();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                </table>                                                  
            </div>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="1" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('log/loc/inventario/ajx_obt_municipios.php');?>" />
    </form>
  
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>