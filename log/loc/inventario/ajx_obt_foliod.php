<?php
/**
 * Complemento del llamado ajax para listar las marcas dentro de un combobox. 
 * @param int id_clase, recibido por el m�todo GET.
 * @param int id_modelo, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/logcat_arm_foliod.class.php';
    $objFoliod = new LogcatArmFoliod();
    
    $datos = '<option value="0"></option>';
    if ( $_GET["id_folioc"] != 0 ){
        $datos .= $objFoliod->shwFoliod( 0, $_GET["id_folioc"] );
    }
    if (empty($objFoliod->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objFoliod->msjError;        
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>