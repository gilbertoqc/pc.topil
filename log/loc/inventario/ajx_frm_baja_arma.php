<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/logtbl_arm_armamento.class.php';
    include $path . 'includes/class/logcat_arm_motivo_baja.class.php';
    
    $objArm             = new LogtblArmArmamento();    
    $objMotivo          = new LogcatArmMotivoBaja();

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//
    $html .= '<script type="text/javascript" src="log/loc/_js/inventario/ajx_frm_baja_arma.js"></script>';
                    
    $html .='<form id="frmBaja" method="post" action="' . $urlSave . '" enctype="multipart/form-data">';    
            $html .='<div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 160px; width: auto;">';                         
                //registro de armamento
                $html .='<table id="tbForm-Baja-Arma" class="tbForm-Data" style="width: 500px;">'; 
                    $html .='<tr>';
                        $html .='<td style="width: 200px;"><label for="txtMatricula">Matricula:</label></td>';
                        $html .='<td class="validation" style="width: 300px;">';
                            $html .='<input type="text" name="txtMatricula" id="txtMatricula" value="' . $_POST["matricula"] . '" maxlength="12" title="..." style="padding: 5px; width: 210px;" readonly="true"/>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                 
                    $html .='<tr>';
                        $html .='<td style="width: 200px;"><label for="txtOficio">Oficio:</label></td>';
                        $html .='<td class="validation" style="width: 300px;">';
                            $html .='<input type="text" name="txtOficio" id="txtOficio" value="" maxlength="20" title="..." style="width: 200px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td style="width: 200px;"><label for="txtFechaOficio">Fecha de oficio:</label></td>';
                        $html .='<td class="validation" style="width: 300px;">';
                            $html .='<input type="text" name="txtFechaOficio" id="txtFechaOficio" value="" maxlength="10" title="..." style="width: 100px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td style="width: 200px;"><label for="cbxMotivo">Motivo:</label></td>';
                        $html .='<td class="validation" style="width: 300px;">';
                            $html .='<select name="cbxMotivo" id="cbxMotivo" title="..." >';
                            $html .='<option value="0">&nbsp;</option>';
                                $html .= $objMotivo->shwMotivos();                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    /*
                    $html .='<tr>';
                        $html .='<td style="width: 300px;"><label for="txtFechaBaja">Fecha de baja:</label></td>';
                        $html .='<td class="validation" style="width: 300px;">';
                            $html .='<input type="text" name="txtFechaBaja" id="txtFechaBaja" value="" maxlength="10" title="..." style="width: 100px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    */
                $html .='</table>';                                                  
            $html .='</div>';
            $html .='<p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>';                        
        $html .= '<input type="hidden" id="dtTypeOper" name="dtTypeOper" value="1" />';
    $html .='</form>';
  

    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>