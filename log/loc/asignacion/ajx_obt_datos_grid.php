<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_arm_asignacion.class.php';
    $objSys = new System();
    //$objArm = new LogtblArmArmamento();
    $objAsig = new LogtblArmAsignacion();
    

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    
    if (!empty($_GET["txtSearch"])) {
        $sql_where = ' (a.nombre LIKE ? OR a.a_paterno LIKE ? OR a.a_materno LIKE ? '
                   . 'OR a.curp LIKE ? '
                   . 'OR j.especialidad LIKE ? '
                   . 'OR h.categoria LIKE ? '
                   . 'OR i.area LIKE ? )';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%');
    }
    
    // Ordenaci�n...
    $sql_order = '';
    
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'a.nombre ' . $_GET['typeSort']  . ', a.a_paterno ' . $_GET['typeSort']  . ', a.a_materno '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'a.curp ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'j.especialidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'h.categoria ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'i.area ' . $_GET['typeSort'];
    }  
    
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
      
    $datos = $objAsig->selectAllGridPer($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ( $totalReg > 0 ) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["curp"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["curp"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                   //$html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
       			$nombrePersona = $dato["a_paterno"] . ' ' . $dato["a_materno"] . ' ' .$dato["nombre"];                
                $html .= '<td style="text-align: center; width: 20%;"><a href="#" class="toolTipTrigger" rel="' . $id_crypt . '">' . $nombrePersona . '</a></td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["curp"] . '</td>';                                
                $html .= '<td style="text-align: center; width: 20%;">' . $dato["especialidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 17%;">' . $dato["categoria"] . '</td>';
                $html .= '<td style="text-align: center; width: 25%;">' . $dato["area"] . '</td>';                
                $html .= '<td style="text-align: center; width:  5%;">';
$html .= '  <a href="#" rel="asg-' . $dato["curp"] . '" class="lnkBtnOpcionGrid classAsigna" title="Ver la portaci&oacute;n de armamento..." >';
$html .='<img src="' . PATH_IMAGES . 'icons/asig_arma24.png" alt="Ver la portaci&oacute;n de armamento." /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';               		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron personas en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>