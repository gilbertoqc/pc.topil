<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_exp_expediente.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario();        
    $objExp  = new LogtblExpExpediente();                               
    
    $curp = $_POST["curp"];                      
    if( $objExp->select( $curp ) ){
        if ( $resp = $objExp->insert( $curp ) ) {
            $objSys->registroLog($objUsr->idUsr, 'logtbl_exp_expediente', $objExp->curp, "Ins");
            $ajx_datos['rslt']  = true;                                    
            $ajx_datos['error'] = '';
                
        } else {
            $ajx_datos['rslt']  = false;                   
            $ajx_datos['error'] = $objExp->msjError;
        }
    }else{
        $ajx_datos['rslt']  = false;                
        $ajx_datos['error'] = $objExp->msjError; 
    }
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;    
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>