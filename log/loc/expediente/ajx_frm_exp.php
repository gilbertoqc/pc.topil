<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_exp_expediente.class.php';   
    
    $objSys = new System();
    $objExp = new LogtblExpExpediente();
    
    //se incluye archivo javascript para funcionalidad necesario
    $html .= '<script type="text/javascript" src="log/loc/_js/expediente/ajx_frm_exp.js"></script>';
    //$html .= '<script type="text/javascript" src="log/loc/_js/radioButton.js"></script>';  
    //$html .= '<link type="text/css" rel="stylesheet" href="log/loc/_css/locStyles.css" />';   
    //$html .= '<link type="text/css" rel="stylesheet" href="log/loc/_css/font-awesome410/css/font-awesome.css" />'; 
    
    //variables recibidas
    $curp = $_POST["curp"]; 
    //se seleccionan los datos personales
    $datos = $objExp->select($curp);               
    $datPer = $objExp->AdmtblDatosPersonales;
    $datPer->select( $curp );

    //se elabora el encabezado de la ventana
    $titulo = 'Expediente LOC [ ' . $datPer->nombre . " " . $datPer->a_paterno . " " . $datPer->a_materno . ' ] ';
    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
    $col1='10%';
    $col2='50%';
    $col3='10%';
    $col4='10%';
    $col5='20%';                      
                  
    $html .='<form id="frmExp" method="post" enctype="multipart/form-data">';                
            
        $html.='';                        
        //$datosAsg = $objAsigna->selectAll( "a.curp='" . $curp ."'" ); 
        
        //PORTACION DE ARMAMENTO DEL ELEMENTO EN CUESTION.    
        $html.='';                                                                                       
        $html.='<div style="width: 100%; position: relative; padding-top: 5px;">';
            $html.='<table class="styTablaExp">';
            $html.='<thead>';
                //$html.='<tr><th colspan="3">E&nbsp;X&nbsp;P&nbsp;E&nbsp;D&nbsp;I&nbsp;E&nbsp;N&nbsp;T&nbsp;E</th></tr>';  
            $html.='</thead>'; 
            $html.='<tbody>';
                $html.='<tr><td colspan="2">&nbsp</td><td style="text-align: center;">NO</td><td style="text-align: center;">SI</td><td>&nbsp</td></tr>';
                $html.='<tr>';
                    $cons0= $objExp->constancia == 0 ? 'checked="true"' : '';  
                    $cons1= $objExp->constancia == 1 ? 'checked="true"' : '';
                    $html.='<td style="width: '.$col1.';">&nbsp;</td>'; 
                    $html.='<td style="text-align: right; width: '.$col2.';"><p>Constancia modo honesto de vivir</p></td>';
                    $html.='<td style="text-align: center; width: '.$col3.';">';
                        $html.='<input type="radio" id="radio1" name="constancia" class="classRadio" value="0" '.$cons0.'/> ';
                        $html.='<label for="radio1">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='NO';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="text-align: center; width: '.$col4.';">';
                        $html.='<input type="radio" id="radio2" name="constancia" class="classRadio" value="1" '.$cons1.' /> ';
                        $html.='<label for="radio2">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='SI';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="width: '.$col5.';">&nbsp;</td>';                        
                $html.='</tr>';
                $html.='<tr>';
                    $cart0= $objExp->cartilla_smn == 0 ? 'checked' : '';  
                    $cart1= $objExp->cartilla_smn == 1 ? 'checked' : '';
                    $html.='<td style="width: '.$col1.';">&nbsp;</td>'; 
                    $html.='<td style="text-align: right; width: '.$col2.';"><p>Cartilla SMN</p></td>';
                    $html.='<td style="text-align: center; width: '.$col3.';">';
                        $html.='<input type="radio" id="radio3" name="cartilla_smn" class="classRadio" value="0" '.$cart0.'/> ';
                        $html.='<label for="radio3">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='NO';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="text-align: center; width: '.$col4.';">';
                        $html.='<input type="radio" id="radio4" name="cartilla_smn" class="classRadio" value="1" '.$cart1.'/> ';
                        $html.='<label for="radio4">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='SI';
                        $html.='</label>';
                    $html.='</td>';
                    $html.='<td style="width: '.$col5.';">&nbsp;</td>'; 
                $html.='</tr>';
                $html.='<tr>';
                    $med0= $objExp->medico == 0 ? 'checked' : '';  
                    $med1= $objExp->medico == 1 ? 'checked' : '';
                    $html.='<td style="width: '.$col1.';">&nbsp;</td>'; 
                    $html.='<td style="text-align: right; width: '.$col2.';"><p>Evaluaci&oacute;n M&eacute;dica</p></td>';
                    $html.='<td style="text-align: center; width: '.$col5�3.';">';
                        $html.='<input type="radio" id="radio5" name="medico" class="classRadio" value="0" '.$med0.'/> ';
                        $html.='<label for="radio5">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='NO';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="text-align: center; width: '.$col4.';">';
                        $html.='<input type="radio" id="radio6" name="medico" class="classRadio" value="1" '.$med1.'/> ';
                        $html.='<label for="radio6">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='SI';
                        $html.='</label>';
                    $html.='</td>';
                    $html.='<td style="width: '.$col5.';">&nbsp;</td>'; 
                $html.='</tr>';
                $html.='<tr>';
                    $tox0= $objExp->toxicologico == 0 ? 'checked' : '';  
                    $tox1= $objExp->toxicologico == 1 ? 'checked' : '';
                    $html.='<td style="width: '.$col1.';">&nbsp;</td>'; 
                    $html.='<td style="text-align: right; width: '.$col2.';"><p>Evaluaci&oacute;n Toxicol&oacute;gica</p></td>';
                    $html.='<td style="text-align: center; width: '.$col3.';">';
                        $html.='<input type="radio" id="radio7" name="toxicologico" class="classRadio" value="0" '.$tox0.'/> ';
                        $html.='<label for="radio7">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='NO';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="text-align: center; width: '.$col4.';">';
                        $html.='<input type="radio" id="radio8" name="toxicologico" class="classRadio" value="1" '.$tox1.'/> ';
                        $html.='<label for="radio8">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='SI';
                        $html.='</label>';
                    $html.='</td>';
                    $html.='<td style="width: '.$col5.';">&nbsp;</td>'; 
                $html.='</tr>';
                $html.='<tr>';
                    $psi0= $objExp->psicologico == 0 ? 'checked' : '';  
                    $psi1= $objExp->psicologico == 1 ? 'checked' : '';
                    $html.='<td style="width: '.$col1.';">&nbsp;</td>'; 
                    $html.='<td style="text-align: right; width: '.$col2.';"><p>Evaluaci&oacute;n Psicol&oacute;gica</p></td>';
                    $html.='<td style="text-align: center; width: '.$col3.';">';
                        $html.='<input type="radio" id="radio9" name="psicologico" class="classRadio" value="0" '.$psi0.'/> ';
                        $html.='<label for="radio9">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='NO';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="text-align: center; width: '.$col4.';">';
                        $html.='<input type="radio" id="radio10" name="psicologico" class="classRadio" value="1" '.$psi1.'/> ';
                        $html.='<label for="radio10">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='SI';
                        $html.='</label>';
                    $html.='</td>';
                    $html.='<td style="width: '.$col5.';">&nbsp;</td>'; 
                $html.='</tr>';
                $html.='<tr>';
                    $ant0= $objExp->ant_np == 0 ? 'checked' : '';  
                    $ant1= $objExp->ant_np == 1 ? 'checked' : '';
                    $html.='<td style="width: '.$col1.';">&nbsp;</td>'; 
                    $html.='<td style="text-align: right; width: '.$col2.';"><p>Carta de antecedentes no penales</p></td>';
                    $html.='<td style="text-align: center; width: '.$col3.';">';
                        $html.='<input type="radio" id="radio11" name="ant_np" class="classRadio" value="0" '.$ant0.'/> ';
                        $html.='<label for="radio11">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='NO';
                        $html.='</label>';
                    $html.='</td>'; 
                    $html.='<td style="text-align: center; width: '.$col4.';">';
                        $html.='<input type="radio" id="radio12" name="ant_np" class="classRadio" value="1" '.$ant1.'/> ';
                        $html.='<label for="radio12">';
                            $html.='<span class="fa-stack">';
                                $html.='<i class="fa fa-circle-o fa-stack-2x"></i>';
                                $html.='<i class="fa fa-circle fa-stack-2x"></i>';
                            $html.='</span>';
                            //$html.='SI';
                        $html.='</label>';
                    $html.='</td>';
                    $html.='<td style="width: '.$col5.';">&nbsp;</td>'; 
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align: center; width: '.$col1.';">&nbsp;</td>';
                    $html.='<td style="text-align: right; width: '.$col2.';"><span class="styEstatus">Estatus:<span></td>';
                    //se verifica cual es el estatus del expediente al cargar la interfaz.
                    switch( $objExp->id_estatus ){
                        case 0:
                            $estatus = 'SIN DOCUMENTACION';
                            break;
                        case 1:
                            $estatus = 'COMPLETO';
                            break;
                        case 2:
                            $estatus = 'INCOMPLETO';
                            break;
                    }
                    $html.='<td style="text-align: center;" colspan="2" id="estatus">'.$estatus.'</td>';                    
                    $html.='<td style="text-align: center; width: '.$col5.';">&nbsp;</td>';
                $html.='</tr>';
                $html.='</tbody>';                                               
            $html.='</table>';
        $html.='</div>';                  
                 
        //datos necesarios para pasarlos por formulario                                                                  
        $html.='<input type="hidden" id="hdnUrlUpdExp"  value="' . $objSys->encrypt('log/loc/expediente/ajx_upd_exp.php') . '" />';
        $html.='<input type="hidden" id="hdnUrlInsExp"  value="' . $objSys->encrypt('log/loc/expediente/ajx_ins_exp.php') . '" />';
        $html.='<input type="hidden" id="hdnCurp"       value="' . $objSys->encrypt( $curp ) . '" />';
    $html .='</form>';

    // Formatea los datos y los envia al grid...
    $ajx_datos["titulo"] = utf8_encode($titulo);
    //se inyecta la informacion en los contenedores
    $ajx_datos["html"]  = utf8_encode($html);      
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>