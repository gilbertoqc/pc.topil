<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_exp_expediente.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario();        
    $objExp  = new LogtblExpExpediente();
    
    // Datos necesarios recibidos por post
    $documento  = $_POST["documento"];
    $valor      = $_POST["valor"];
    $curp       = $_POST["curp"];
    $objExp->select( $curp );  
    //se verifica que no se realice ninguna accion en caso de intentar establecer un valor que ya existe en la tabla
    //del expediente.                                                          
    if( $valor != $objExp->{$documento} ){
        //se verifica que la actualizacion del dato se realizo correctamente
        if ( $resp = $objExp->update( $documento, $valor ) ) {
            //se actualiza el valor del estatus
            if( $objExp->chkEstatus( $curp, 1 ) ){
                $objExp->updEstatus( $curp, 1 );
                $ajx_datos['estatus'] = 'COMPLETO';
            }elseif( $objExp->chkEstatus( $curp, 0 ) ){
                $objExp->updEstatus( $curp, 0 );
                $ajx_datos['estatus'] = 'SIN DOCUMENTACION';
            }else{
                $objExp->updEstatus( $curp, 2 );
                $ajx_datos['estatus'] = 'INCOMPLETO';
            }                 
            //se guarda la transaccion en el log del sisp
            $objSys->registroLog($objUsr->idUsr, 'logtbl_exp_expediente', $objExp->curp.'-'.$documento.'='.$valor, "Upd");
            $ajx_datos['rslt']  = true;                              
            $ajx_datos['error'] = '';
                
        } else {
            $ajx_datos['rslt']  = false;                
            $ajx_datos['error'] = $objExp->msjError;
        }
    }else{
        $ajx_datos['rslt']  = false;                
        $ajx_datos['error'] = $objExp->msjError;
    }
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;    
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>