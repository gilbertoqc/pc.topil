<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
include 'includes/class/logtbl_veh_mantenimientos.class.php';
include 'includes/class/logcat_veh_tipo_mantenimiento.class.php';
include 'includes/class/logcat_veh_tipo_servicio.class.php';
include 'includes/class/admcat_entidades.class.php';

$objVeh			= new LogtblVehiculos();
$objMtto		= new LogtblVehMantenimientos();
$objTipoMtto	= new LogcatVehTipoMantenimiento();
$objTipoServ	= new LogcatVehTipoServicio();
$objEnt			= new AdmcatEntidades();

//se obtiene el id del vehiculo enviado por get encryptado
if( !empty($_GET["id"]) ){
	$objMtto->select( $objSys->decrypt( $_GET["id"] ) );
}
//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="log/pvehicular/_js/talleres/mtto_edit.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
$url_cancel = "index.php?m=" . $_SESSION["xIdMenu"];
$urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('mtto_edit_rg');
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                </a>
                <a href="<?php echo $url_cancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                </a>
                </td>
            </tr>
        </table>
    </div>
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Edici&oacute;n de datos del mantenimiento</span>                                
            <div id="tabsForm" style="
            margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">                  
                       
                <!-- datos -->                                                                   
                <table id="tbForm-DatPer" class="tbForm-Data" style="width: 650px;"> 
                	<tr>
                        <td><label for="cbxTipoMtto">Tipo de mantenimiento:</label></td>
                        <td class="validation">
                            <select name="cbxTipoMtto" id="cbxTipoMtto">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipoMtto->shwTipoMtto( $objMtto->id_tipo_mtto );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                     
                    <tr>
                        <td><label for="cbxTipoServicio">Tipo de servicio:</label></td>
                        <td class="validation">
                            <select name="cbxTipoServicio" id="cbxTipoServicio">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipoServ->shwTipoServicio( $objMtto->id_tipo_servicio );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                	<tr>
                        <td><label for="txtFechaIni">Fecha de inicio:</label></td>
                        <td class="validation">
 <input type="text" name="txtFechaIni" id="txtFechaIni" value="<?php echo date('d/m/Y', strtotime($objMtto->fecha_ini)); ?>" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>                        
                    </tr>  
                    <tr>
                        <td><label for="txtFechaIni">Fecha de finalizaci&oacute;n:</label></td>
                        <td class="validation">
 <input type="text" name="txtFechaFin" id="txtFechaFin" value="<?php echo date('d/m/Y', strtotime($objMtto->fecha_fin)); ?>" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>                        
                    </tr>
                    <tr>
                        <td><label for="txtDetalles">Detalles:</label></td>
                        <td class="validation" >
                            <textarea name="txtDetalles" id="txtDetalles" rows="4" cols="49" title="..." style="resize: none;" /><?php echo $objMtto->detalles; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNomResp">Nombre del responsable:</label></td>
                        <td class="validation">
<input type="text" name="txtNomResp" id="txtNomResp" value="<?php echo $objMtto->nom_resp; ?>" title="..." maxlength="75" style="text-align: left; width: 350px;" />
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="txtRfcCurp">R.F.C. o C.U.R.P</label></td>
                        <td class="validation">
<input type="text" name="txtRfcCurp" id="txtRfcCurp" value="<?php echo $objMtto->rfc_curp; ?>" title="..." maxlength="18" style="text-align: left; width: 160px;" />
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="cbxColor">Entidad federativa:</label></td>
                        <td class="validation">
                            <select name="cbxEntidad" id="cbxEntidad">
                                <option value="0">&nbsp;</option>
                                <?php
                                $entidad = 12;
								//$objMun->select( $objDat->id_municipio );
                                echo $objEnt->shwEntidades( $entidad, 1 );
                                ?>
                            </select>
                        </td>
                    </tr>
                	<tr>
                        <td><label for="cbxMunicipio">Municipio:</label></td>
                        <td class="validation">
                            <select name="cbxMunicipio" id="cbxMunicipio">
                                <option value="0">&nbsp;</option>
                                <?php
                                    echo $objMtto->AdmcatMunicipios->shwMunicipios( $objMtto->id_municipio, $entidad );
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="txtDomicilio">Domicilio:</label></td>
                        <td class="validation">
<textarea name="txtDomicilio" id="txtDomicilio" rows="4" cols="49" title="..." maxlength="200" style="resize: none;" /><?php echo $objMtto->domicilio; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtRazonSocial">Raz&oacute;n social:</label></td>
                        <td class="validation">
<input type="text" name="txtRazonSocial" id="txtRazonSocial" value="<?php echo $objMtto->razon_social; ?>" title="..." maxlength="18" style="text-align: left; width: 350px;" />
                        </td>
                    </tr> 
                    <tr>
                        <td><label for="txtRespTaller">Responsable del taller:</label></td>
                        <td class="validation">
<input type="text" name="txtRespTaller" id="txtRespTaller" value="<?php echo $objMtto->resp_taller; ?>" title="..." maxlength="75" style="text-align: left; width: 350px;" />
                        </td>
                    </tr> 
                     <tr>
                        <td><label for="txtKmTotal">Kilometraje total:</label></td>
                        <td class="validation">
<input type="text" name="txtKmTotal" id="txtKmTotal" value="<?php echo $objMtto->kilometraje_total; ?>" title="..." maxlength="7" style="text-align: left; width: 60px;" />
                        </td>
                    </tr>                                              
                </table>
                                                  
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
  <input type="hidden" name="dtTypeOper" value="1" />
  <input type="hidden" name="hdnIdMtto" value="<?php echo $_GET["id"]; ?>" />
  <input type="hidden" name="dtTypeOper" value="<?php echo $e; ?>" />
  <input type="hidden" id="hdnUrlMarca" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_marcas.php');?>" />
  <input type="hidden" id="hdnUrlTipo" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_tipos.php');?>" />
    </form>
 
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>