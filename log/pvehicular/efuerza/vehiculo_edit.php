<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
include 'includes/class/logtbl_veh_datos.class.php';
include 'includes/class/logtbl_veh_detalles.class.php';
include 'includes/class/logcat_veh_operatividad.class.php';
include 'includes/class/logcat_veh_ubicacion.class.php';
include 'includes/class/logcat_veh_marca_llantas.class.php';
include 'includes/class/admcat_entidades.class.php';
include 'includes/class/logcat_veh_marca.class.php';
include 'includes/class/logcat_veh_clasificacion.class.php';

$objVeh			= new LogtblVehiculos();
$objDat 		= new LogtblVehDatos();
$objDet 		= new LogtblVehDetalles();
$objOper		= new LogcatVehOperatividad();
$objUbi 		= new LogcatVehUbicacion();
$objMarLl       = new LogcatVehMarcaLlantas();
$objEnt			= new AdmcatEntidades();
$objMar			= new LogcatVehMarca();
$objCla			= new LogcatVehClasificacion();


//se obtiene el id del vehiculo enviado por get encryptado
$objVeh->select( $objSys->decrypt( $_GET["id"] ) );
$objDat->select( $objSys->decrypt( $_GET["id"] ) );
$objDet->select( $objSys->decrypt( $_GET["id"] ) );

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                    '<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                    '<script type="text/javascript" src="includes/js/file_uploader/AjaxUpload.2.0.min.js"></script>',
                                    '<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_edit.js"></script>',
                                    '<style type="text/css">
                                    .labelRadioCond{                                         
                                        border: 1px solid #d2d2d1;
                                        border-radius: 4px;
                                        margin: 2px;
                                        padding: 3px 10px 2px 5px;
                                        vertical-align: top;
                                        display: block;
                                        width: 170px;                                        
                                    }
                                    .lblColor1{
                                        background-color: #ebebea;
                                    }
                                    .lblColor2{
                                        background-color: #ebebcc;
                                    }
                                    .chkrbnCond{
                                        width: 50px; text-align:center
                                    }
                                    .radioCond{
                                        width: 50px; text-align:center
                                    }
                                    .tituloTD{
                                        width: 150px !important;                                          
                                    }
                                    .bordeInfTD{
                                        border-bottom: 1px solid #A4A4A4;
                                    }
                                    </style>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
$url_cancel = "index.php?m=" . $_SESSION["xIdMenu"];
$urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('vehiculo_edit_rg');
    ?>
    <style type="text/css">
        .dvFoto{ border: 2px solid gray; cursor: pointer; height: 100px; margin: auto auto; width: 100px; }
        .dvFoto img{ height: 100px; width: 100px; }
    </style>
    
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 60%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 40%;">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display: none; width: 90px;" title="Validar y continuar...">
                    <img src="<?php echo PATH_IMAGES;?>icons/back241.png" alt="" style="border: none;" /><br />Anterior
                </a>                
                <a href="#" id="btnSiguiente" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Validar y continuar...">
                    <img src="<?php echo PATH_IMAGES;?>icons/next24.png" alt="" style="border: none;" /><br />Siguiente
                </a>
                <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del vehiculo...">
                    <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                </a>
                <a href="<?php echo $url_cancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                </a>
                </td>
            </tr>
        </table>
    </div>
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 900px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Registro</span>                                
            <div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
                <ul>
                    <li><a href="#tab-1" style="width: 130px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Caracteristicas</a></li>
                    <li><a href="#tab-2" style="width: 130px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Resguardo</a></li>
                    <li><a href="#tab-3" style="width: 130px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">3</span>Condiciones</a></li>
                    <li><a href="#tab-4" style="width: 130px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">4</span>Herramienta</a></li>
                    <li><a href="#tab-5" style="width: 130px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">5</span>Accesorios</a></li>
                </ul>  
                       
                <!-- caracteristicas -->
                <div id="tab-1">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                        	<tr>
                                <td><label for="cbxClasificacion">Clasificaci&oacute;n:</label></td>
                                <td class="validation">
                                    <select name="cbxClasificacion" id="cbxClasificacion">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objCla->shwClasificacion( $objCla->getClasificacion( $objVeh->id_tipo ) );
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="cbxMarca">Marca:</label></td>
                                <td class="validation">
                                    <select name="cbxMarca" id="cbxMarca">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objMar->shwMarca( $objMar->getIdMarca( $objVeh->id_tipo ) );
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>                     
                            <tr>
                                <td><label for="cbxCategoria">Tipo:</label></td>
                                <td class="validation">
                                    <select name="cbxTipo" id="cbxTipo">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objVeh->LogcatVehTipo->shwTipos( $objVeh->id_tipo );
                                        ?>
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtNumSerie">Serie:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNumSerie" id="txtNumSerie" value="<?php echo $objVeh->num_serie;?>" maxlength="20" title="..." style="width: 210px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtNumEco">N&uacute;mero econ&oacute;mico:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNumEco" id="txtNumEco" value="<?php echo $objVeh->num_economico;?>" maxlength="6" title="..." style="width: 70px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtNumMotor">N&uacute;mero de motor:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNumMotor" id="txtNumMotor" value="<?php echo $objVeh->num_motor;?>" maxlength="20" title="..." style="width: 210px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtPoliza">P&oacute;liza:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtPoliza" id="txtPoliza" value="<?php echo $objVeh->poliza;?>" maxlength="20" title="..." style="width: 210px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtInciso">Inciso:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtInciso" id="txtInciso" value="<?php echo $objVeh->inciso;?>" maxlength="20" title="..." style="width: 210px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtPlacas">Placas:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtPlacas" id="txtPlacas" value="<?php echo $objVeh->placas;?>" maxlength="10" title="..." style="width: 110px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label>N&uacute;mero de placas:</label></td>
                                <?php 
								//se valida el numero de placas.
								if( $objVeh->num_placas == 1 ){
									$checkedUNA = "checked";
									$checkedDOS = "";
								}else{									
									$checkedUNA = "";
									$checkedDOS = "checked";
								}
								?>
                                <td class="validation">
                                    <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnNumPlacas" id="rbnNumPlacas" value="1" <?php echo $checkedUNA ?>/>una</label>
                                    <label class="label-Radio"><input type="radio" name="rbnNumPlacas" id="rbnNumPlacas2" value="2" <?php echo $checkedDOS ?>/>dos</label>
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtTarjetaCir">Tarjeta de Circulaci&oacute;n:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtTarjetaCir" id="txtTarjetaCir" value="<?php echo $objVeh->tarjeta_circulacion;?>" size="12" maxlength="12" title="..." style="width: 130px;" />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtRegFedVeh">Registro federal vehicular:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtRegFedVeh" id="txtRegFedVeh" value="<?php echo $objVeh->reg_fed_veh;?>" size="20" maxlength="20" title="..." style="width: 200px;" />
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtNumPuertas">N&uacute;mero de puertas:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNumPuertas" id="txtNumPuertas" value="<?php echo $objVeh->num_puertas;?>" size="1" maxlength="1" title="..." style="text-align: center; width: 20px;" />
                                </td>
                            </tr>                        
                            <tr>
                                <td><label for="txtNumCilindros">N&uacute;mero de cilindros:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtNumCilindros" id="txtNumCilindros" value="<?php echo $objVeh->num_cilindros;?>" maxlength="2" title="..." style="text-align: center; width: 20px;" />
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="cbxEdoFisico">Estado f&iacute;sico:</label></td>
                                <td class="validation">
                                    <select name="cbxEdoFisico" id="cbxEdoFisico" title="">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objVeh->LogcatVehEstadoFisico->shwEstadoFisico( $objVeh->id_estado_fisico );
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxSituacion">Situaci&oacute;n:</label></td>
                                <td class="validation">
                                    <select name="cbxSituacion" id="cbxSituacion">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objVeh->LogcatVehSituacion->shwSituacion( $objVeh->id_situacion );
                                        ?>
                                    </select>
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="cbxModelo">Modelo:</label></td>
                                <td class="validation">
                                    <select name="cbxModelo" id="cbxModelo">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objVeh->LogcatVehModelo->shwModelo( $objVeh->id_modelo );
                                        ?>
                                    </select>
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="cbxTransmision">Transmisi&oacute;n:</label></td>
                                <td class="validation">
                                    <select name="cbxTransmision" id="cbxTransmision">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                       	echo $objVeh->LogcatVehTransmision->shwTransmision( $objVeh->id_transmision );
                                        ?>
                                    </select>
                                </td>
                            </tr>                                                                              
                            <tr>
                                <td><label for="cbxColor">Color:</label></td>
                                <td class="validation">
                                    <select name="cbxColor" id="cbxColor">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objVeh->LogcatVehColor->shwColor( $objVeh->id_color );
                                        ?>
                                    </select>
                                </td>
                            </tr>      
<?php
    $path_docs = 'log/pvehicular/fotos/';
    $foto_frente= ( !empty($objVeh->foto_frente) )   ? '<img src="' . $path_docs . $objVeh->foto_frente . '" alt="Foto de frente">'  : '';
    $foto_izq   = ( !empty($objVeh->foto_lat_izq) )   ? '<img src="' . $path_docs . $objVeh->foto_lat_izq . '" alt="Foto lateral izquierda">'  : '';
    $foto_der   = ( !empty($objVeh->foto_lat_der) )   ? '<img src="' . $path_docs . $objVeh->foto_lat_der . '" alt="Foto lateral derecha">'  : '';
    $foto_post  = ( !empty($objVeh->foto_posterior) )   ? '<img src="' . $path_docs . $objVeh->foto_posterior . '" alt="Foto posterior">'  : '';
    $foto_int   = ( !empty($objVeh->foto_interior) )   ? '<img src="' . $path_docs . $objVeh->foto_interior . '" alt="Foto interior">'  : '';
?>                                                  
                            <!-- imagenes -->
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr><td colspan="2" style="text-align: center;"><span style="font-weight: bold;">Fotografias del Vehiculo</span></td></tr>                             
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table style="border-width: 2px; border-style: solid;">
                                    <td style="width : 20%; text-align: center;">
                                        <span>Frente</span>
                                        <div id="dvFotoFrente" class="dvFoto"> 
                                            <?php echo $foto_frente; ?>                                                     
                                        </div>
                                        <input type="hidden" name="hdnFotoFrente" id="hdnFotoFrente" value="<?php echo $objVeh->foto_frente; ?>" />
                                    </td>
                                    <td style="width : 20%; text-align: center;">
                                        <span>Derecha</span>
                                        <div id="dvFotoDer" class="dvFoto">     
                                            <?php echo $foto_der; ?>                                                    
                                        </div>
                                        <input type="hidden" name="hdnFotoDer" id="hdnFotoDer" value="<?php echo $objVeh->foto_lat_der; ?>" />
                                    </td>
                                    <td style="width : 20%; text-align: center;">
                                        <span>Izquierda</span>
                                        <div id="dvFotoIzq" class="dvFoto"> 
                                            <?php echo $foto_izq; ?>                                                          
                                        </div>
                                        <input type="hidden" name="hdnFotoIzq" id="hdnFotoIzq" value="<?php echo $objVeh->foto_lat_izq; ?>" />
                                    </td>
                                    <td style="width : 20%; text-align: center;">
                                        <span>Posterior</span>
                                        <div id="dvFotoPost" class="dvFoto">
                                            <?php echo $foto_post; ?>                                                              
                                        </div>
                                        <input type="hidden" name="hdnFotoPost" id="hdnFotoPost" value="<?php echo $objVeh->foto_posterior; ?>" />
                                    </td>                      
                                    <td style="width : 20%; text-align: center;">
                                        <span>Interior</span>
                                        <div id="dvFotoInt" class="dvFoto">    
                                            <?php echo $foto_int; ?>                                                           
                                        </div>
                                        <input type="hidden" name="hdnFotoInt" id="hdnFotoInt" value="<?php echo $objVeh->foto_interior; ?>" />
                                    </td>
                                    </table>
                                </td>
                            </tr>                               
                        </table>
                    </fieldset>
                </div>
                
                <!-- resguardo del vehiculo -->
                <div id="tab-2">
                    <fieldset class="fsetForm-Data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="txtNombre">Nombre(s):</label></td>
                                <td class="validation">
                                    <input type="hidden" id="hdnCurp" name="hdnCurp" value="<?php echo $objDat->curp; ?>" />
<?php 
    $objAux = $objDat->AdmtblDatosPersonales;
    $nombre_completo= $objAux->nombre . " " . $objAux->a_paterno . " " . $objAux->a_materno;
?>                                     
                                    <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $nombre_completo;?>" maxlength="40" style="width: 380px;" readonly="true"/>
                                    <!-- en este paso se abre la ventana emergente de busqueda -->
                                    <a href="#" class="classBuscar" title="Buscar un elemento ...">
                                        <img src="<?php echo PATH_IMAGES;?>icons/search16.png" alt="" style="border: none;" />
                                     </a>
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="cbxColor">Operatividad:</label></td>
                                <td class="validation">
                                    <select name="cbxOperatividad" id="cbxOperatividad">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objDat->LogcatVehOperatividad->shwOperatividad( $objDat->id_operatividad );
                                        ?>
                                    </select>
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="cbxColor">Entidad federativa:</label></td>
                                <td class="validation">
                                    <select name="cbxEntidad" id="cbxEntidad">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        $entidad = empty( $objDat->AdmcatMunicipios->id_entidad ) ? 12 : $objDat->AdmcatMunicipios->id_entidad;
										$objDat->AdmcatMunicipios->select( $objDat->id_municipio );
                                        echo $objEnt->shwEntidades( $entidad, 1 );
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxColor">Municipio o delegaci&oacute;n:</label></td>
                                <td class="validation">
                                    <select name="cbxMunicipio" id="cbxMunicipio">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objDat->AdmcatMunicipios->shwMunicipios( $objDat->id_municipio, $entidad );
                                        ?>
                                    </select>
                                </td>
                            </tr>                            
                            <tr>
                                <td><label for="cbxColor">Ubicaci&oacute;n:</label></td>
                                <td class="validation">
                                    <select name="cbxUbicacion" id="cbxUbicacion">
                                        <option value="0">&nbsp;</option>
                                        <?php
                                        echo $objUbi->shwUbicacion( $objDat->id_ubicacion );
                                        ?>
                                    </select>
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="txtFechaRes">Fecha de resguardo:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaRes" id="txtFechaRes" value="<?php echo date('d/m/Y', strtotime($objDat->fecha_res)); ?>" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                </td>
                            </tr>                            
                        </table>
                    </fieldset>
                </div>
                
                <!-- condiciones del vehiculo -->
                <div id="tab-3">
                    <fieldset class="fsetForm-Data">
                    	
                        <table class="tbForm-Data">
                        	<tr>
                                <td><label for="txtKm">Kilometraje:</label></td>
                                <td class="validation" style="padding-left: 10px;">
                                    <input type="text" name="txtKm" id="txtKm" value="<?php echo $objDet->kilometraje; ?>" maxlength="7" title="..." style="text-align: center; width: 90px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tituloTD"><label for="txtEspejos">Espejos:</label></td>
                                <?php
									//se validan los espejos
									$espejo_der = $objDet->espejo_der == 1 ? "checked" : "";
									$espejo_izq = $objDet->espejo_izq == 1 ? "checked" : "";
									switch( $objDet->espejo_estado ){
										case "":
											$espejos_bue = "";
											$espejos_mal = "";
											break;
										case "0":																
											$espejos_bue = "";
											$espejos_mal = "checked";
											break;
										case "1":
											$espejos_bue = "checked";
											$espejos_mal = "";
											break;
									}
                                ?>
                                
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>                                  		
                                        <label class="labelRadioCond lblColor1">
   <input type="checkbox" name="chkEspejoDer" id="chkEspejoDer" value="1" title="..." class="chkrbnCond" <?php echo $espejo_der; ?>/>Derecho</label>
                                        </td>
                                        <td>
                                        <label class="labelRadioCond lblColor1">
   <input type="checkbox" name="chkEspejoIzq" id="chkEspejoIzq" value="1" title="..." class="chkrbnCond" <?php echo $espejo_izq; ?>/>Izquierdo</label>
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                            <label class="labelRadioCond lblColor1">
   <input type="radio" name="rbnEspejosEst" id="rbnEspejoBue" value="1" title="..." class="chkrbnCond" <?php echo $espejos_bue; ?>/>Buen estado</label>
                                        </td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1">
   <input type="radio" name="rbnEspejosEst" id="rbnEspejoMal" value="0" title="..." class="chkrbnCond" <?php echo $espejos_mal; ?>/>Mal estado</label>                                        
                                    	</td>
                                    <tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tituloTD"><label for="txtCuartos">Cuartos:</label></td>
                                <?php
									//se validan los cuartos
									$cuarto_der = $objDet->cuarto_der == 1 ? "checked" : "";
									$cuarto_izq = $objDet->cuarto_izq == 1 ? "checked" : "";
									switch( $objDet->cuarto_estado ){
										case "":
											$cuartos_bue = "";
											$cuartos_mal = "";
											break;
										case "0":																
											$cuartos_bue = "";
											$cuartos_mal = "checked";
											break;
										case "1":
											$cuartos_bue = "checked";
											$cuartos_mal = "";
											break;
									}
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>
                                    		<label class="labelRadioCond lblColor2" >
   <input type="checkbox" name="chkCuartoDer" id="chkCuartoDer" value="1" title="..." class="chkrbnCond" <?php echo $cuarto_der; ?>/>Derecho</label>
                                        </td>
                                        <td>
                                        	<label class="labelRadioCond lblColor2" >
   <input type="checkbox" name="chkCuartoIzq" id="chkCuartoIzq" value="1" title="..." class="chkrbnCond" <?php echo $cuarto_izq; ?>/>Izquierdo</label>
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                        	<label class="labelRadioCond lblColor2" >
   <input type="radio" name="rbnCuartosEst" id="rbnCuartoBue" value="1" title="..." class="chkrbnCond" <?php echo $cuartos_bue; ?>/>Buen estado</label>
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor2">
   <input type="radio" name="rbnCuartosEst" id="rbnCuartoMal" value="0" title="..." class="chkrbnCond" <?php echo $cuartos_mal; ?>/>Mal estado</label>
                                    	</td>
                                    <tr>                                    
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tituloTD"><label for="txtFanales">Fanales:</label></td>
                                <?php
									//se validan los fanales
									$fanal_der = $objDet->fanal_der == 1 ? "checked" : "";
									$fanal_izq = $objDet->fanal_izq == 1 ? "checked" : "";
									switch( $objDet->fanal_estado ){
										case "":
											$fanal_bue = "";
											$fanal_mal = "";
											break;
										case "0":																
											$fanal_bue = "";
											$fanal_mal = "checked";
											break;
										case "1":
											$fanal_bue = "checked";
											$fanal_mal = "";
											break;
									}
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>
                                        	<label class="labelRadioCond lblColor1" >
   <input type="checkbox" name="chkFanalDer" id="chkFanalDer" value="1" title="..." class="chkrbnCond" <?php echo $fanal_der; ?>/>Derecho</label>
                                        </td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1" >
   <input type="checkbox" name="chkFanalIzq" id="chkFanalIzq" value="1" title="..." class="chkrbnCond" <?php echo $fanal_izq; ?>/>Izquierdo</label>
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                        	<label class="labelRadioCond lblColor1" >
<input type="radio" name="rbnFanalesEst" id="rbnFanalBue" value="1" title="..." class="chkrbnCond" <?php echo $fanal_bue; ?>/>Buen estado</label>   
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1">
<input type="radio" name="rbnFanalesEst" id="rbnFanalMal" value="0" title="..." class="chkrbnCond" <?php echo $fanal_mal; ?>/>Mal estado</label>   
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr> 
                            <tr>
                                <td class="tituloTD"><label for="txtCalavera">Calavera:</label></td>
                                <?php
									//se validan las calaveras
									$calavera_der = $objDet->calavera_der == 1 ? "checked" : "";
									$calavera_izq = $objDet->calavera_izq == 1 ? "checked" : "";
									switch( $objDet->calavera_estado ){
										case "":
											$calavera_bue = "";
											$calavera_mal = "";
											break;
										case "0":																
											$calavera_bue = "";
											$calavera_mal = "checked";
											break;
										case "1":
											$calavera_bue = "checked";
											$calavera_mal = "";
											break;
									}
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>
                                        	<label class="labelRadioCond lblColor2">
   <input type="checkbox" name="chkCalaveraDer" id="chkCalaveraDer" value="1" title="..." class="chkrbnCond" <?php echo $calavera_der; ?>/>Derecho</label>
                                        </td>
                                        <td>
                                        	<label class="labelRadioCond lblColor2">
   <input type="checkbox" name="chkCalaveraIzq" id="chkCalaveraIzq" value="1" title="..." class="chkrbnCond" <?php echo $calavera_izq; ?>/>Izquierdo</label>
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                        	<label class="labelRadioCond lblColor2" >
   <input type="radio" name="rbnCalaverasEst" id="rbnCalaveraBue" value="1" title="..." class="chkrbnCond" <?php echo $calavera_bue; ?>/>Buen estado</label>
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor2">
   <input type="radio" name="rbnCalaverasEst" id="rbnCalaveraMal" value="0" title="..." class="chkrbnCond" <?php echo $calavera_mal; ?>/>Mal estado</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tituloTD"><label for="txtMolduras">Moldura:</label></td>
                                <?php
									//se validan las calaveras
									$moldura_der = $objDet->moldura_der == 1 ? "checked" : "";
									$moldura_izq = $objDet->moldura_izq == 1 ? "checked" : "";
									$moldura_del = $objDet->moldura_del == 1 ? "checked" : "";
									$moldura_tras= $objDet->moldura_tras == 1 ? "checked" : "";																	
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>
                                        	<label class="labelRadioCond lblColor1">
   <input type="checkbox" name="chkMolduraDer" id="chkMolduraDer" value="1" title="..." class="chkrbnCond" <?php echo $moldura_der; ?>/>Derecho</label>
                                        </td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1" >
   <input type="checkbox" name="chkMolduraIzq" id="chkMolduraIzq" value="1" title="..." class="chkrbnCond" <?php echo $moldura_izq; ?>/>Izquierdo</label>   
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                        	<label class="labelRadioCond lblColor1" >
	<input type="checkbox" name="chkMolduraDel" id="chkMolduraDel" value="1" title="..." class="chkrbnCond" <?php echo $moldura_del; ?>/>Delantera</label>
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1" >
    <input type="checkbox" name="chkMolduraTras" id="chkMolduraTras" value="1" title="..." class="chkrbnCond" <?php echo $moldura_tras; ?>/>Trasera</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>  
                            <tr>
                                <td class="tituloTD"><label for="txtEmblema">Emblema:</label></td>
                                <?php
									//se validan las calaveras
									$emblema_der = $objDet->emblema_der == 1 ? "checked" : "";
									$emblema_izq = $objDet->emblema_izq == 1 ? "checked" : "";
									$emblema_del = $objDet->emblema_del == 1 ? "checked" : "";
									$emblema_tras= $objDet->emblema_tras == 1 ? "checked" : "";																	
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>
                                        	<label class="labelRadioCond lblColor2">
   <input type="checkbox" name="chkEmblemaDer" id="chkEmblemaDer" value="1" title="..." class="chkrbnCond" <?php echo $emblema_der; ?>/>Derecho</label>   
                                        </td>
                                        <td>
                                        	<label class="labelRadioCond lblColor2">
   <input type="checkbox" name="chkEmblemaIzq" id="chkEmblemaIzq" value="1" title="..." class="chkrbnCond" <?php echo $emblema_izq; ?>/>Izquierdo</label>   
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                        	<label class="labelRadioCond lblColor2">
   <input type="checkbox" name="chkEmblemaDel" id="chkEmblemaDel" value="1" title="..." class="chkrbnCond" <?php echo $emblema_del; ?>/>Delantera</label>   
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor2">
<input type="checkbox" name="chkEmblemaTras" id="chkEmblemaTras" value="1" title="..." class="chkrbnCond" <?php echo $emblema_tras; ?>/>Trasera</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tituloTD"><label for="txtPintura">Pintura:</label></td>
                                <?php
								//se verifica el valor de la pintura.
                                switch( $objDet->pintura ){
										case "":
											$pintura_bue = "";
											$pintura_mal = "";
											$pintura_reg = "";
											break;
										case "0":																
											$pintura_bue = "";
											$pintura_mal = "checked";
											$pintura_reg = "";
											break;
										case "1":
											$pintura_bue = "checked";
											$pintura_mal = "";
											$pintura_reg = "";
											break;
										case "2":
											$pintura_bue = "";
											$pintura_mal = "";
											$pintura_reg = "checked";
											break;
									}
								?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                        	<label class="labelRadioCond lblColor1" style="width: 130px !important;">
   <input type="radio" name="rbnPinturaEst" id="rbnPinturaBue" value="1" title="..." class="chkrbnCond" <?php echo $pintura_bue; ?>/>                                            Buena</label>
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1" style="width: 130px !important;">
   <input type="radio" name="rbnPinturaEst" id="rbnPinturaMal" value="0" title="..." class="chkrbnCond" <?php echo $pintura_mal; ?>/>                                            Mala</label>
                                    	</td>
                                        <td>
                                        	<label class="labelRadioCond lblColor1" style="width: 130px !important;">
   <input type="radio" name="rbnPinturaEst" id="rbnPinturaReg" value="2" title="..." class="chkrbnCond" <?php echo $pintura_reg; ?>/>
                                    		Regular</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>   
                            <tr>
                                <td class="tituloTD"><label for="txtFarosAntN">Faros antiniebla:</label></td>
                                <?php
								//se verifica el valor de los faron antiniebla
                                switch( $objDet->faros_antiniebla ){
										case "":
											$faros_antiniebla_si = "";
											$faros_antiniebla_no = "";
											break;
										case "0":																
											$faros_antiniebla_si = "";
											$faros_antiniebla_no = "checked";
											break;
										case "1":
											$faros_antiniebla_si = "checked";
											$faros_antiniebla_no = "";
											break;
									}
								?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnFarosAntNSI" class="labelRadioCond lblColor2" >                                        
   <input type="radio" name="rbnFarosAntN" id="rbnFarosAntNSI" value="1" title="..." class="radioCond" <?php echo $faros_antiniebla_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnFarosAntNNO" class="labelRadioCond lblColor2" >
   <input type="radio" name="rbnFarosAntN" id="rbnFarosAntNNO" value="0" title="..." class="radioCond" <?php echo $faros_antiniebla_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>  
                            <tr>
                                <td class="tituloTD"><label for="txtLimpiadores">Limpiadores:</label></td>
                                 <?php
									//se validan los limpiadores
									switch( $objDet->limpiadores ){
										case "":
											$limpiadores_si = "";
											$limpiadores_no = "";
											break;
										case "0":																
											$limpiadores_si = "";
											$limpiadores_no = "checked";
											break;
										case "1":
											$limpiadores_si = "checked";
											$limpiadores_no = "";
											break;
									}
									switch( $objDet->limpiadores_estado ){
										case "":
											$limpiadores_estado_bue = "";
											$limpiadores_estado_mal = "";
											break;
										case "0":																
											$limpiadores_estado_bue = "";
											$limpiadores_estado_mal = "checked";
											break;
										case "1":
											$limpiadores_estado_bue = "checked";
											$limpiadores_estado_mal = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                    	<td>
                                    		<label for="rbnLimpiadorSi" class="labelRadioCond lblColor1" >
   <input type="radio" name="rbnLimpiadores" id="rbnLimpiadorSi" value="1" title="..." class="radioCond" <?php echo $limpiadores_si; ?>/>Si</label>
                                        </td>
                                        <td>
                  				            <label for="rbnLimpiadorNo" class="labelRadioCond lblColor1" >
   <input type="radio" name="rbnLimpiadores" id="rbnLimpiadorNo" value="0" title="..." class="radioCond" <?php echo $limpiadores_no; ?>/>No</label>
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                    		<label for="rbnLimpiadorBue" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnLimpiadorEst" id="rbnLimpiadorBue" value="1" title="..." class="radioCond" <?php echo $limpiadores_estado_bue; ?>/>Buen estado</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnLimpiadorMal" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnLimpiadorEst" id="rbnLimpiadorMal" value="0" title="..." class="radioCond" <?php echo $limpiadores_estado_mal; ?>/>Mal estado</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>                  
                            <tr>
                                <td class="tituloTD"><label for="txtEspejoInt">Espejo Interior:</label></td>
                                <?php
									//se validan los limpiadores
									switch( $objDet->espejo_interior ){
										case "":
											$espejo_interior_si = "";
											$espejo_interior_no = "";
											break;
										case "0":																
											$espejo_interior_si = "";
											$espejo_interior_no = "checked";
											break;
										case "1":
											$espejo_interior_si = "checked";
											$espejo_interior_no = "";
											break;
									}
									switch( $objDet->espejo_interior_estado ){
										case "":
											$espejo_interior_estado_bie = "";
											$espejo_interior_estado_mal = "";
											break;
										case "0":																
											$espejo_interior_estado_bue = "";
											$espejo_interior_estado_mal = "checked";
											break;
										case "1":
											$espejo_interior_estado_bue = "checked";
											$espejo_interior_estado_mal = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table >
                                    <tr>
                                    	<td>
                                    		<label for="rbnEspejoIntSI" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnEspejoInt" id="rbnEspejoIntSI" value="1" title="..." class="radioCond" <?php echo $espejo_interior_si; ?>/>Si</label>
                                        </td>
                                        <td>
                  				            <label for="rbnLimpiadorNO" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnEspejoInt" id="rbnEspejoIntNO" value="0" title="..." class="radioCond" <?php echo $espejo_interior_no; ?>/>No</label>
                                		</td>
                            		</tr>
                            		<tr>
                                		<td class="validation">
                                    		<label for="rbnEspejoIntBue" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnEspejoIntEst" id="rbnEspejoIntBue" value="1" title="..." class="radioCond" <?php echo $espejo_interior_estado_bue; ?>/>Buen estado</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnEspejoIntMal" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnEspejoIntEst" id="rbnEspejoIntMal" value="0" title="..." class="radioCond" <?php echo $espejo_interior_estado_mal; ?>/>Mal estado</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtEncendedor">Encendedor:</label></td>
                                <?php
									//se validan los limpiadores
									switch( $objDet->encendedor ){
										case "":
											$encendedor_si = "";
											$encendedor_no = "";
											break;
										case "0":																
											$encendedor_si = "";
											$encendedor_no = "checked";
											break;
										case "1":
											$encendedor_si = "checked";
											$encendedor_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnEncendedorSI" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnEncendedor" id="rbnEncendedorSI" value="1" title="..." class="radioCond" <?php echo $encendedor_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnEncendedorNO" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnEncendedor" id="rbnEncendedorNO" value="0" title="..." class="radioCond" <?php echo $encendedor_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCinturon">Cintur&oacute;n:</label></td>
                                <?php
									//se validan el cinturon
									switch( $objDet->cinturon ){
										case "":
											$cinturon_si = "";
											$cinturon_no = "";
											break;
										case "0":																
											$cinturon_si = "";
											$cinturon_no = "checked";
											break;
										case "1":
											$cinturon_si = "checked";
											$cinturon_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnCinturonSI" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnCinturon" id="rbnCinturonSI" value="1" title="..." class="radioCond" <?php echo $cinturon_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnCinturonNO" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnCinturon" id="rbnCinturonNO" value="0" title="..." class="radioCond" <?php echo $cinturon_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr> 
                            <tr>
                                <td><label for="txtTapetes">Tapetes:</label></td>
                                <?php
									//se validan el cinturon
									switch( $objDet->tapetes ){
										case "":
											$tapetes_si = "";
											$tapetes_no = "";
											break;
										case "0":																
											$tapetes_si = "";
											$tapetes_no = "checked";
											break;
										case "1":
											$tapetes_si = "checked";
											$tapetes_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnTapetesSI" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnTapetes" id="rbnTapetesSI" value="1" title="..." class="radioCond" <?php echo $tapetes_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnTapetesNO" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnTapetes" id="rbnTapetesNO" value="0" title="..." class="radioCond" <?php echo $tapetes_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtCD">Disco compacto:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->disco_compacto ){
										case "":
											$disco_compacto_si = "";
											$disco_compacto_no = "";
											break;
										case "0":																
											$disco_compacto_si = "";
											$disco_compacto_no = "checked";
											break;
										case "1":
											$disco_compacto_si = "checked";
											$disco_compacto_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnCDSI" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnCD" id="rbnCDSI" value="1" title="..." class="radioCond" <?php echo $disco_compacto_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnCDNO" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnCD" id="rbnCDNO" value="0" title="..." class="radioCond" <?php echo $disco_compacto_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtAntena">Antena:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->antena ){
										case "":
											$antena_si = "";
											$antena_no = "";
											break;
										case "0":																
											$antena_si = "";
											$antena_no = "checked";
											break;
										case "1":
											$antena_si = "checked";
											$antena_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnAntenaSI"class="labelRadioCond lblColor1">
   <input type="radio" name="rbnAntena" id="rbnAntenaSI" value="1" title="..." class="radioCond" <?php echo $antena_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnAntenaNO" class="labelRadioCond lblColor1">                                        
   <input type="radio" name="rbnAntena" id="rbnAntenaNO" value="0" title="..." class="radioCond" <?php echo $antena_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtManijas">Manijas:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->manijas ){
										case "":
											$manijas_si = "";
											$manijas_no = "";
											break;
										case "0":																
											$manijas_si = "";
											$manijas_no = "checked";
											break;
										case "1":
											$manijas_si = "checked";
											$manijas_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnManijasSI" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnManijas" id="rbnManijasSI" value="1" title="..." class="radioCond" <?php echo $manijas_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnManijasNO" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnManijas" id="rbnManijasNO" value="0" title="..." class="radioCond" <?php echo $manijas_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCenicero">Cenicero:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->cenicero ){
										case "":
											$cenicero_si = "";
											$cenicero_no = "";
											break;
										case "0":																
											$cenicero_si = "";
											$cenicero_no = "checked";
											break;
										case "1":
											$cenicero_si = "checked";
											$cenicero_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnCeniceroSI" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnCenicero" id="rbnCeniceroSI" value="1" title="..." class="radioCond" <?php echo $cenicero_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnCeniceroNO" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnCenicero" id="rbnCeniceroNO" value="0" title="..." class="radioCond" <?php echo $cenicero_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtClaxon">Claxon:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->claxon ){
										case "":
											$claxon_si = "";
											$claxon_no = "";
											break;
										case "0":																
											$claxon_si = "";
											$claxon_no = "checked";
											break;
										case "1":
											$claxon_si = "checked";
											$claxon_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnClaxonSI" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnClaxon" id="rbnClaxonSI" value="1" title="..." class="radioCond" <?php echo $claxon_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnClaxonNO" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnClaxon" id="rbnClaxonNO" value="0" title="..." class="radioCond" <?php echo $claxon_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtRadio">Radio AM&frasl;FM:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->radio_amfm ){
										case "":
											$radio_amfm_si = "";
											$radio_amfm_no = "";
											break;
										case "0":																
											$radio_amfm_si = "";
											$radio_amfm_no = "checked";
											break;
										case "1":
											$radio_amfm_si = "checked";
											$radio_amfm_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnRadioSI" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnRadio" id="rbnRadioSI" value="1" title="..." class="radioCond" <?php echo $radio_amfm_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnRadioNO" class="labelRadioCond lblColor1">
   <input type="radio" name="rbnRadio" id="rbnRadioNO" value="0" title="..." class="radioCond" <?php echo $radio_amfm_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtVestiduras">Vestiduras:</label></td>
                                <?php
									//se valida el disco compacto
									switch( $objDet->vestiduras ){
										case "":
											$vestiduras_si = "";
											$vestiduras_no = "";
											break;
										case "0":																
											$vestiduras_si = "";
											$vestiduras_no = "checked";
											break;
										case "1":
											$vestiduras_si = "checked";
											$vestiduras_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table class="bordeInfTD">
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnVestidurasSI" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnVestiduras" id="rbnVestidurasSI" value="1" title="..." class="radioCond" <?php echo $vestiduras_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnVestidurasNO" class="labelRadioCond lblColor2">
   <input type="radio" name="rbnVestiduras" id="rbnVestidurasNO" value="0" title="..." class="radioCond" <?php echo $vestiduras_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtOtros">Otros:</label></td>
                                <td class="validation" style="padding-left: 10px;">
                                    <textarea name="txtOtros" id="txtOtros" rows="4" cols="49" title="..." style="resize: none;" /><?php echo $objDet->otros; ?></textarea>
                                </td>
							</tr>
                           </table>
                    </fieldset>
                </div> 
                
                <!-- herramientas vehiculo -->
                <div id="tab-4">
                    <fieldset class="fsetForm-Data">
                        <table class="tbForm-Data">
                            <tr>
                                <td><label for="txtGato">Gato:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->gato ){
										case "":
											$gato_si = "";
											$gato_no = "";
											break;
										case "0":																
											$gato_si = "";
											$gato_no = "checked";
											break;
										case "1":
											$gato_si = "checked";
											$gato_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnGatoSI" class="labelRadioCond lblColor1" style="width: 100px !important;">                                        
   <input type="radio" name="rbnGato" id="rbnGatoSI" value="1" title="..." class="radioCond" <?php echo $gato_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnGatoNO" class="labelRadioCond lblColor1" style="width: 100px !important;">                                        
   <input type="radio" name="rbnGato" id="rbnGatoNO" value="0" title="..." class="radioCond" <?php echo $gato_no; ?> />No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtManeral">Maneral:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->maneral ){
										case "":
											$maneral_si = "";
											$maneral_no = "";
											break;
										case "0":																
											$maneral_si = "";
											$maneral_no = "checked";
											break;
										case "1":
											$maneral_si = "checked";
											$maneral_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnManeralSI" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnManeral" id="rbnManeralSI" value="1" title="..." class="radioCond" <?php echo $maneral_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnManeralNO" class="labelRadioCond lblColor2" style="width: 100px !important;">                                      
   <input type="radio" name="rbnManeral" id="rbnManeralNO" value="0" title="..." class="radioCond" <?php echo $maneral_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txReflejantes">Reflejantes:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->reflejantes ){
										case "":
											$reflejantes_si = "";
											$reflejantes_no = "";
											break;
										case "0":																
											$reflejantes_si = "";
											$reflejantes_no = "checked";
											break;
										case "1":
											$reflejantes_si = "checked";
											$reflejantes_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnReflejantesSI" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnReflejantes" id="rbnReflejantesSI" value="1" title="..." class="radioCond" <?php echo $reflejantes_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnReflejantesNO" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnReflejantes" id="rbnReflejantesNO" value="0" title="..." class="radioCond" <?php echo $reflejantes_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtCable">Cable para corriente:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->cable_pc ){
										case "":
											$cable_pc_si = "";
											$cable_pc_no = "";
											break;
										case "0":																
											$cable_pc_si = "";
											$cable_pc_no = "checked";
											break;
										case "1":
											$cable_pc_si = "checked";
											$cable_pc_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnCableSI" class="labelRadioCond lblColor2" style="width: 100px !important;">
    <input type="radio" name="rbnCable" id="rbnCableSI" value="1" title="..." class="radioCond" <?php echo $cable_pc_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnCableNO" class="labelRadioCond lblColor2" style="width: 100px !important;">                                        
   <input type="radio" name="rbnCable" id="rbnCableNO" value="0" title="..." class="radioCond" <?php echo $cable_pc_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtLlavees">Llave espa&ntilde;ola:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->llave_espanola ){
										case "":
											$llave_espanola_si = "";
											$llave_espanola_no = "";
											break;
										case "0":																
											$llave_espanola_si = "";
											$llave_espanola_no = "checked";
											break;
										case "1":
											$llave_espanola_si = "checked";
											$llave_espanola_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnLlaveesSI" class="labelRadioCond lblColor1" style="width: 100px !important;">                                        
   <input type="radio" name="rbnLlavees" id="rbnLlaveesSI" value="1" title="..." class="radioCond" <?php echo $llave_espanola_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnLlaveesNO" class="labelRadioCond lblColor1" style="width: 100px !important;">                                        
   <input type="radio" name="rbnLlavees" id="rbnLlaveesNO" value="0" title="..." class="radioCond" <?php echo $llave_espanola_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtDesPlano">Desarmador plano:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->desarmador_plano ){
										case "":
											$desarmador_plano_si = "";
											$desarmador_plano_no = "";
											break;
										case "0":																
											$desarmador_plano_si = "";
											$desarmador_plano_no = "checked";
											break;
										case "1":
											$desarmador_plano_si = "checked";
											$desarmador_plano_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnDesPlanoSI" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnDesPlano" id="rbnDesPlanoSI" value="1" title="..." class="radioCond" <?php echo $desarmador_plano_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnDesPlanoNO" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnDesPlano" id="rbnDesPlanoNO" value="0" title="..." class="radioCond" <?php echo $desarmador_plano_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtExtintor">Extintor:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->extintor ){
										case "":
											$extintor_si = "";
											$extintor_no = "";
											break;
										case "0":																
											$extintor_si = "";
											$extintor_no = "checked";
											break;
										case "1":
											$extintor_si = "checked";
											$extintor_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnExtintorSI"class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnExtintor" id="rbnExtintorSI" value="1" title="..." class="radioCond" <?php echo $extintor_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnExtintorNO" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnExtintor" id="rbnExtintorNO" value="0" title="..." class="radioCond" <?php echo $extintor_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtLlaveCruz">Llave de cruz:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->llave_cruz ){
										case "":
											$llave_cruz_si = "";
											$llave_cruz_no = "";
											break;
										case "0":																
											$llave_cruz_si = "";
											$llave_cruz_no = "checked";
											break;
										case "1":
											$llave_cruz_si = "checked";
											$llave_cruz_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnLlaveCruzSI" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnLlaveCruz" id="rbnLlaveCruzSI" value="1" title="..." class="radioCond" <?php echo $llave_cruz_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnLlaveCruzNO" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnLlaveCruz" id="rbnLlaveCruzNO" value="0" title="..." class="radioCond" <?php echo $llave_cruz_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtLlavel">Llave L:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->llave_l ){
										case "":
											$llave_l_si = "";
											$llave_l_no = "";
											break;
										case "0":																
											$llave_l_si = "";
											$llave_l_no = "checked";
											break;
										case "1":
											$llave_l_si = "checked";
											$llave_l_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnLlavelSI"class="labelRadioCond lblColor1" style="width: 100px !important;">                                        
   <input type="radio" name="rbnLlavel" id="rbnLlavelSI" value="1" title="..." class="radioCond" <?php echo $llave_l_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnLlavelNO" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnLlavel" id="rbnLlavelNO" value="0" title="..." class="radioCond" <?php echo $llave_l_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtPinzas">Pinzas:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->pinzas ){
										case "":
											$pinzas_si = "";
											$pinzas_no = "";
											break;
										case "0":																
											$pinzas_si = "";
											$pinzas_no = "checked";
											break;
										case "1":
											$pinzas_si = "checked";
											$pinzas_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnPinzasSI" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnPinzas" id="rbnPinzasSI" value="1" title="..." class="radioCond" <?php echo $pinzas_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnPinzasNO" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnPinzas" id="rbnPinzasNO" value="0" title="..." class="radioCond" <?php echo $pinzas_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtLlaveAllen">Llave allen:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->llave_allen ){
										case "":
											$llave_allen_si = "";
											$llave_allen_no = "";
											break;
										case "0":																
											$llave_allen_si = "";
											$llave_allen_no = "checked";
											break;
										case "1":
											$llave_allen_si = "checked";
											$llave_allen_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnLlaveAllenSI" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnLlaveAllen" id="rbnLlaveAllenSI" value="1" title="..." class="radioCond" <?php echo $llave_allen_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnLlaveAllenNO" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnLlaveAllen" id="rbnLlaveAllenNO" value="0" title="..." class="radioCond" <?php echo $llave_allen_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtDesCruz">Desarmador de cruz:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->desarmador_cruz ){
										case "":
											$desarmador_cruz_si = "";
											$desarmador_cruz_no = "";
											break;
										case "0":																
											$desarmador_cruz_si = "";
											$desarmador_cruz_no = "checked";
											break;
										case "1":
											$desarmador_cruz_si = "checked";
											$desarmador_cruz_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnDesCruzSI" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnDesCruz" id="rbnDesCruzSI" value="1" title="..." class="radioCond" <?php echo $desarmador_cruz_si; ?> />Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnDesCruzNO" class="labelRadioCond lblColor2" style="width: 100px !important;">
   <input type="radio" name="rbnDesCruz" id="rbnDesCruzNO" value="0" title="..." class="radioCond" <?php echo $desarmador_cruz_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtLlaveBujia">Llave buj&iacute;a:</label></td>
                                <?php
									//se valida el gato
									switch( $objDet->llave_bujia ){
										case "":
											$llave_bujia_si = "";
											$llave_bujia_no = "";
											break;
										case "0":																
											$llave_bujia_si = "";
											$llave_bujia_no = "checked";
											break;
										case "1":
											$llave_bujia_si = "checked";
											$llave_bujia_no = "";
											break;
									}									
                                ?>
                                <td class="validation">
                                	<table>
                                    <tr>
                                		<td class="validation">
                                    		<label for="rbnLlaveBujiaSI" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnLlaveBujia" id="rbnLlaveBujiaSI" value="1" title="..." class="radioCond" <?php echo $llave_bujia_si; ?>/>Si</label>
                                    	</td>
                                        <td>
                                    		<label for="rbnLlaveBujiaNO" class="labelRadioCond lblColor1" style="width: 100px !important;">
   <input type="radio" name="rbnLlaveBujia" id="rbnLlaveBujiaNO" value="0" title="..." class="radioCond" <?php echo $llave_bujia_no; ?>/>No</label>
                                    	</td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    </div>
                    
                    <!-- accesorios incluidos en el vehiculo -->
                    <div id="tab-5">
                        <fieldset class="fsetForm-Data">
                            <table class="tbForm-Data">
                                <tr>
                                    <td><label for="txtTapones">Tapones:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->tapones ){
											case "":
												$tapones_si = "";
												$tapones_no = "";
												break;
											case "0":																
												$tapones_si = "";
												$tapones_no = "checked";
												break;
											case "1":
												$tapones_si = "checked";
												$tapones_no = "";
												break;
										}									
                                	?>
                                    
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnTapones" id="rbnTaponesSI" value="1" title="..." class="radioCond" <?php echo $tapones_si; ?>/>                                        		
                                                Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnTapones" id="rbnTaponesNO" value="0" title="..." class="radioCond" <?php echo $tapones_no; ?>/>                                        		
                                                No</label>                                                 
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtRinesAC">Rines de acero:</label></td>
                                    <?php
										//se validan los rines de acero
										switch( $objDet->rines_acero ){
											case "":
												$rines_acero_si = "";
												$rines_acero_no = "";
												break;
											case "0":																
												$rines_acero_si = "";
												$rines_acero_no = "checked";
												break;
											case "1":
												$rines_acero_si = "checked";
												$rines_acero_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRinesAC" id="rbnRinesACSI" value="1" title="..." class="radioCond" <?php echo $rines_acero_si; ?>/>                                        		
                                                Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRinesAC" id="rbnRinesACNO" value="0" title="..." class="radioCond" <?php echo $rines_acero_no; ?>/>                                        		
                                                No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtTorreta">Torreta:</label></td>
                                    <?php
										//se valida la torreta
										switch( $objDet->torreta ){
											case "":
												$torreta_si = "";
												$torreta_no = "";
												break;
											case "0":																
												$torreta_si = "";
												$torreta_no = "checked";
												break;
											case "1":
												$torreta_si = "checked";
												$torreta_no = "";
												break;
										}	
										//se valida el estado de la torreta
										switch( $objDet->torreta_estado ){
											case "":
												$torreta_estado_bue = "";
												$torreta_estado_mal = "";
												break;
											case "0":																
												$torreta_estado_bue = "";
												$torreta_estado_mal = "checked";
												break;
											case "1":
												$torreta_estado_bue = "checked";
												$torreta_estado_mal = "";
												break;
										}								
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnTorreta" id="rbnTorretaSI" value="1" title="..." class="radioCond" <?php echo $torreta_si; ?>/>                                        		
                                                Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnTorreta" id="rbnTorretaNO" value="0" title="..." class="radioCond" <?php echo $torreta_no; ?>/>                                        		
                                                No</label>                                                 
                                        	</td>
                                         </tr>
                                         <tr>
                                            <td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnTorretaEST" id="rbnTorretaESTSI" value="1" title="..." class="radioCond" <?php echo $torreta_estado_bue; ?>/>                                        		
                                                Buen estado</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnTorretaEST" id="rbnTorretaESTNO" value="0" title="..." class="radioCond" <?php echo $torreta_estado_mal; ?>/>                                        		
                                                Mal estado</label>                                                 
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtRadioMovil">Radio m&oacute;vil:</label></td>
                                    <?php
										//se valida el radio movil
										switch( $objDet->radio_movil ){
											case "":
												$radio_movil_si = "";
												$radio_movil_no = "";
												break;
											case "0":																
												$radio_movil_si = "";
												$radio_movil_no = "checked";
												break;
											case "1":
												$radio_movil_si = "checked";
												$radio_movil_no = "";
												break;
										}
										//se valida el estado de la radio movil	
										switch( $objDet->radio_movil_estado ){
											case "":
												$radio_movil_estado_bue = "";
												$radio_movil_estado_mal = "";
												break;
											case "0":																
												$radio_movil_estado_bue = "";
												$radio_movil_estado_mal = "checked";
												break;
											case "1":
												$radio_movil_estado_bue = "checked";
												$radio_movil_estado_mal = "";
												break;
										}								
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRadioM" id="rbnRadioMSI" value="1" title="..." class="radioCond" <?php echo $radio_movil_si; ?>/>                                        		
                                                Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRadioM" id="rbnRadioMNO" value="0" title="..." class="radioCond" <?php echo $radio_movil_no; ?>/>                                        		
                                                No</label>                                                 
                                        	</td>
                                         </tr>
                                         <tr>
                                            <td class="validation">
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRadioMEST" id="rbnRadioMESTSI" value="1" title="..." class="radioCond" <?php echo $radio_movil_estado_bue; ?>/>                                        		
                                                Buen estado</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRadioMEST" id="rbnRadioMESTNO" value="0" title="..." class="radioCond" <?php echo $radio_movil_estado_mal; ?>/>                                        		
                                                Mal estado</label>                                                 
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtParlante">Alto parlante:</label></td>
                                    <?php
										//se valida el alto parlante
										switch( $objDet->alto_parlante ){
											case "":
												$alto_parlante_si = "";
												$alto_parlante_no = "";
												break;
											case "0":																
												$alto_parlante_si = "";
												$alto_parlante_no = "checked";
												break;
											case "1":
												$alto_parlante_si = "checked";
												$alto_parlante_no = "";
												break;
										}	
										//se valida el estado del alto parlante
										switch( $objDet->alto_parlante ){
											case "":
												$alto_parlante_estado_bue = "";
												$alto_parlante_estado_mal = "";
												break;
											case "0":																
												$alto_parlante_estado_bue = "";
												$alto_parlante_estado_mal = "checked";
												break;
											case "1":
												$alto_parlante_estado_bue = "checked";
												$alto_parlante_estado_mal = "";
												break;
										}								
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnParlante" id="rbnParlanteSI" value="1" title="..." class="radioCond" <?php echo $alto_parlante_si; ?>/>Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnParlante" id="rbnParlanteNO" value="0" title="..." class="radioCond" <?php echo $alto_parlante_no; ?>/>No</label>                                                 
                                        	</td>
                                         </tr>
                                         <tr>
                                            <td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnParlanteEST" id="rbnParlanteESTSI" value="1" title="..." class="radioCond" <?php echo $alto_parlante_estado_bue; ?>/>Buen estado</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnParlanteEST" id="rbnParlanteESTNO" value="0" title="..." class="radioCond" <?php echo $alto_parlante_estado_mal; ?>/>Mal estado</label>                                                 
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtTumbaburros">Protector frontal:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->protector_frontal ){
											case "":
												$protector_frontal_si = "";
												$protector_frontal_no = "";
												break;
											case "0":																
												$protector_frontal_si = "";
												$protector_frontal_no = "checked";
												break;
											case "1":
												$protector_frontal_si = "checked";
												$protector_frontal_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnProtFrontal" id="rbnProtFrontalSI" value="1" title="..." class="radioCond" <?php echo $protector_frontal_si; ?>/>Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnProtFrontal" id="rbnProtFrontalNO" value="0" title="..." class="radioCond" <?php echo $protector_frontal_no; ?>/>No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtLona">Lona:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->lona ){
											case "":
												$lona_si = "";
												$lona_no = "";
												break;
											case "0":																
												$lona_si = "";
												$lona_no = "checked";
												break;
											case "1":
												$lona_si = "checked";
												$lona_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnLona" id="rbnLonaSI" value="1" title="..." class="radioCond" <?php echo $lona_si; ?>/>Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnLona" id="rbnLonaNO" value="0" title="..." class="radioCond" <?php echo $lona_no; ?>/>No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtRinesAL">Rines de aluminio:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->rin_aluminio ){
											case "":
												$rin_aluminio_si = "";
												$rin_aluminio_no = "";
												break;
											case "0":																
												$rin_aluminio_si = "";
												$rin_aluminio_no = "checked";
												break;
											case "1":
												$rin_aluminio_si = "checked";
												$rin_aluminio_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRinesAL" id="rbnRinesALSI" value="1" title="..." class="radioCond" <?php echo $rin_aluminio_si; ?> />Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRinesAL" id="rbnRinesALNO" value="0" title="..." class="radioCond" <?php echo $rin_aluminio_no; ?>/>No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtAA">A &frasl; A:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->aa ){
											case "":
												$aa_si = "";
												$aa_no = "";
												break;
											case "0":																
												$aa_si = "";
												$aa_no = "checked";
												break;
											case "1":
												$aa_si = "checked";
												$aa_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnAA" id="rbnAASI" value="1" title="..." class="radioCond" <?php echo $aa_si; ?>/>Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnAA" id="rbnAANO" value="0" title="..." class="radioCond" <?php echo $aa_no; ?>/>No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtRollbar">Roll bar:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->roll_bar ){
											case "":
												$roll_bar_si = "";
												$roll_bar_no = "";
												break;
											case "0":																
												$roll_bar_si = "";
												$roll_bar_no = "checked";
												break;
											case "1":
												$roll_bar_si = "checked";
												$roll_bar_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRollbar" id="rbnRollbarSI" value="1" title="..." class="radioCond" <?php echo $roll_bar_si ?>/>Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor2" style="width: 160px !important;">
<input type="radio" name="rbnRollbar" id="rbnRollbarNO" value="0" title="..." class="radioCond" <?php echo $roll_bar_no; ?>/>No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtRedilas">Redilas:</label></td>
                                    <?php
										//se valida 
										switch( $objDet->redilas ){
											case "":
												$redilas_si = "";
												$redilas_no = "";
												break;
											case "0":																
												$redilas_si = "";
												$redilas_no = "checked";
												break;
											case "1":
												$redilas_si = "checked";
												$redilas_no = "";
												break;
										}									
                                	?>
                                    <td class="validation">
                                    	<table>
                                        <tr>
                                    		<td class="validation">
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnRedilas" id="rbnRedilasSI" value="1" title="..." class="radioCond" <?php echo $redilas_si; ?>/>Si</label>                                                   
                                        	</td>
                                            <td>
                                                <label class="labelRadioCond lblColor1" style="width: 160px !important;">
<input type="radio" name="rbnRedilas" id="rbnRedilasNO" value="0" title="..." class="radioCond" <?php echo $redilas_no; ?>/>No</label>                                               
                                        	</td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtRedilas">Cantidad de llantas:</label></td>
                                    <td class="validation" style="padding-left: 10px;">
                                    	<input type="text" id="txtLlantasCant" name="txtLlantasCant" value="<?php echo $objDet->llantas_cantidad; ?>" maxlength="1" title="..." style="width: 20px; text-align: center;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="cbxMedidaLlantas">Medida de las llantas:</label></td>
                                    <td class="validation" style="padding-left: 8px;">
                                    	<select name="cbxMedidaLlantas" id="cbxMedidaLlantas">
                                            <option value="0">&nbsp;</option>
                                            <?php
                                            echo $objDet->LogcatVehMedidaLlantas->shwMedidallantas( $objDet->id_medida_llantas );
                                            ?>
                                        </select>
                                    </td>
                                </tr> 
                                <tr>
                                    <td><label for="cbxMarcaLlantas">Marca de llantas:</label></td>                                    
                                    <td class="validation" style="padding-left: 8px;">
                                        <select name="cbxMarcaLlantas" id="cbxMarcaLlantas">
                                            <option value="0">&nbsp;</option>
                                            <?php
                                            echo $objDet->LogcatVehMarcaLlantas->shwMarcaLlantas( $objDet->id_marca_llantas );
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="txtLlantasVU">Vida &uacute;til de las llantas:</label></td>                                    
                                    <td class="validation" style="padding-left: 10px;">
                                    	<input type="text" id="txtLlantasVU" name="txtLlantasVU" value="<?php echo $objDet->llantas_vida_util; ?>" maxlength="20" title="..." style="width: 200px;" />
                                    </td>
                                </tr>                                  
                                
                            </table>
                        </fieldset>                    
                    </div>
                                            
            </div>
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>   
        
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE BUSQUEDA DE ELEMENTOS ------------- -->
    <div id="dvBuscar" style="display: none;" title="Busqueda de personal">            
        
            <!-- PORTACION DE ARMAMENTO DEL ELEMENTO EN CUESTION -->                                                                                
            <div id="dvGrid-Personal" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
                <div class="xGrid-dvHeader gradient">
                    <table class="xGrid-tbSearch">
                        <tr>
                            <td>Buscar: <input type="text" name="txtSearch" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                        </tr>
                    </table>
                
                    <table class="xGrid-tbCols">
                        <tr>
                            <th style="width: 70%; text-align: center;">NOMBRE COMPLETO</th>                        
                            <th style="width: 25%; text-align: center;">CATEGORIA</th>                                                                            
                            <th style="width:  5%; text-align: center;">&nbsp;</th>                            
                        </tr>
                    </table>
                </div>
                <div class="xGrid-dvBody">                                       
                <!-- aqui de despliegan LOS ELEMENTOS BUSCADOS -->
                </div> 
            </div>
              
    </div>        
                                     
    <input type="hidden" name="dtTypeOper" value="1" />
    <input type="hidden" name="hdnIdVehiculo" value="<?php echo $_GET["id"]; ?>" />
    <input type="hidden" name="dtTypeOper" value="<?php echo $e; ?>" />
    <input type="hidden" id="hdnUrlMarca" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_marcas.php');?>" />
    <input type="hidden" id="hdnUrlTipo" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_tipos.php');?>" />
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_personal.php'); ?>" />
    <input type="hidden" id="hdnUrlUpload" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_upload.php');?>" />
    <input type="hidden" id="hdnUrlDirTemp" value="<?php echo $objSys->encrypt('log/_uploadfiles/');?>" />            
        
    </form>   
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>